'use strict';

module.exports = function (grunt) {

    require('jit-grunt')(grunt, {
        fileblocks: 'grunt-file-blocks',
        useminPrepare: 'grunt-usemin',
        libsass: 'grunt-contrib-sass',
        connect: 'grunt-contrib-connect',
        watch: 'grunt-contrib-watch'
    });
    require('time-grunt')(grunt);

    grunt.loadNpmTasks('grunt-text-replace');
    grunt.renameTask('replace', 'textReplace');

    var editorTemplate = grunt.file.read('app/editor.template.html', {encoding: 'utf8'});

    grunt.initConfig({
        watch: {
            css: {
                files: ['app/styles/sass/**/*.scss'],
                tasks: [
                    'sass:dev',
                    'fileblocks:dev',
                    'replace:dev'
                ]
            },
            js: {
                files: ['app/scripts/**/*.js'],
                tasks: [
                    'browserify:dev'
                ]
            }
        },
        browserify: {
            options: {
                require: [
                    './node_modules/prismjs/components/prism-c.js',
                    './node_modules/prismjs/components/prism-php.js',
                    './node_modules/prismjs/components/prism-go.js',
                    './node_modules/prismjs/components/prism-python.js',
                    './node_modules/prismjs/components/prism-ruby.js',
                    './node_modules/prismjs/components/prism-java.js',
                    './node_modules/prismjs/components/prism-cpp.js'
                ]
            },
            dev: {
                browserifyOptions: {
                    debug: true
                },
                src: 'app/scripts/core/Redige.js',
                dest: 'app/editor.js',
            },
            dist: {
                browserifyOptions: {
                    debug: false
                },
                src: 'app/scripts/core/Redige.js',
                dest: 'dist/editor.js',
            },
            pasteLab: {
                src: 'lab/paste/main_template.js',
                dest: 'lab/paste/main.js',
            }
        },
        wrap: {
            basic: {
                src: 'dist/editor.js',
                dest: 'dist/editor.js',
                options: {
                    wrapper: ['(function () {\n var ', '\n})();']
                }
            }
        },

        sass: {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'app/styles',
                    src: ['**/*.scss'],
                    dest: 'app/styles',
                    ext: '.css'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'dist/styles',
                    src: ['**/*.scss'],
                    dest: 'dist/styles',
                    ext: '.css'
                }]
            }
        },
        replace: {
            options: {
                patterns: [
                    {
                        match: /<!-- replace:editor -->\n?([\s\S]*?)\n?<!-- end-replace -->/gm,
                        replacement: '<!-- replace:editor -->\n' + editorTemplate + '\n<!-- end-replace -->'
                    }
                ]
            },
            dev: {
                files: {
                    'app/index.html': 'app/index.html'
                }
            },
            dist: {
                files: {
                    'dist/index.html': 'dist/index.html'
                }
            },
            test: {
                files: {
                    'test/index.html': 'test/index.html'
                }
            }
        },
        textReplace: {
            dist: {
                src: ['dist/editor.css'],
                overwrite: true,
                replacements: [{
                    from: 'url("../',
                    to: 'url("./'
                }]
            }
        },

        fileblocks: {
            dev: {
                files: [
                    {
                        src: 'app/index.html',
                        blocks: {
                            'style': {
                                src: ['app/styles/sass/main.css', 'node_modules/mdi/css/materialdesignicons.css'],
                                rename: function (dest, src) {
                                    //remove the first part of the path (app/)
                                    if (src.indexOf("app") === 0) {
                                        return src.substring(src.indexOf("app")).substring(4);
                                    } else {
                                        return "../" + src;
                                    }
                                }
                            }
                        }
                    }
                ]
            },
            dist: {
                files: [
                    {
                        src: 'dist/index.html',
                        blocks: {
                            'style': {
                                src: ['dist/styles/sass/*.css', 'node_modules/mdi/css/materialdesignicons.css'],
                                rename: function (dest, src) {
                                    //remove the first part of the path (dist/)
                                    if (src.indexOf("dist") === 0) {
                                        return src.substring(src.indexOf("dist")).substring(4);
                                    } else {
                                        return "../" + src;
                                    }
                                }
                            }
                        }
                    }
                ]
            }
        },
        copy: {
            index: {
                src: 'app/index.template.html',
                dest: 'app/index.html'
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: 'app/',
                        src: ['**'],
                        dest: 'dist/'
                    },
                    {
                        src: ['app/index.template.html'],
                        dest: 'dist/index.html'
                    },
                    {
                        expand: true,
                        cwd: 'node_modules/mdi/fonts/',
                        src: ['*'],
                        dest: 'dist/fonts/'
                    },
                    {
                        expand: true,
                        cwd: 'app/styles/sass/fonts/',
                        src: ['**/*.ttf'],
                        dest: 'dist/fonts'
                    }
                ]
            }
        },

        clean: {
            dev: {
                files: {
                    src: [
                        "app/index.html"
                    ]
                }
            },
            dist: ["dist"],
            postDist: {
                files: {
                    src: [
                        "dist/styles",
                        "dist/scripts/**/*",
                        "!dist/scripts/*.js",
                        "dist/index.template.html",
                        "dist/editor.template.html",
                        ".tmp"
                    ]
                }
            }
        },

        useminPrepare: {
            html: 'dist/index.html',
            options: {
                dest: 'dist',
                flow: {
                    steps: {
                        css: ['concat']
                    },
                    post: {}
                }
            }
        },

        usemin: {
            html: 'dist/index.html'
        },

        connect: {
            all: {
                options: {
                    port: 9001,
                    hostname: 'localhost'
                }
            }
        },
        open: {
            test: {path: 'http://localhost:9001/test'}
        },
        mochaTest: {
            all: {
                src: ['test2/**/*.spec.js']
            }
        }
    });


    grunt.registerTask('serve', [
        'sass:dev',
        'fileblocks:dev',
        'replace:dev',
        'connect:all',
        'watch'
    ]);

    grunt.registerTask('test', ['mochaTest']);

    grunt.registerTask('dev', [
        'clean:dev',
        'copy:index',
        'sass:dev',
        'browserify:dev',
        'fileblocks:dev',
        'replace:dev'
    ]);

    grunt.registerTask('paste','browserify:pasteLab');

    grunt.registerTask('default', [
        'dev',
        'watch'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'copy:dist',
        'sass:dist',
        'browserify:dist',
        'fileblocks:dist',
        'replace:dist',
        'useminPrepare',
        'concat',
        'textReplace:dist',
        'usemin',
        'clean:postDist',
        'wrap'
    ]);
};
