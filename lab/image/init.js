var mockData = function () {
    "use strict";
    return new Model.Document().deserialize({
        id: 1,
        divisions: [
            {
                id: "k10185580p59",
                type: "default",
                sections: [
                    {
                        id: "S11",
                        level: "1",
                        type: "hierarchical",
                        content: "Mon Titre 1"
                    },
                    {
                        id: "S21",
                        level: "2",
                        type: "hierarchical",
                        content: "Mon Titre 2"
                    }
                ]
            },
            {
                id: "i2",
                type: "code",
                sections: [
                    {
                        id: "S1",
                        level: "1",
                        type: "hierarchical",
                        content: "aaa"
                    },
                    {
                        id: "S2",
                        level: "2",
                        type: "hierarchical",
                        content: "Bonjour voici un élémént décoré",
                        decorators: [{
                            id: "A",
                            start: 17,
                            end: 24,
                            type: "link"
                        }, {
                            id: "B",
                            start: 7,
                            end: 20,
                            type: "bold"
                        }]
                    }
                ]
            }, {
                id: "i101s8558059",
                type: "default",
                sections: [
                    {
                        id: "S11m",
                        level: "0",
                        type: "hierarchical",
                        content: "mon texte 1"
                    },
                    {
                        id: "S21m",
                        level: "0",
                        type: "hierarchical",
                        content: "Mon TExt 2"
                    },
                    {
                        id: "imgId",
                        type: "image",
                        data: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAV4AAACWCAAAAACzjL+rAAAKQWlDQ1BJQ0MgUHJvZmlsZQAASA2dlndUU9kWh8+9N73QEiIgJfQaegkg0jtIFQRRiUmAUAKGhCZ2RAVGFBEpVmRUwAFHhyJjRRQLg4Ji1wnyEFDGwVFEReXdjGsJ7601896a/cdZ39nnt9fZZ+9917oAUPyCBMJ0WAGANKFYFO7rwVwSE8vE9wIYEAEOWAHA4WZmBEf4RALU/L09mZmoSMaz9u4ugGS72yy/UCZz1v9/kSI3QyQGAApF1TY8fiYX5QKUU7PFGTL/BMr0lSkyhjEyFqEJoqwi48SvbPan5iu7yZiXJuShGlnOGbw0noy7UN6aJeGjjAShXJgl4GejfAdlvVRJmgDl9yjT0/icTAAwFJlfzOcmoWyJMkUUGe6J8gIACJTEObxyDov5OWieAHimZ+SKBIlJYqYR15hp5ejIZvrxs1P5YjErlMNN4Yh4TM/0tAyOMBeAr2+WRQElWW2ZaJHtrRzt7VnW5mj5v9nfHn5T/T3IevtV8Sbsz55BjJ5Z32zsrC+9FgD2JFqbHbO+lVUAtG0GQOXhrE/vIADyBQC03pzzHoZsXpLE4gwnC4vs7GxzAZ9rLivoN/ufgm/Kv4Y595nL7vtWO6YXP4EjSRUzZUXlpqemS0TMzAwOl89k/fcQ/+PAOWnNycMsnJ/AF/GF6FVR6JQJhIlou4U8gViQLmQKhH/V4X8YNicHGX6daxRodV8AfYU5ULhJB8hvPQBDIwMkbj96An3rWxAxCsi+vGitka9zjzJ6/uf6Hwtcim7hTEEiU+b2DI9kciWiLBmj34RswQISkAd0oAo0gS4wAixgDRyAM3AD3iAAhIBIEAOWAy5IAmlABLJBPtgACkEx2AF2g2pwANSBetAEToI2cAZcBFfADXALDIBHQAqGwUswAd6BaQiC8BAVokGqkBakD5lC1hAbWgh5Q0FQOBQDxUOJkBCSQPnQJqgYKoOqoUNQPfQjdBq6CF2D+qAH0CA0Bv0BfYQRmALTYQ3YALaA2bA7HAhHwsvgRHgVnAcXwNvhSrgWPg63whfhG/AALIVfwpMIQMgIA9FGWAgb8URCkFgkAREha5EipAKpRZqQDqQbuY1IkXHkAwaHoWGYGBbGGeOHWYzhYlZh1mJKMNWYY5hWTBfmNmYQM4H5gqVi1bGmWCesP3YJNhGbjS3EVmCPYFuwl7ED2GHsOxwOx8AZ4hxwfrgYXDJuNa4Etw/XjLuA68MN4SbxeLwq3hTvgg/Bc/BifCG+Cn8cfx7fjx/GvyeQCVoEa4IPIZYgJGwkVBAaCOcI/YQRwjRRgahPdCKGEHnEXGIpsY7YQbxJHCZOkxRJhiQXUiQpmbSBVElqIl0mPSa9IZPJOmRHchhZQF5PriSfIF8lD5I/UJQoJhRPShxFQtlOOUq5QHlAeUOlUg2obtRYqpi6nVpPvUR9Sn0vR5Mzl/OX48mtk6uRa5Xrl3slT5TXl3eXXy6fJ18hf0r+pvy4AlHBQMFTgaOwVqFG4bTCPYVJRZqilWKIYppiiWKD4jXFUSW8koGStxJPqUDpsNIlpSEaQtOledK4tE20Otpl2jAdRzek+9OT6cX0H+i99AllJWVb5SjlHOUa5bPKUgbCMGD4M1IZpYyTjLuMj/M05rnP48/bNq9pXv+8KZX5Km4qfJUilWaVAZWPqkxVb9UU1Z2qbapP1DBqJmphatlq+9Uuq43Pp893ns+dXzT/5PyH6rC6iXq4+mr1w+o96pMamhq+GhkaVRqXNMY1GZpumsma5ZrnNMe0aFoLtQRa5VrntV4wlZnuzFRmJbOLOaGtru2nLdE+pN2rPa1jqLNYZ6NOs84TXZIuWzdBt1y3U3dCT0svWC9fr1HvoT5Rn62fpL9Hv1t/ysDQINpgi0GbwaihiqG/YZ5ho+FjI6qRq9Eqo1qjO8Y4Y7ZxivE+41smsImdSZJJjclNU9jU3lRgus+0zwxr5mgmNKs1u8eisNxZWaxG1qA5wzzIfKN5m/krCz2LWIudFt0WXyztLFMt6ywfWSlZBVhttOqw+sPaxJprXWN9x4Zq42Ozzqbd5rWtqS3fdr/tfTuaXbDdFrtOu8/2DvYi+yb7MQc9h3iHvQ732HR2KLuEfdUR6+jhuM7xjOMHJ3snsdNJp9+dWc4pzg3OowsMF/AX1C0YctFx4bgccpEuZC6MX3hwodRV25XjWuv6zE3Xjed2xG3E3dg92f24+ysPSw+RR4vHlKeT5xrPC16Il69XkVevt5L3Yu9q76c+Oj6JPo0+E752vqt9L/hh/QL9dvrd89fw5/rX+08EOASsCegKpARGBFYHPgsyCRIFdQTDwQHBu4IfL9JfJFzUFgJC/EN2hTwJNQxdFfpzGC4sNKwm7Hm4VXh+eHcELWJFREPEu0iPyNLIR4uNFksWd0bJR8VF1UdNRXtFl0VLl1gsWbPkRoxajCCmPRYfGxV7JHZyqffS3UuH4+ziCuPuLjNclrPs2nK15anLz66QX8FZcSoeGx8d3xD/iRPCqeVMrvRfuXflBNeTu4f7kufGK+eN8V34ZfyRBJeEsoTRRJfEXYljSa5JFUnjAk9BteB1sl/ygeSplJCUoykzqdGpzWmEtPi000IlYYqwK10zPSe9L8M0ozBDuspp1e5VE6JA0ZFMKHNZZruYjv5M9UiMJJslg1kLs2qy3mdHZZ/KUcwR5vTkmuRuyx3J88n7fjVmNXd1Z752/ob8wTXuaw6thdauXNu5Tnddwbrh9b7rj20gbUjZ8MtGy41lG99uit7UUaBRsL5gaLPv5sZCuUJR4b0tzlsObMVsFWzt3WazrWrblyJe0fViy+KK4k8l3JLr31l9V/ndzPaE7b2l9qX7d+B2CHfc3em681iZYlle2dCu4F2t5czyovK3u1fsvlZhW3FgD2mPZI+0MqiyvUqvakfVp+qk6oEaj5rmvep7t+2d2sfb17/fbX/TAY0DxQc+HhQcvH/I91BrrUFtxWHc4azDz+ui6rq/Z39ff0TtSPGRz0eFR6XHwo911TvU1zeoN5Q2wo2SxrHjccdv/eD1Q3sTq+lQM6O5+AQ4ITnx4sf4H++eDDzZeYp9qukn/Z/2ttBailqh1tzWibakNml7THvf6YDTnR3OHS0/m/989Iz2mZqzymdLz5HOFZybOZ93fvJCxoXxi4kXhzpXdD66tOTSna6wrt7LgZevXvG5cqnbvfv8VZerZ645XTt9nX297Yb9jdYeu56WX+x+aem172296XCz/ZbjrY6+BX3n+l37L972un3ljv+dGwOLBvruLr57/17cPel93v3RB6kPXj/Mejj9aP1j7OOiJwpPKp6qP6391fjXZqm99Oyg12DPs4hnj4a4Qy//lfmvT8MFz6nPK0a0RupHrUfPjPmM3Xqx9MXwy4yX0+OFvyn+tveV0auffnf7vWdiycTwa9HrmT9K3qi+OfrW9m3nZOjk03dp76anit6rvj/2gf2h+2P0x5Hp7E/4T5WfjT93fAn88ngmbWbm3/eE8/syOll+AAAEtElEQVR42u3a21LiTBSG4bn/C3g7QBAQ3IDiBjc4imyUbbIu6j/oTkgscayamrLqz/edTCWZ5OCxs3p1h1+m/MP8EoF4xauIV7ziVcQrXvEq4hWveBXxilcRr3jFq4hXvOJVxCte8SriFa8iXvGKVxGveMWriFe84lXEK17xKuIVryJe8YpXEa94xauIV7ziVcQrXkW84hWvIl7xilcRr3jFq4hXvF8nXW6/vL5J/uLZ64rzLrsOGtc54Sj2GQef6xau+3Lo7ts4juM4M0zCve1wPDuLiC92FeZdRQDQzU6c+2Me/OEQADc9cPsVAKtw9B7urYdn1wA4SavLOwSOGsAknDgu8T4DNSDaHOB1rsD7WuJN2+Hm6+ry1uHSkg6chRMNXJqmaRhxHTjZvTm4P1iZC7yPcLu/eQJMkz7Uk6ryJsC72Rha+Ylm2W5u1ofed3hv4Gl/5RJOzJbArKq8SyAxm+f1clmSnEDNzJ78P3/k7ZckO37MN78Y+v933vdOpxPmJH9iChf7y3fQMbM54LuD8N6nn/N2s/9mZmaRL+gnpSdWclkxz4fnGEalxuDUzFa+RpilF/3UzNatyae8MW4PvwMWZjaAk4rz/s4r7i383p/vQ9/MtsBL6DP6qa2bhU6twJvCUbnuLP097Wrzpj3P6Mda+/IhG47hxU4Jc9arg/6qCa3NJ7xrqJ3dvKT5G8Haz3dxlXnHN20gDMceANE4L6ZDMzPg0TJfV9Qt8s5819tb50db/z40qsxbA2rB05rOL+PG2RpjaGbm8lXcq6OkW+R9wi/Tmkm2xtiZ2SjvSqrL211ki+Q0XQ6AOAm91VXgvQsFIAb6n3cO263tXhrZim8SeO8gqjLv/fURuGLrf0GY4Aqj1/eu6yMo+xY7BzOzlQsTXGH01qo9te3a5dl9Bdx8qL0PmW7ryRV9P/LaAEh9B53V3nq1eW1MaUFgtdBJnMBl6BzGYTuttbGJg+eDvPehIZsDG3UOWRM1Lxy3wlLg3DPvCJ7bdmtjZhN3fbA42HN41nu4cJXvZ1SWdwW8Fo7jsJAd+lXbOt+W2fqe4d0O8z6FN2ELvPlC3qvsnsN0ugojrbBjnjq4NT8tHZvZ4iOhfcE7IiyNnf+TncKgqrwDP4fNQ720fF/Hr9Je/JLgGaL0u7zdbIHd9muRVt7UVY/3xn8GeghNVMgpuHVWNJZmQz+Iv8W7wE+HZgPom21cue5UincMbmG7Vjb9zMaJ7Ybke4gtuLRtrbSNdpB39Ga2aOTHz+DWdgtRZb9WrCPguJEVA7vENR37UvEIdOoQrf7Mu4F6A/JSm8RQ6xJa52p2Djfh6+NxkrUMALV8P7fvTxz8El/gfQqPOsvKzJvfvugk1eW1hwiIrjxBOmoCUW/feCWDOhw92Dd45+cOaI72k+BLB6LTn/4lyc/2velqWnzz17P3cpOQLr7tk7zNPvzgZzv/4aFr+o2ZeMWriFe84lXEK17xKuIVr3gV8YpXEa94xauIV7ziVcQrXvEq4hWvIl7xilcRr3jFq4hXvOJVxCte8SriFa8iXvGKVxGveMWriFe84lXEK15FvOIVryJe8YpXEa94xauIV7ziFcG/zH9acb4vUyMUYQAAAABJRU5ErkJggg=="
                    },
                    {
                        id: "imgId2",
                        type: "image",
                        data: "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%971500&w=350&h=1500"
                    },
                    {
                        id: "imgId3",
                        type: "image",
                        data: "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%971500&w=1000&h=150"
                    },
                    {
                        id: "imgId3",
                        type: "image",
                        data: "https://placeholdit.imgix.net/~text?txtsize=33&txt=350%C3%97351&w=350&h=351"
                    }, {
                        id: "imgId3",
                        type: "image",
                        data: "https://placeholdit.imgix.net/~text?txtsize=33&txt=351%C3%97350&w=351&h=350"
                    }, {
                        id: "imgId4",
                        type: "image",
                        data: "https://images.unsplash.com/17/unsplash_527bf56961712_1.JPG?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&s=0297526e21985f2cc06e61611a49b390"
                    },
                    {
                        id: "S21o",
                        level: "0",
                        type: "olist",
                        content: "Mon TExt 2"
                    }, {
                        id: "S2u1",
                        level: "0",
                        type: "ulist",
                        content: "Mon TExt 2"
                    }
                ]
            }]
    });
};

setTimeout(function () {
    "use strict";
    //Get editor element
    var editor = document.getElementById("editor");
    var redigeDoc = mockData();
    while (editor.childNodes.length > 0) {
        editor.removeChild(editor.childNodes[0]);
    }
    editor.appendChild(redigeDoc);
    NumberingHandler.refreshNumbering(editor);
    Redige.dispatchEventByName(editor, 'selectionChange');
}, 250);