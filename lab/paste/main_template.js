(function () {
    var Redige = require("../../app/scripts/core/Redige.js");
    var _ = require("lodash");

    document.getElementById('pastabox').addEventListener('paste', handlePaste)

    var pastedArea = CodeMirror.fromTextArea(document.getElementById('pasted'), {
        lineNumbers: true,
        mode: "xml",
        lineWrapping: true
    });

    var normalizdArea = CodeMirror.fromTextArea(document.getElementById('cleanned'), {
        lineNumbers: true,
        mode: "xml",
        lineWrapping: true
    });
    
    function handlePaste(event) {

        console.log(event.clipboardData.types);

        var htmlType;
        var plainType;
        var imageType;
        var fileType;
        event.clipboardData.types.forEach(function (type) {
            if (type.match("Files")) {
                fileType = type;
            }
            if (type.match("^text/html")) {
                htmlType = type;
            }
            if (type.match("^text/plain")) {
                plainType = type;
                console.log(event.clipboardData.getData(plainType));
            }
            if (type.match("^image/")) {
                imageType = type;
            }
        });

        if (htmlType) {
            var data = (event.clipboardData.getData(htmlType));
            var htmlParserType = detectOrigin(data);
            document.getElementById("pastedFrom").textContent = "Pasted from " + htmlParserType;
            pastedArea.setValue(removeClasses(removeStyles(data)));

            var newDoc;
            var parsedResult;
            if (htmlParserType === "HTML") {
                parsedResult = parseDefaultHTML(data);
            } else if (htmlParserType === "WRITER") {
                parsedResult = parseWriterHTML(data);
            } else if (htmlParserType === "WORD") {
                parsedResult = parseWordHTML(data);
            }
            normalizdArea.setValue(JSON.stringify(summarize(parsedResult[0]), null, ' '));
            normalizdArea.autoFormatRange(
                {line: 0, ch: 0},
                {
                    line: normalizdArea.lineCount(),
                    ch: normalizdArea.getLine(normalizdArea.lineCount() - 1).length
                });
            console.log(parsedResult[0]);
            newDoc = parsedResult[1];

            let rdDoc = new Redige.Document();
            if (newDoc.copyType !== "inline") {
                rdDoc.init(newDoc.content);
            } else {
                //let sec = new Redige.Section();
                //sec.init(newDoc.content);
                //rdDoc.children.push(sec);
                rdDoc.init({children: [newDoc.content]});
            }
            Redige.reset(rdDoc);
        }
        event.preventDefault();

    }

    function detectOrigin(data) {
        var writerHTMLParser = require("../../app/scripts/features/paste/WriterParser/WriterHTMLParser");
        var wordHTMLParser = require("../../app/scripts/features/paste/WordParser/WordHTMLParser");
        var gdocHTMLParser = require("../../app/scripts/features/paste/GdocParser/GdocHTMLParser");
        var extract = data.substring(0, 300);
        if (writerHTMLParser.couldParse(extract)) {
            return "WRITER";
        } else if (wordHTMLParser.couldParse(extract)) {
            return "WORD";
        } else if (gdocHTMLParser.couldParse(extract)) {
            return "GDOC";
        } else {
            return "HTML";
        }
    }

    function parseDefaultHTML(data) {
        var htmlparser = require("htmlparser2");
        var HTMLSaxHandler = require("../../app/scripts/features/paste/HTMLParser2/HTMLSaxHandler");
        var HTMLNormalize = require("../../app/scripts/features/paste/HTMLParser2/HTMLNormalize");
        var toRedigeModel = require("../../app/scripts/features/paste/HTMLParser2/toRedigeModel");

        var result = {};
        var parser = new htmlparser.Parser(new HTMLSaxHandler(result), {
            decodeEntities: true,
            recognizeSelfClosing: true
        });
        parser.write(data);
        parser.end();
        var normalized = HTMLNormalize.normalize(result);
        console.log(normalized);
        return [normalized, toRedigeModel.convert(normalized)];
    }

    function parseWriterHTML(data) {
        var htmlparser = require("htmlparser2");
        var HTMLSaxHandler = require("../../app/scripts/features/paste/HTMLParser2/HTMLSaxHandler");
        var HTMLNormalize = require("../../app/scripts/features/paste/WriterParser/HTMLNormalize");
        var toRedigeModel = require("../../app/scripts/features/paste/HTMLParser2/toRedigeModel");

        var result = {};
        var parser = new htmlparser.Parser(new HTMLSaxHandler(result), {
            decodeEntities: true,
            recognizeSelfClosing: true
        });
        parser.write(data);
        parser.end();
        var normalized = HTMLNormalize.normalize(result);
        console.log(normalized);
        return [normalized, toRedigeModel.convert(normalized)];
    }

    function parseWordHTML(data) {
        var htmlparser = require("htmlparser2");
        var HTMLSaxHandler = require("../../app/scripts/features/paste/WordParser/HTMLSaxHandler");
        var HTMLNormalize = require("../../app/scripts/features/paste/WordParser/HTMLNormalize");
        var toRedigeModel = require("../../app/scripts/features/paste/HTMLParser2/toRedigeModel");

        var result = {};
        var parser = new htmlparser.Parser(new HTMLSaxHandler(result), {
            decodeEntities: true,
            recognizeSelfClosing: true
        });
        parser.write(data);
        parser.end();
        var normalized = HTMLNormalize.normalize(result);
        console.log(normalized);
        return [normalized, toRedigeModel.convert(normalized)];
    }

    function removeStyles(result) {
        result = result.replace(/ style=\"[^\"]*\"/gim, "");
        return result;
    }

    function removeClasses(result) {
        result = result.replace(/ class=\"[^\"]*\"/gim, "");
        return result;
    }

    function summarize(norm) {
        if (norm.name === "#text") {
            return norm.text;
        }
        if (_.isEmpty(norm.children) && !_.isEmpty(norm.text)) {
            return "<" + norm.name + ">" + norm.text + "</" + norm.name + ">";
        } else if (_.isEmpty(norm.children) && _.isEmpty(norm.text)) {
            return "</" + norm.name + ">";
        }

        let result = "<" + norm.name + ">";
        norm.children.forEach(function (child) {
            result += summarize(child);
        });
        return result + "</" + norm.name + ">";
    }
})();
