(function () {

    NodeList.prototype.forEach = HTMLCollection.prototype.forEach = Array.prototype.forEach;

    function render() {
        var rdDocument = document.getElementsByTagName("Document")[0];
        handleDocument(rdDocument, document.body);
        highlight();
    }

    function highlight() {
        if (typeof hljs !== "undefined") {
            hljs.configure({useBR: true});
            var divs = document.querySelectorAll('div.code');
            [].forEach.call(divs, function (div) {
                hljs.highlightBlock(div);
            });
        }
    }

    function handleDocument(rdDocument, parentNode) {

        var documentNode = document.createElement("div");
        documentNode.setAttribute("class", "document");

        parentNode.appendChild(documentNode);

        var rdDivisionList = rdDocument.getElementsByTagName("Division");
        rdDivisionList.forEach(function (division) {
            handleDivision(division, documentNode);
        });
    }

    function handleDivision(division, docNode) {

        var node = document.createElement("div");
        node.setAttribute("class", division.getAttribute("type") + " division");
        node.setAttribute("id", division.getAttribute("id"));

        docNode.appendChild(node);

        var rdSectionList = division.getElementsByTagName("Section");
        rdSectionList.forEach(function (section) {
            handleSection(section, node);
        });
    }

    function handleSection(section, divNode) {

        var sectionNode = document.createElement("p");
        sectionNode.setAttribute("class", "level_" + section.getAttribute("level") + " division " + section.getAttribute("type") + " " + section.getAttribute("type") + "_" + section.getAttribute("level"));
        sectionNode.setAttribute("id", section.getAttribute("id"));
        if (section.getAttribute("numbering")) {
            sectionNode.setAttribute("numbering", section.getAttribute("numbering"));
        }
        divNode.appendChild(sectionNode);

        handleSectionContent(section, sectionNode);
    }

    function handleSectionContent(section, sectionNode) {
        var content = section.getElementsByTagName("content")[0].innerText;
        var currentSpan;
        var textContent = "";
        for (var index = 0; index < content.length; index++) {
            var currentChar = content[index];
            if (!currentSpan || isDecoratorChange(section, index)) {
                if (currentSpan) {
                    currentSpan.textContent = textContent;
                    textContent = "";
                }
                currentSpan = changeSpan(index, section, sectionNode);
            }
            textContent += currentChar;
        }
        if (currentSpan) {
            currentSpan.textContent = textContent;
        }
    }

    function changeSpan(offset, section, sectionNode) {
        var currentSpan;
        var link = getLink(section, offset);
        if (link) {
            currentSpan = document.createElement("a");
            currentSpan.setAttribute("href", link.getAttribute("url"));
        } else {
            currentSpan = document.createElement("span");
        }
        var classes = buildDecoratorClass(section, offset);
        if (classes.length > 0) {
            currentSpan.setAttribute("class", classes);
        }
        sectionNode.appendChild(currentSpan);
        return currentSpan;
    }

    function getLink(section, offset) {
        var linkNodeList = section.getElementsByTagName("Link");
        for (var i = 0; i < linkNodeList.length; i++) {
            if (offset >= parseInt(linkNodeList[i].getAttribute("from")) && offset < parseInt(linkNodeList[i].getAttribute("to"))) {
                return linkNodeList[i];
            }
        }
        return undefined;
    }

    function buildDecoratorClass(section, offset) {
        var result = "";
        var decoratorsList = section.getElementsByTagName("decorators");
        if (decoratorsList.length > 0) {
            var decoratorList = decoratorsList[0].childNodes;
            decoratorList.forEach(function (decorator) {
                if (decorator.nodeType == 1 && ((offset >= parseInt(decorator.getAttribute("from"))) && (offset < parseInt(decorator.getAttribute("to"))))) {
                    if (result.length > 0) {
                        result += " ";
                    }
                    result += decorator.tagName.toLowerCase();
                }
            });
        }
        return result;
    }

    function isDecoratorChange(section, offset) {
        var decoratorsList = section.getElementsByTagName("decorators");
        if (decoratorsList.length > 0) {
            var decoratorList = decoratorsList[0].childNodes;
            for (var i = 0; i < decoratorList.length; i++) {
                if (decoratorList[i].nodeType == 1 && ((offset == parseInt(decoratorList[i].getAttribute("from"))) || offset == parseInt(decoratorList[i].getAttribute("to")))) {
                    return true;
                }
            }
        }
        return false;
    }

    document.addEventListener("DOMContentLoaded", render);
}());