module.exports = {
    toolbar: {
        //version short : implementation is in ToolbarDefaultConfig
        exports: ['pdf', 'publish'],
        inlines: ['code', 'important', 'quote', 'link', {
            type: 'strike',
            tooltip: 'Barré',
            icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3gwCCA8sMErT4QAAARdJREFUOMul0z8oxGEYB/DP6ScyWq5MBmKzKZZTl0Gk25RVBoNFEQYZxGCQwSLKwqYsyqp0kwwGirqyifyJUyeuLO9wnbvfXTz19Dzv8z593+f7PM+bmJ1f8B9pqBKbxjnyKOAK22irB+AAq1hBEr14wiTStQAGMYZdHOEDlxjFa6UKorLzQLAdZfEXLOOxVgX5YIexg/aSu81ALxbgEN/Bn0AOWUyhpdIUyincIIXWCrnduPgF0H97WkDTH9fgM8p2prrQGJOUQF8JhVxQ+IpwV8dL7zhBD9YxV62J6dC0crnHUvAf4qbQHModqgCSRBHHcQDFYPcxHnhHyGANi7iOG2MWMxjBFvbwHD5WBme19uANG0Hrkh8OxThjqAkkPQAAAABJRU5ErkJggg==',
            process: function () {
                var currentSelection = Redige.getDocumentSelection();
                Redige.patch({
                    action: 'toggleDecorator',
                    sectionId: currentSelection.startSection.id,
                    decorator: {
                        start: currentSelection.startOffset,
                        end: currentSelection.endOffset,
                        type: 'strike'
                    }
                });
            }
        }],
        sections: ['normal', 'header', 'olist', 'ulist'],
        blocks: ['blockquote', 'blockcode'],
        inserts: ['image', 'attachment']
    },
    style: {
        editor: {'strike': {'text-decoration': 'line-through'}},
        pdf: {'strike': {'text-decoration': 'line-through'}}
    },
    language: 'FR',
};