/*! blanket - v1.2.0 */

(function (define) {

})(null);
(function (module) {

    window.isArguments = module.exports;
})({exports: {}});
(function (module) {

    window.forEach = module.exports;
})({exports: {}});
(function (module) {

    window.isArray = module.exports;
})({exports: {}});
(function (require, module) {

    window.objectKeys = module.exports;
})(function () {
    return isArguments;
}, {exports: {}});

var inBrowser = typeof window !== 'undefined' && this === window;

(inBrowser ? window : exports).blanket = (function () {
    var __blanket,
        coverageInfo = {}, options = {
            reporter: null,
            adapter: null,
            filter: null,
            customVariable: null,
            loader: null,
            ignoreScriptError: false,
            existingRequireJS: false,
            autoStart: false,
            timeout: 180,
            ignoreCors: false,
            branchTracking: false,
            sourceURL: false,
            debug: false,
            engineOnly: false,
            testReadyCallback: null,
            commonJS: false,
            instrumentCache: false,
            modulePattern: null,
            ecmaVersion: 5
        };

    if (inBrowser && typeof window.blanket !== 'undefined') {
        __blanket = window.blanket.noConflict();
    }

    _blanket = {
        noConflict: function () {
            if (__blanket) {
                return __blanket;
            }
            return _blanket;
        },
        extend: function (obj) {
            //borrowed from underscore
            _blanket._extend(_blanket, obj);
        },
        _extend: function (dest, source) {
            if (source) {
                for (var prop in source) {
                    if (dest[prop] instanceof Object && typeof dest[prop] !== "function") {
                        _blanket._extend(dest[prop], source[prop]);
                    } else {
                        dest[prop] = source[prop];
                    }
                }
            }
        },
        getCovVar: function () {
            var opt = _blanket.options("customVariable");
            if (opt) {
                if (_blanket.options("debug")) {
                    console.log("BLANKET-Using custom tracking variable:", opt);
                }
                return inBrowser ? "window." + opt : opt;
            }
            return inBrowser ? "window._$blanket" : "_$jscoverage";
        },
        options: function (key, value) {
            if (typeof key !== "string") {
                _blanket._extend(options, key);
            } else if (typeof value === 'undefined') {
                return options[key];
            } else {
                options[key] = value;
            }
        },
        _trackingArraySetup: [],
        _branchingArraySetup: [],
        _useStrictMode: false,

        setupCoverage: function () {
            coverageInfo.instrumentation = "blanket";
            coverageInfo.stats = {
                "suites": 0,
                "tests": 0,
                "passes": 0,
                "pending": 0,
                "failures": 0,
                "start": new Date()
            };
        },
        _checkIfSetup: function () {
            if (!coverageInfo.stats) {
                throw new Error("You must call blanket.setupCoverage() first.");
            }
        },
        onTestStart: function () {
            if (_blanket.options("debug")) {
                console.log("BLANKET-Test event started");
            }
            this._checkIfSetup();
            coverageInfo.stats.tests++;
            coverageInfo.stats.pending++;
        },
        onTestDone: function (total, passed) {
            this._checkIfSetup();
            if (passed === total) {
                coverageInfo.stats.passes++;
            } else {
                coverageInfo.stats.failures++;
            }
            coverageInfo.stats.pending--;
        },
        onTestsDone: function () {
            /*
             *
             * PATCH !! HARD CODED !!
             */
            window._$blanket = [_$jscoverage['dist/editor.js']];
            if (_blanket.options("debug")) {
                console.log("BLANKET-Test event done");
            }
            this._checkIfSetup();
            coverageInfo.stats.end = new Date();

            if (inBrowser) {
                this.report(coverageInfo);
            } else {
                if (!_blanket.options("branchTracking")) {
                    delete (inBrowser ? window : global)[_blanket.getCovVar()].branchFcn;
                }
                this.options("reporter").call(this, coverageInfo);
            }
        }
    };
    return _blanket;
})();

(function (_blanket) {
    var oldOptions = _blanket.options;
    _blanket.extend({
        outstandingRequireFiles: [],
        options: function (key, value) {
            var newVal = {};

            if (typeof key !== "string") {
                //key is key/value map
                oldOptions(key);
                newVal = key;
            } else if (typeof value === 'undefined') {
                //accessor
                return oldOptions(key);
            } else {
                //setter
                oldOptions(key, value);
                newVal[key] = value;
            }

            if (newVal.adapter) {
                _blanket._loadFile(newVal.adapter);
            }
            if (newVal.loader) {
                _blanket._loadFile(newVal.loader);
            }
        },
        requiringFile: function (filename, done) {
            if (typeof filename === "undefined") {
                _blanket.outstandingRequireFiles = [];
            } else if (typeof done === "undefined") {
                _blanket.outstandingRequireFiles.push(filename);
            } else {
                _blanket.outstandingRequireFiles.splice(_blanket.outstandingRequireFiles.indexOf(filename), 1);
            }
        },
        requireFilesLoaded: function () {
            return _blanket.outstandingRequireFiles.length === 0;
        },
        showManualLoader: function () {
            if (document.getElementById("blanketLoaderDialog")) {
                return;
            }
            //copied from http://blog.avtex.com/2012/01/26/cross-browser-css-only-modal-box/
            var loader = "<div class='blanketDialogOverlay'>";
            loader += "&nbsp;</div>";
            loader += "<div class='blanketDialogVerticalOffset'>";
            loader += "<div class='blanketDialogBox'>";
            loader += "<b>Error:</b> Blanket.js encountered a cross origin request error while instrumenting the source files.  ";
            loader += "<br><br>This is likely caused by the source files being referenced locally (using the file:// protocol). ";
            loader += "<br><br>Some solutions include <a href='http://askubuntu.com/questions/160245/making-google-chrome-option-allow-file-access-from-files-permanent' target='_blank'>starting Chrome with special flags</a>, <a target='_blank' href='https://github.com/remy/servedir'>running a server locally</a>, or using a browser without these CORS restrictions (Safari).";
            loader += "<br>";
            if (typeof FileReader !== "undefined") {
                loader += "<br>Or, try the experimental loader.  When prompted, simply click on the directory containing all the source files you want covered.";
                loader += "<a href='javascript:document.getElementById(\"fileInput\").click();'>Start Loader</a>";
                loader += "<input type='file' type='application/x-javascript' accept='application/x-javascript' webkitdirectory id='fileInput' multiple onchange='window.blanket.manualFileLoader(this.files)' style='visibility:hidden;position:absolute;top:-50;left:-50'/>";
            }
            loader += "<br><span style='float:right;cursor:pointer;'  onclick=document.getElementById('blanketLoaderDialog').style.display='none';>Close</span>";
            loader += "<div style='clear:both'></div>";
            loader += "</div></div>";

            var css = ".blanketDialogWrapper {";
            css += "display:block;";
            css += "position:fixed;";
            css += "z-index:40001; }";

            css += ".blanketDialogOverlay {";
            css += "position:fixed;";
            css += "width:100%;";
            css += "height:100%;";
            css += "background-color:black;";
            css += "opacity:.5; ";
            css += "-ms-filter:'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)'; ";
            css += "filter:alpha(opacity=50); ";
            css += "z-index:40001; }";

            css += ".blanketDialogVerticalOffset { ";
            css += "position:fixed;";
            css += "top:30%;";
            css += "width:100%;";
            css += "z-index:40002; }";

            css += ".blanketDialogBox { ";
            css += "width:405px; ";
            css += "position:relative;";
            css += "margin:0 auto;";
            css += "background-color:white;";
            css += "padding:10px;";
            css += "border:1px solid black; }";

            var dom = document.createElement("style");
            dom.innerHTML = css;
            document.head.appendChild(dom);

            var div = document.createElement("div");
            div.id = "blanketLoaderDialog";
            div.className = "blanketDialogWrapper";
            div.innerHTML = loader;
            document.body.insertBefore(div, document.body.firstChild);

        },
        manualFileLoader: function (files) {
            var toArray = Array.prototype.slice;
            files = toArray.call(files).filter(function (item) {
                return item.type !== "";
            });
            var sessionLength = files.length - 1;
            var sessionIndx = 0;
            var sessionArray = {};
            if (sessionStorage["blanketSessionLoader"]) {
                sessionArray = JSON.parse(sessionStorage["blanketSessionLoader"]);
            }


            var fileLoader = function (event) {
                var fileContent = event.currentTarget.result;
                var file = files[sessionIndx];
                var filename = file.webkitRelativePath && file.webkitRelativePath !== '' ? file.webkitRelativePath : file.name;
                sessionArray[filename] = fileContent;
                sessionIndx++;
                if (sessionIndx === sessionLength) {
                    sessionStorage.setItem("blanketSessionLoader", JSON.stringify(sessionArray));
                    document.location.reload();
                } else {
                    readFile(files[sessionIndx]);
                }
            };

            function readFile(file) {
                var reader = new FileReader();
                reader.onload = fileLoader;
                reader.readAsText(file);
            }

            readFile(files[sessionIndx]);
        },
        _loadFile: function (path) {
            if (typeof path !== "undefined") {
                var request = new XMLHttpRequest();
                request.open('GET', path, false);
                request.send();
                _blanket._addScript(request.responseText);
            }
        },
        _addScript: function (data) {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.text = data;
            (document.body || document.getElementsByTagName('head')[0]).appendChild(script);
        },
        report: function (coverage_data) {
            if (!document.getElementById("blanketLoaderDialog")) {
                //all found, clear it
                _blanket.blanketSession = null;
            }
            coverage_data.files = window._$blanket;
            var require = blanket.options("commonJS") ? blanket._commonjs.require : window.require;

            // Check if we have any covered files that requires reporting
            // otherwise just exit gracefully.
            if (!coverage_data.files || !Object.keys(coverage_data.files).length) {
                if (_blanket.options("debug")) {
                    console.log("BLANKET-Reporting No files were instrumented.");
                }
                return;
            }

            if (typeof coverage_data.files.branchFcn !== "undefined") {
                delete coverage_data.files.branchFcn;
            }
            if (typeof _blanket.options("reporter") === "string") {
                _blanket._loadFile(_blanket.options("reporter"));
                _blanket.customReporter(coverage_data, _blanket.options("reporter_options"));
            } else if (typeof _blanket.options("reporter") === "function") {
                _blanket.options("reporter")(coverage_data, _blanket.options("reporter_options"));
            } else if (typeof _blanket.defaultReporter === 'function') {
                _blanket.defaultReporter(coverage_data, _blanket.options("reporter_options"));
            } else {
                throw new Error("no reporter defined.");
            }
        },
        _bindStartTestRunner: function (bindEvent, startEvent) {
            if (bindEvent) {
                bindEvent(startEvent);
            } else {
                if (document.readyState === "complete") {
                    startEvent();
                } else {
                    window.addEventListener("load", startEvent, false);
                }
            }
        },
        _loadSourceFiles: function (callback) {
            var require = blanket.options("commonJS") ? blanket._commonjs.require : window.require;

            function copy(o) {
                var _copy = Object.create(Object.getPrototypeOf(o));
                var propNames = Object.getOwnPropertyNames(o);

                propNames.forEach(function (name) {
                    var desc = Object.getOwnPropertyDescriptor(o, name);
                    Object.defineProperty(_copy, name, desc);
                });

                return _copy;
            }

            if (_blanket.options("debug")) {
                console.log("BLANKET-Collecting page scripts");
            }
            var scripts = _blanket.utils.collectPageScripts();
            //_blanket.options("filter",scripts);
            if (scripts.length === 0) {
                callback();
            } else {

                //check session state
                if (sessionStorage["blanketSessionLoader"]) {
                    _blanket.blanketSession = JSON.parse(sessionStorage["blanketSessionLoader"]);
                }

                scripts.forEach(function (file, indx) {
                    _blanket.utils.cache[file] = {
                        loaded: false
                    };
                });

                var currScript = -1;
                _blanket.utils.loadAll(function (test) {
                    if (test) {
                        return typeof scripts[currScript + 1] !== 'undefined';
                    }
                    currScript++;
                    if (currScript >= scripts.length) {
                        return null;
                    }
                    return scripts[currScript];
                }, callback);
            }
        },
        beforeStartTestRunner: function (opts) {
            opts = opts || {};
            opts.checkRequirejs = typeof opts.checkRequirejs === "undefined" ? true : opts.checkRequirejs;
            opts.callback = opts.callback || function () {
                };
            opts.coverage = typeof opts.coverage === "undefined" ? true : opts.coverage;
            if (opts.coverage) {
                _blanket._bindStartTestRunner(opts.bindEvent,
                    function () {
                        _blanket._loadSourceFiles(function () {

                            var allLoaded = function () {
                                return opts.condition ? opts.condition() : _blanket.requireFilesLoaded();
                            };
                            var check = function () {
                                if (allLoaded()) {
                                    if (_blanket.options("debug")) {
                                        console.log("BLANKET-All files loaded, init start test runner callback.");
                                    }
                                    var cb = _blanket.options("testReadyCallback");

                                    if (cb) {
                                        if (typeof cb === "function") {
                                            cb(opts.callback);
                                        } else if (typeof cb === "string") {
                                            _blanket._addScript(cb);
                                            opts.callback();
                                        }
                                    } else {
                                        opts.callback();
                                    }
                                } else {
                                    setTimeout(check, 13);
                                }
                            };
                            check();
                        });
                    });
            } else {
                opts.callback();
            }
        },
        utils: {
            qualifyURL: function (url) {
                //http://stackoverflow.com/questions/470832/getting-an-absolute-url-from-a-relative-one-ie6-issue
                var a = document.createElement('a');
                a.href = url;
                return a.href;
            }
        }
    });

})(blanket);

blanket.defaultReporter = function (coverage) {
    var cssSytle = "#blanket-main {margin:2px;background:#EEE;color:#333;clear:both;font-family:'Helvetica Neue Light', 'HelveticaNeue-Light', 'Helvetica Neue', Calibri, Helvetica, Arial, sans-serif; font-size:17px;} #blanket-main a {color:#333;text-decoration:none;}  #blanket-main a:hover {text-decoration:underline;} .blanket {margin:0;padding:5px;clear:both;border-bottom: 1px solid #FFFFFF;} .bl-error {color:red;}.bl-success {color:#5E7D00;} .bl-file{width:auto;} .bl-cl{float:left;} .blanket div.rs {margin-left:50px; width:150px; float:right} .bl-nb {padding-right:10px;} #blanket-main a.bl-logo {color: #EB1764;cursor: pointer;font-weight: bold;text-decoration: none} .bl-source{ overflow-x:scroll; background-color: #FFFFFF; border: 1px solid #CBCBCB; color: #363636; margin: 25px 20px; width: 80%;} .bl-source div{white-space: pre;font-family: monospace;} .bl-source > div > span:first-child{background-color: #EAEAEA;color: #949494;display: inline-block;padding: 0 10px;text-align: center;width: 30px;} .bl-source .hit{background-color:#c3e6c7} .bl-source .miss{background-color:#e6c3c7} .bl-source span.branchWarning{color:#000;background-color:yellow;} .bl-source span.branchOkay{color:#000;background-color:transparent;}",
        successRate = 60,
        head = document.head,
        fileNumber = 0,
        body = document.body,
        headerContent,
        hasBranchTracking = Object.keys(coverage.files).some(function (elem) {
            return typeof coverage.files[elem].branchData !== 'undefined';
        }),
        bodyContent = "<div id='blanket-main'><div class='blanket bl-title'><div class='bl-cl bl-file'><a href='http://alex-seville.github.com/blanket/' target='_blank' class='bl-logo'>Blanket.js</a> results</div><div class='bl-cl rs'>Coverage (%)</div><div class='bl-cl rs'>Covered/Total Smts.</div>" + (hasBranchTracking ? "<div class='bl-cl rs'>Covered/Total Branches</div>" : "") + "<div style='clear:both;'></div></div>",
        fileTemplate = "<div class='blanket {{statusclass}}'><div class='bl-cl bl-file'><span class='bl-nb'>{{fileNumber}}.</span><a href='javascript:blanket_toggleSource(\"file-{{fileNumber}}\")'>{{file}}</a></div><div class='bl-cl rs'>{{percentage}} %</div><div class='bl-cl rs'>{{numberCovered}}/{{totalSmts}}</div>" + ( hasBranchTracking ? "<div class='bl-cl rs'>{{passedBranches}}/{{totalBranches}}</div>" : "" ) + "<div id='file-{{fileNumber}}' class='bl-source' style='display:none;'>{{source}}</div><div style='clear:both;'></div></div>";
    grandTotalTemplate = "<div class='blanket grand-total {{statusclass}}'><div class='bl-cl'>{{rowTitle}}</div><div class='bl-cl rs'>{{percentage}} %</div><div class='bl-cl rs'>{{numberCovered}}/{{totalSmts}}</div>" + ( hasBranchTracking ? "<div class='bl-cl rs'>{{passedBranches}}/{{totalBranches}}</div>" : "" ) + "<div style='clear:both;'></div></div>";

    function blanket_toggleSource(id) {
        var element = document.getElementById(id);
        if (element.style.display === 'block') {
            element.style.display = 'none';
        } else {
            element.style.display = 'block';
        }
    }


    var script = document.createElement("script");
    script.type = "text/javascript";
    script.text = blanket_toggleSource.toString().replace('function ' + blanket_toggleSource.name, 'function blanket_toggleSource');
    body.appendChild(script);

    var percentage = function (number, total) {
        return (Math.round(((number / total) * 100) * 100) / 100);
    };

    var appendTag = function (type, el, str) {
        var dom = document.createElement(type);
        dom.innerHTML = str;
        el.appendChild(dom);
    };

    function escapeInvalidXmlChars(str) {
        return str.replace(/\&/g, "&amp;")
            .replace(/</g, "&lt;")
            .replace(/\>/g, "&gt;")
            .replace(/\"/g, "&quot;")
            .replace(/\'/g, "&apos;")
            .replace(/`/g, "&grave;")
            .replace(/[$]/g, "&dollar;")
            .replace(/&/g, "&amp;");
    }

    function isBranchFollowed(data, bool) {
        var mode = bool ? 0 : 1;
        if (typeof data === 'undefined' ||
            typeof data === null ||
            typeof data[mode] === 'undefined') {
            return false;
        }
        return data[mode].length > 0;
    }

    var branchStack = [];

    function branchReport(colsIndex, src, cols, offset, lineNum) {
        var newsrc = "";
        var postfix = "";
        if (branchStack.length > 0) {
            newsrc += "<span class='" + (isBranchFollowed(branchStack[0][1], branchStack[0][1].consequent === branchStack[0][0]) ? 'branchOkay' : 'branchWarning') + "'>";
            if (branchStack[0][0].end.line === lineNum) {
                newsrc += escapeInvalidXmlChars(src.slice(0, branchStack[0][0].end.column)) + "</span>";
                src = src.slice(branchStack[0][0].end.column);
                branchStack.shift();
                if (branchStack.length > 0) {
                    newsrc += "<span class='" + (isBranchFollowed(branchStack[0][1], false) ? 'branchOkay' : 'branchWarning') + "'>";
                    if (branchStack[0][0].end.line === lineNum) {
                        newsrc += escapeInvalidXmlChars(src.slice(0, branchStack[0][0].end.column)) + "</span>";
                        src = src.slice(branchStack[0][0].end.column);
                        branchStack.shift();
                        if (!cols) {
                            return {src: newsrc + escapeInvalidXmlChars(src), cols: cols};
                        }
                    }
                    else if (!cols) {
                        return {src: newsrc + escapeInvalidXmlChars(src) + "</span>", cols: cols};
                    }
                    else {
                        postfix = "</span>";
                    }
                } else if (!cols) {
                    return {src: newsrc + escapeInvalidXmlChars(src), cols: cols};
                }
            } else if (!cols) {
                return {src: newsrc + escapeInvalidXmlChars(src) + "</span>", cols: cols};
            } else {
                postfix = "</span>";
            }
        }
        var thisline = cols[colsIndex];
        //consequent

        var cons = thisline.consequent;
        if (cons.start.line > lineNum) {
            branchStack.unshift([thisline.alternate, thisline]);
            branchStack.unshift([cons, thisline]);
            src = escapeInvalidXmlChars(src);
        } else {
            var style = "<span class='" + (isBranchFollowed(thisline, true) ? 'branchOkay' : 'branchWarning') + "'>";
            newsrc += escapeInvalidXmlChars(src.slice(0, cons.start.column - offset)) + style;

            if (cols.length > colsIndex + 1 &&
                cols[colsIndex + 1].consequent.start.line === lineNum &&
                cols[colsIndex + 1].consequent.start.column - offset < cols[colsIndex].consequent.end.column - offset) {
                var res = branchReport(colsIndex + 1, src.slice(cons.start.column - offset, cons.end.column - offset), cols, cons.start.column - offset, lineNum);
                newsrc += res.src;
                cols = res.cols;
                cols[colsIndex + 1] = cols[colsIndex + 2];
                cols.length--;
            } else {
                newsrc += escapeInvalidXmlChars(src.slice(cons.start.column - offset, cons.end.column - offset));
            }
            newsrc += "</span>";

            var alt = thisline.alternate;
            if (alt.start.line > lineNum) {
                newsrc += escapeInvalidXmlChars(src.slice(cons.end.column - offset));
                branchStack.unshift([alt, thisline]);
            } else {
                newsrc += escapeInvalidXmlChars(src.slice(cons.end.column - offset, alt.start.column - offset));
                style = "<span class='" + (isBranchFollowed(thisline, false) ? 'branchOkay' : 'branchWarning') + "'>";
                newsrc += style;
                if (cols.length > colsIndex + 1 &&
                    cols[colsIndex + 1].consequent.start.line === lineNum &&
                    cols[colsIndex + 1].consequent.start.column - offset < cols[colsIndex].alternate.end.column - offset) {
                    var res2 = branchReport(colsIndex + 1, src.slice(alt.start.column - offset, alt.end.column - offset), cols, alt.start.column - offset, lineNum);
                    newsrc += res2.src;
                    cols = res2.cols;
                    cols[colsIndex + 1] = cols[colsIndex + 2];
                    cols.length--;
                } else {
                    newsrc += escapeInvalidXmlChars(src.slice(alt.start.column - offset, alt.end.column - offset));
                }
                newsrc += "</span>";
                newsrc += escapeInvalidXmlChars(src.slice(alt.end.column - offset));
                src = newsrc;
            }
        }
        return {src: src + postfix, cols: cols};
    }

    var isUndefined = function (item) {
        return typeof item !== 'undefined';
    };

    var files = coverage.files;
    var totals = {
        totalSmts: 0,
        numberOfFilesCovered: 0,
        passedBranches: 0,
        totalBranches: 0,
        moduleTotalStatements: {},
        moduleTotalCoveredStatements: {},
        moduleTotalBranches: {},
        moduleTotalCoveredBranches: {}
    };

    // check if a data-cover-modulepattern was provided for per-module coverage reporting
    var modulePattern = _blanket.options("modulePattern");
    var modulePatternRegex = ( modulePattern ? new RegExp(modulePattern) : null );

    for (var file in files) {
        if (!files.hasOwnProperty(file)) {
            continue;
        }

        fileNumber++;

        var statsForFile = files[file],
            totalSmts = 0,
            numberOfFilesCovered = 0,
            code = [],
            i;


        var end = [];
        for (i = 0; i < statsForFile.source.length; i += 1) {
            var src = statsForFile.source[i];

            if (branchStack.length > 0 ||
                typeof statsForFile.branchData !== 'undefined') {
                if (typeof statsForFile.branchData[i + 1] !== 'undefined') {
                    var cols = statsForFile.branchData[i + 1].filter(isUndefined);
                    var colsIndex = 0;


                    src = branchReport(colsIndex, src, cols, 0, i + 1).src;

                } else if (branchStack.length) {
                    src = branchReport(0, src, null, 0, i + 1).src;
                } else {
                    src = escapeInvalidXmlChars(src);
                }
            } else {
                src = escapeInvalidXmlChars(src);
            }
            var lineClass = "";
            if (statsForFile[i + 1]) {
                numberOfFilesCovered += 1;
                totalSmts += 1;
                lineClass = 'hit';
            } else {
                if (statsForFile[i + 1] === 0) {
                    totalSmts++;
                    lineClass = 'miss';
                }
            }
            code[i + 1] = "<div class='" + lineClass + "'><span class=''>" + (i + 1) + "</span>" + src + "</div>";
        }
        totals.totalSmts += totalSmts;
        totals.numberOfFilesCovered += numberOfFilesCovered;
        var totalBranches = 0;
        var passedBranches = 0;
        if (typeof statsForFile.branchData !== 'undefined') {
            for (var j = 0; j < statsForFile.branchData.length; j++) {
                if (typeof statsForFile.branchData[j] !== 'undefined') {
                    for (var k = 0; k < statsForFile.branchData[j].length; k++) {
                        if (typeof statsForFile.branchData[j][k] !== 'undefined') {
                            totalBranches++;
                            if (typeof statsForFile.branchData[j][k][0] !== 'undefined' &&
                                statsForFile.branchData[j][k][0].length > 0 &&
                                typeof statsForFile.branchData[j][k][1] !== 'undefined' &&
                                statsForFile.branchData[j][k][1].length > 0) {
                                passedBranches++;
                            }
                        }
                    }
                }
            }
        }
        totals.passedBranches += passedBranches;
        totals.totalBranches += totalBranches;

        // if "data-cover-modulepattern" was provided,
        // track totals per module name as well as globally
        if (modulePatternRegex) {
            var moduleName = file.match(modulePatternRegex)[1];

            if (!totals.moduleTotalStatements.hasOwnProperty(moduleName)) {
                totals.moduleTotalStatements[moduleName] = 0;
                totals.moduleTotalCoveredStatements[moduleName] = 0;
            }

            totals.moduleTotalStatements[moduleName] += totalSmts;
            totals.moduleTotalCoveredStatements[moduleName] += numberOfFilesCovered;

            if (!totals.moduleTotalBranches.hasOwnProperty(moduleName)) {
                totals.moduleTotalBranches[moduleName] = 0;
                totals.moduleTotalCoveredBranches[moduleName] = 0;
            }

            totals.moduleTotalBranches[moduleName] += totalBranches;
            totals.moduleTotalCoveredBranches[moduleName] += passedBranches;
        }

        var result = percentage(numberOfFilesCovered, totalSmts);

        var output = fileTemplate.replace("{{file}}", file)
            .replace("{{percentage}}", result)
            .replace("{{numberCovered}}", numberOfFilesCovered)
            .replace(/\{\{fileNumber\}\}/g, fileNumber)
            .replace("{{totalSmts}}", totalSmts)
            .replace("{{totalBranches}}", totalBranches)
            .replace("{{passedBranches}}", passedBranches)
            .replace("{{source}}", code.join(" "));
        if (result < successRate) {
            output = output.replace("{{statusclass}}", "bl-error");
        } else {
            output = output.replace("{{statusclass}}", "bl-success");
        }
        bodyContent += output;
    }

    // create temporary function for use by the global totals reporter,
    // as well as the per-module totals reporter
    var createAggregateTotal = function (numSt, numCov, numBranch, numCovBr, moduleName) {

        var totalPercent = percentage(numCov, numSt);
        var statusClass = totalPercent < successRate ? "bl-error" : "bl-success";
        var rowTitle = ( moduleName ? "Total for module: " + moduleName : "Global total" );
        var totalsOutput = grandTotalTemplate.replace("{{rowTitle}}", rowTitle)
            .replace("{{percentage}}", totalPercent)
            .replace("{{numberCovered}}", numCov)
            .replace("{{totalSmts}}", numSt)
            .replace("{{passedBranches}}", numCovBr)
            .replace("{{totalBranches}}", numBranch)
            .replace("{{statusclass}}", statusClass);

        bodyContent += totalsOutput;
    };

    // if "data-cover-modulepattern" was provided,
    // output the per-module totals alongside the global totals
    if (modulePatternRegex) {
        for (var thisModuleName in totals.moduleTotalStatements) {
            if (totals.moduleTotalStatements.hasOwnProperty(thisModuleName)) {

                var moduleTotalSt = totals.moduleTotalStatements[thisModuleName];
                var moduleTotalCovSt = totals.moduleTotalCoveredStatements[thisModuleName];

                var moduleTotalBr = totals.moduleTotalBranches[thisModuleName];
                var moduleTotalCovBr = totals.moduleTotalCoveredBranches[thisModuleName];

                createAggregateTotal(moduleTotalSt, moduleTotalCovSt, moduleTotalBr, moduleTotalCovBr, thisModuleName);
            }
        }
    }

    createAggregateTotal(totals.totalSmts, totals.numberOfFilesCovered, totals.totalBranches, totals.passedBranches, null);
    bodyContent += "</div>"; //closing main


    appendTag('style', head, cssSytle);
    //appendStyle(body, headerContent);
    if (document.getElementById("blanket-main")) {
        document.getElementById("blanket-main").innerHTML =
            bodyContent.slice(23, -6);
    } else {
        appendTag('div', body, bodyContent);
    }
    //appendHtml(body, '</div>');
};

(function () {
    var newOptions = {};
    //http://stackoverflow.com/a/2954896
    var toArray = Array.prototype.slice;
    var scripts = toArray.call(document.scripts);
    toArray.call(scripts[scripts.length - 1].attributes)
        .forEach(function (es) {
            if (es.nodeName === "data-cover-only") {
                newOptions.filter = es.nodeValue;
            }
            if (es.nodeName === "data-cover-never") {
                newOptions.antifilter = es.nodeValue;
            }
            if (es.nodeName === "data-cover-reporter") {
                newOptions.reporter = es.nodeValue;
            }
            if (es.nodeName === "data-cover-adapter") {
                newOptions.adapter = es.nodeValue;
            }
            if (es.nodeName === "data-cover-loader") {
                newOptions.loader = es.nodeValue;
            }
            if (es.nodeName === "data-cover-timeout") {
                newOptions.timeout = es.nodeValue;
            }
            if (es.nodeName === "data-cover-modulepattern") {
                newOptions.modulePattern = es.nodeValue;
            }
            if (es.nodeName === "data-cover-reporter-options") {
                try {
                    newOptions.reporter_options = JSON.parse(es.nodeValue);
                } catch (e) {
                    if (blanket.options("debug")) {
                        throw new Error("Invalid reporter options.  Must be a valid stringified JSON object.");
                    }
                }
            }
            if (es.nodeName === "data-cover-testReadyCallback") {
                newOptions.testReadyCallback = es.nodeValue;
            }
            if (es.nodeName === "data-cover-customVariable") {
                newOptions.customVariable = es.nodeValue;
            }
            if (es.nodeName === "data-cover-flags") {
                var flags = " " + es.nodeValue + " ";
                if (flags.indexOf(" ignoreError ") > -1) {
                    newOptions.ignoreScriptError = true;
                }
                if (flags.indexOf(" autoStart ") > -1) {
                    newOptions.autoStart = true;
                }
                if (flags.indexOf(" ignoreCors ") > -1) {
                    newOptions.ignoreCors = true;
                }
                if (flags.indexOf(" branchTracking ") > -1) {
                    newOptions.branchTracking = true;
                }
                if (flags.indexOf(" sourceURL ") > -1) {
                    newOptions.sourceURL = true;
                }
                if (flags.indexOf(" debug ") > -1) {
                    newOptions.debug = true;
                }
                if (flags.indexOf(" engineOnly ") > -1) {
                    newOptions.engineOnly = true;
                }
                if (flags.indexOf(" commonJS ") > -1) {
                    newOptions.commonJS = true;
                }
                if (flags.indexOf(" instrumentCache ") > -1) {
                    newOptions.instrumentCache = true;
                }
            }
        });
    blanket.options(newOptions);

    if (typeof requirejs !== 'undefined') {
        blanket.options("existingRequireJS", true);
    }
    /* setup requirejs loader, if needed */

    if (blanket.options("commonJS")) {
        blanket._commonjs = {};
    }
})();
(function (_blanket) {
    _blanket.extend({
        utils: {
            collectPageScripts: function () {
                var toArray = Array.prototype.slice;
                var scripts = toArray.call(document.scripts);
                var selectedScripts = [], scriptNames = [];
                var filter = _blanket.options("filter");
                if (filter != null) {
                    //global filter in place, data-cover-only
                    var antimatch = _blanket.options("antifilter");
                    selectedScripts = toArray.call(document.scripts)
                        .filter(function (s) {
                            return toArray.call(s.attributes).filter(function (sn) {
                                    return sn.nodeName === "src" && _blanket.utils.matchPatternAttribute(sn.nodeValue, filter) &&
                                        (typeof antimatch === "undefined" || !_blanket.utils.matchPatternAttribute(sn.nodeValue, antimatch));
                                }).length === 1;
                        });
                } else {
                    selectedScripts = toArray.call(document.querySelectorAll("script[data-cover]"));
                }
                scriptNames = selectedScripts.map(function (s) {
                    return _blanket.utils.qualifyURL(
                        toArray.call(s.attributes).filter(
                            function (sn) {
                                return sn.nodeName === "src";
                            })[0].nodeValue);
                });
                if (!filter) {
                    _blanket.options("filter", "['" + scriptNames.join("','") + "']");
                }
                return scriptNames;
            },
            cache: {},
            cacheXhrConstructor: function () {
                var Constructor, createXhr, i, progId;
                if (typeof XMLHttpRequest !== "undefined") {
                    Constructor = XMLHttpRequest;
                    this.createXhr = function () {
                        return new Constructor();
                    };
                } else if (typeof ActiveXObject !== "undefined") {
                    Constructor = ActiveXObject;
                    for (i = 0; i < 3; i += 1) {
                        progId = progIds[i];
                        try {
                            new ActiveXObject(progId);
                            break;
                        } catch (e) {
                        }
                    }
                    this.createXhr = function () {
                        return new Constructor(progId);
                    };
                }
            },
        }
    });

    (function () {
        var require = blanket.options("commonJS") ? blanket._commonjs.require : window.require;
        var requirejs = blanket.options("commonJS") ? blanket._commonjs.requirejs : window.requirejs;
        if (!_blanket.options("engineOnly") && _blanket.options("existingRequireJS")) {

            _blanket.utils.oldloader = requirejs.load;

            requirejs.load = function (context, moduleName, url) {
                _blanket.requiringFile(url);
                _blanket.utils.getFile(url,
                    function (content) {
                        _blanket.utils.processFile(
                            content,
                            url,
                            function newLoader() {
                                context.completeLoad(moduleName);
                            },
                            function oldLoader() {
                                _blanket.utils.oldloader(context, moduleName, url);
                            }
                        );
                    }, function (err) {
                        _blanket.requiringFile();
                        throw err;
                    });
            };
        }
        // Save the XHR constructor, just in case frameworks like Sinon would sandbox it.
        _blanket.utils.cacheXhrConstructor();
    })();

})(blanket);

(function () {

    if (typeof jasmine === "undefined") {
        throw new Exception("jasmine library does not exist in global namespace!");
    }

    /**
     * based on https://raw.github.com/larrymyers/jasmine-reporters/master/src/jasmine.junit_reporter.js
     */
    var BlanketReporter = function (savePath, consolidate, useDotNotation) {

        blanket.setupCoverage();
    };
    BlanketReporter.finished_at = null; // will be updated after all files have been written

    BlanketReporter.prototype = {
        specStarted: function (spec) {
            blanket.onTestStart();
        },

        specDone: function (result) {
            var passed = result.status === "passed" ? 1 : 0;
            blanket.onTestDone(1, passed);
        },

        jasmineDone: function () {
            blanket.onTestsDone();
        },

        log: function (str) {
            var console = jasmine.getGlobal().console;

            if (console && console.log) {
                console.log(str);
            }
        }
    };

    // export public
    jasmine.BlanketReporter = BlanketReporter;

    //override existing jasmine execute
    var originalJasmineExecute = jasmine.getEnv().execute;
    jasmine.getEnv().execute = function () {
        console.log("waiting for blanket...");
    };

    blanket.beforeStartTestRunner({
        checkRequirejs: true,
        callback: function () {

            Object.keys(_$jscoverage).forEach(function (element) {
                _$jscoverage[element].forEach(function (value, index) {
                    _$jscoverage[element][index] = 0;
                });
            });

            jasmine.getEnv().addReporter(new jasmine.BlanketReporter());
            jasmine.getEnv().execute = originalJasmineExecute;
            jasmine.getEnv().execute();
        }
    });
})();
