var consoleHistory = (function () {
    var oldConsole = console;
    var logEntries = [];
    var preventDefault = false;

    function getErrors() {
        return logEntries.filter(function (log) {
            return log.level === "error";
        });
    }

    return {
        newConsole: {
            log: function () {
                arguments.level = 'log';
                logEntries.push(arguments);
                if (!preventDefault) {
                    oldConsole.log.apply(oldConsole, arguments);
                }
            },
            error: function () {
                arguments.level = 'error';
                logEntries.push(arguments);
                if (!preventDefault) {
                    oldConsole.error.apply(oldConsole, arguments);
                }
            },
            warn: function () {
                arguments.level = 'warn';
                logEntries.push(arguments);
                if (!preventDefault) {
                    oldConsole.warn.apply(oldConsole, arguments);
                }
            }
        },
        countError: function () {
            return getErrors().length;
        },
        hasError: function () {
            return getErrors().length > 0;
        },
        clear: function () {
            logEntries = [];
        },
        preventDefault: function (prevent) {
            preventDefault = prevent;

        }
    };
})();
console = consoleHistory.newConsole;