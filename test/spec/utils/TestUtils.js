var TestUtils = {};
TestUtils.openDocument = function (jsonDoc) {
    //Get editor element
    var editor = document.getElementById("editor");
    var redigeDoc = new Redige.Document().init(jsonDoc);
    while (editor.childNodes.length > 0) {
        editor.removeChild(editor.childNodes[0]);
    }
    editor.appendChild(redigeDoc);
    NumberingHandler.refreshNumbering(editor);
    Redige.setSelection();
};

TestUtils.addToEditor = function (division) {
    var editor = document.getElementById("editor");
    var documentTag = editor.querySelector("rd-document");
    documentTag.appendChild(division);
};

TestUtils.openDefaultDocument = function () {
    TestUtils.openDocument({
        id: 1,
        divisions: [
            {
                id: "1018558059",
                type: "default",
                sections: [
                    {
                        id: "S11",
                        level: "1",
                        type: "hierarchical",
                        content: "Mon Titre 1 hhahaha"
                    },
                    {
                        id: "S21",
                        level: "2",
                        type: "hierarchical",
                        content: "Mon Titre 2"
                    }
                ]
            },
            {
                id: "2",
                type: "code",
                sections: [
                    {
                        id: "S1",
                        level: "1",
                        type: "hierarchical",
                        content: "aaa"
                    },
                    {
                        id: "S2",
                        level: "2",
                        type: "hierarchical",
                        content: "Bonjour voici un élémént décoré",
                        decorators: [{
                            id: "A",
                            start: 17,
                            end: 24,
                            type: "link"
                        }, {
                            id: "B",
                            start: 7,
                            end: 20,
                            type: "bold"
                        }]
                    }
                ]
            }, {
                id: "101s8558059",
                type: "default",
                sections: [
                    {
                        id: "S11m",
                        level: "0",
                        type: "hierarchical",
                        content: "mon texte 1"
                    },
                    {
                        id: "S21m",
                        level: "0",
                        type: "hierarchical",
                        content: "Mon TExt 2"
                    },
                    {
                        id: "S21o",
                        level: "0",
                        type: "olist",
                        content: "Mon TExt 2"
                    }, {
                        id: "S2u1",
                        level: "0",
                        type: "ulist",
                        content: "Mon TExt 2"
                    }
                ]
            }]
    });
};
