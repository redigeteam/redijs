'use strict';

describe('SupprHandler:InSection', function () {

    var section1Content = "Ici est un jeux de test pour les décorateurs";
    var div;
    var section1;
    var decorator;

    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = section1Content;

        decorator = new Redige.Decorator();
        decorator.type = 'toto';
        decorator.start = 0;
        decorator.end = 1;
        section1.decorators.push(decorator);

        decorator = new Redige.Decorator();
        decorator.type = 'italic';
        decorator.start = 15;
        decorator.end = 20;
        section1.decorators.push(decorator);

        section1.refreshContent();

        div = new Redige.Division();
        div.appendChild(section1);

        TestUtils.addToEditor(div);

    });

    describe('typing Suppr', function () {
        describe('with a collapsed selection', function () {

            var event;

            function typeSuppr(offset) {

                //Set the seletion to the range start,end
                Redige.setSelection(section1.id, offset, section1.id, offset);

                event = document.createEvent('CustomEvent');
                event.initCustomEvent('keydown', true, true, null);
                event.which = 46;
                event.keyCode = 46;
                event.ctrlKey = false;
                window.getSelection().focusNode.dispatchEvent(event);

            }


            describe('nominal case (remove the following char)', function () {
                beforeEach(function () {
                    typeSuppr(11);
                });
                it('should not be prevented', function () {
                    expect(event.defaultPrevented).toEqual(false);
                });
            });
            describe('corner cases 0', function () {
                beforeEach(function () {
                    typeSuppr(0);
                });
                it('should not be prevented', function () {
                    expect(event.defaultPrevented).toEqual(false);
                });
            });
            describe('corner cases not prevented (end decorator)', function () {
                beforeEach(function () {
                    typeSuppr(1);
                });
                it('should not be prevented', function () {
                    expect(event.defaultPrevented).toEqual(false);
                });
            });
            describe('corner cases not prevented (start decorator)', function () {
                beforeEach(function () {
                    typeSuppr(15);
                });
                it('should not be prevented (input will handle the content change)', function () {
                    expect(event.defaultPrevented).toEqual(false);
                });
            });
        });
        //RangeSelectionHandler is handler by range Selection
        //InputHandler will handle the content&decorator change
    });
});

