'use strict';

describe('SupprHandler:EndSection', function () {

    var section1Content = "Ici est un jeux de test pour les décorateurs";
    var section2Content = "et le second texte est plus court";
    var div;
    var section1;
    var section2;
    var decorator;

    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = section1Content;

        decorator = new Redige.Decorator();
        decorator.type = 'toto';
        decorator.start = 0;
        decorator.end = 1;
        section1.decorators.push(decorator);

        decorator = new Redige.Decorator();
        decorator.type = 'italic';
        decorator.start = 15;
        decorator.end = 44;
        section1.decorators.push(decorator);

        section1.refreshContent();

        section2 = new Redige.Section();
        section2.content = section2Content;

        decorator = new Redige.Decorator();
        decorator.type = 'hey';
        decorator.start = 0;
        decorator.end = 5;
        section2.decorators.push(decorator);


        decorator = new Redige.Decorator();
        decorator.type = 'italic';
        decorator.start = 0;
        decorator.end = 10;
        section2.decorators.push(decorator);

        section2.refreshContent();

        div = new Redige.Division();
        div.appendChild(section1);
        div.appendChild(section2);

        TestUtils.addToEditor(div);

        var section3 = new Redige.Section();
        section3.content = "coucou";
        decorator = new Redige.Decorator();
        decorator.type = 'fake';
        section3.decorators.push(decorator);

        section3.refreshContent();

        div = new Redige.Division();
        div.appendChild(section3);

        TestUtils.addToEditor(div);
    });

    var event;

    function typeSuppr(offset, id) {

        //Set the seletion to the range start,end
        Redige.setSelection(id || section1.id, offset, id || section1.id, offset);

        event = document.createEvent('CustomEvent');
        event.initCustomEvent('keydown', true, true, null);
        event.which = 46;
        event.keyCode = 46;
        event.ctrlKey = false;
        window.getSelection().focusNode.dispatchEvent(event);
    }

    describe('typing Suppr', function () {
        describe('at the end of section', function () {
            describe('nominal case', function () {
                beforeEach(function () {
                    typeSuppr(section1Content.length);
                });
                it('should prevented default suppr', function () {
                    expect(event.defaultPrevented).toEqual(true);
                });
                it('should remove the following section', function () {
                    expect(section1.nextSibling).toEqual(null);
                });
                it('should append the content of the following section to the current selection', function () {
                    expect(section1.textContent).toEqual(section1Content + section2Content);
                });
                it('should append the decorator of the following section to the current selection', function () {
                    expect(section1.decorators.length).toEqual(4);
                });

                it('should appended decorator should have new offsets', function () {
                    expect(section1.decorators[2].start).toEqual(section1Content.length);
                    expect(section1.decorators[2].end).toEqual(5 + section1Content.length);
                });
            });
            describe('not merging decorators', function () {

                beforeEach(function () {
                    typeSuppr(section1Content.length);
                });

                it('should follow nominal rules', function () {
                    expect(event.defaultPrevented).toEqual(true);
                    expect(section1.nextSibling).toEqual(null);
                    expect(section1.textContent).toEqual(section1Content + section2Content);
                    expect(section1.decorators.length).toEqual(4);
                });

                it('should appended decorator should have new offsets', function () {
                    expect(section1.decorators[2].start).toEqual(section1Content.length);
                    expect(section1.decorators[2].end).toEqual(5 + section1Content.length);

                    expect(section1.decorators[3].start).toEqual(section1Content.length);
                    expect(section1.decorators[3].end).toEqual(10 + section1Content.length);
                });
            });

            describe('no following', function () {
                beforeEach(function () {
                    typeSuppr(section2Content.length, section2.id);
                });
                it('should prevented default suppr', function () {
                    expect(event.defaultPrevented).toEqual(true);
                });
                it('should append the content of the following section to the current selection', function () {
                    expect(section2.content).toEqual(section2Content);
                });
            });
        });
        //RangeSelectionHandler is handler by range Selection
        //InputHandler will handle the content&decorator change
    });
});