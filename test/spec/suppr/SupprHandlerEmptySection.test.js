'use strict';

describe('SupprHandler:EmptySection', function () {

    var section1;
    var section2;
    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = "";

        section2 = new Redige.Section();
        section2.content = "hey";

        var div = new Redige.Division();
        div.appendChild(section1);
        div.appendChild(section2);

        TestUtils.addToEditor(div);

    });

    var event;

    function typeSuppr(offset, id) {

        //Set the selection to the range start,end
        Redige.setSelection(id || section1.id, offset, id || section1.id, offset);

        event = document.createEvent('CustomEvent');
        event.initCustomEvent('keydown', true, true, null);
        event.which = 46;
        event.keyCode = 46;
        event.ctrlKey = false;
        window.getSelection().focusNode.dispatchEvent(event);
    }

    describe('typing Suppr', function () {
        describe('in empty section', function () {
            describe('nominal case (not the only section in doc, followed)', function () {
                beforeEach(function () {
                    typeSuppr(0);
                });
                it('should prevented default suppr', function () {
                    expect(event.defaultPrevented).toEqual(true);
                });
                it('should remove the current section', function () {
                    expect(section1.parentNode).toEqual(null);
                });
                it('should select the following section', function () {
                    expect(Redige.getDocumentSelection().startSection).toEqual(section2);
                });
            });
            describe('nominal case (not the only section in doc, preceded)', function () {
                beforeEach(function () {
                    section2.parentNode.insertBefore(section2, section1);
                    typeSuppr(0);
                });
                it('should prevented default suppr', function () {
                    expect(event.defaultPrevented).toEqual(true);
                });
                it('should remove the current section', function () {
                    expect(section1.parentNode).toEqual(null);
                });
                it('should select the previous section', function () {
                    expect(Redige.getDocumentSelection().startSection).toEqual(section2);
                });
            });
        });
        describe('nominal case (the only section)', function () {
            beforeEach(function () {
                section2.parentNode.removeChild(section2);
                typeSuppr(0);
            });
            it('should prevented default suppr', function () {
                expect(event.defaultPrevented).toEqual(true);
            });
            it('should keep the section', function () {
                expect(section1.parentNode).not.toEqual(null);
            });
        });
        //RangeSelectionHandler is handler by range Selection
        //InputHandler will handle the content&decorator change
    });

});