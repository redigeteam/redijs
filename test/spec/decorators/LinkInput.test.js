'use strict';

describe('LinkInput:insert', function () {
    var section1Content = "Ici est un jeux de test pour les décorateurs de type link";
    var div;
    var section1;
    var testUrl = 'http://jasmine.github.io/2.4/';

    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = section1Content;

        div = new Redige.Division();
        div.appendChild(section1);

        TestUtils.addToEditor(div);
    });

    function doInputLink() {
        var linkInput = document.getElementById('linkInput');
        var linkInputUrl = linkInput.getElementsByTagName('INPUT')[0];
        linkInputUrl.value = testUrl;

        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('keyup', true, false, null);
        event.which = 13;
        event.ctrlKey = false;

        linkInputUrl.dispatchEvent(event);
    }

    function clickRemoveButton() {
        var linkInput = document.getElementById('linkInput');
        var removeButton = linkInput.getElementsByClassName('linkInput-btnRemove')[0];

        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('click', true, false, null);

        removeButton.dispatchEvent(event);
    }

    describe('add a link', function () {
        describe('in an editor with content', function () {
            var nbChildren;
            beforeEach(function () {
                nbChildren = section1.childNodes.length;
                Redige.setSelection(section1.id, 11, section1.id, 15);
                Toolbar.onDecoratorButtonClick('link');
            });

            afterEach(function () {
                var popup = document.getElementById('linkInput');
                popup.parentElement.removeChild(popup);
            });

            it('should split section content in 3 spans', function () {
                expect(section1.childNodes.length).not.toEqual(nbChildren);
                expect(section1.childNodes.length).toEqual(3);
            });
            it('should add link decorator to selection', function () {
                expect(section1.decorators.length).not.toBe(0);
                expect(section1.childNodes[1].attributes.item(0).value).toEqual('link');
                expect(section1.decorators[0].start).toEqual(11);
                expect(section1.decorators[0].end).toEqual(15);
            });
            it('should open a popup', function () {
                expect(document.getElementById('linkInput')).not.toBeNull();
            });
        });
        describe('with input in an editor with content', function () {
            var nbChildren;
            beforeEach(function () {
                nbChildren = section1.childNodes.length;
                Redige.setSelection(section1.id, 11, section1.id, 15);
                Toolbar.onDecoratorButtonClick('link');
            });

            it('should persist url', function () {
                expect(section1.decorators[0].data).toBeUndefined();
                doInputLink();
                expect(section1.decorators[0].data).not.toBeUndefined();
                expect(section1.decorators[0].data).toEqual(testUrl);
            });

            it('should persist decorator', function () {
                doInputLink();
                expect(section1.decorators.length).not.toBe(0);
                expect(section1.childNodes[1].attributes.item(0).value).toEqual('link');
                expect(section1.decorators[0].start).toEqual(11);
                expect(section1.decorators[0].end).toEqual(15);
            });

            it('should remove popup', function () {
                expect(document.getElementById('linkInput')).not.toBeNull();
                doInputLink();
                expect(document.getElementById('linkInput')).toBeNull();
            });

        });
    });

    describe('remove a link', function () {
        describe('in an editor with content', function () {
            beforeEach(function () {
                Redige.setSelection(section1.id, 11, section1.id, 15);
                Toolbar.onDecoratorButtonClick('link');
                doInputLink();
                LinkHandler.handleEvent(section1.decorators[0]);
            });

            it('should remove decorator', function () {
                expect(section1.decorators[0]).not.toBeUndefined();
                clickRemoveButton();
                expect(section1.decorators[0]).toBeUndefined();
            });

            it('should merge in one span', function () {
                var nbSpans = section1.childNodes.length;
                expect(nbSpans).toEqual(3);
                clickRemoveButton();
                expect(section1.childNodes.length).not.toEqual(nbSpans);
                expect(section1.childNodes.length).toEqual(1);
            });
        });
    });

});