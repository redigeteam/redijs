'use strict';

describe('DecoratorHandler', function () {

    var section1Content = "Ici est un jeux de test pour les décorateurs";
    var div;
    var section1;

    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = section1Content;

        div = new Redige.Division();
        div.appendChild(section1);

        TestUtils.addToEditor(div);
    });

    describe('calling the DecoratorsHandler', function () {
        describe('in a single section selection', function () {

            describe('no decorator yet', function () {
                var section1Node;
                var decorator;

                function addDecorator(start, end) {
                    //create a decorator
                    decorator = new Redige.Decorator();
                    decorator.type = "bold";

                    section1Node = document.getElementById(section1.id);

                    //Set the seletion to the range start,end
                    Redige.setSelection(section1.id, start, section1.id, end);

                    //call the decoratorHandler
                    DecoratorHandler.handleEvent(decorator.type);
                }

                describe('using a range as selection', function () {

                    describe('nominal case (range 0,3)', function () {
                        beforeEach(function () {
                            addDecorator(0, 3);
                        });

                        it('should not affect the content of the section', function () {
                            //Add a decorator does not affect text content
                            expect(section1Node.content).toEqual(section1Content);
                        });

                        it('HTML content should be split in parts according to selection', function () {
                            expect(section1Node.childNodes.length).toEqual(2);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici');
                            expect(section1Node.childNodes[1].textContent).toEqual(' est un jeux de test pour les décorateurs');
                        });

                        it('the span matching the seleciton should have a new added class (classname = decorator.type)', function () {
                            expect(section1Node.childNodes[1].className).not.toEqual('bold');
                            expect(section1Node.childNodes[0].className).toEqual('bold');
                        });

                        it('the decorator model is updated', function () {
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(0);
                            expect(decorators[0].end).toEqual(3);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                    describe('range 1,2', function () {

                        beforeEach(function () {
                            addDecorator(1, 2);
                        });

                        it('should follow same rules as nominal case.', function () {
                            //Add a decorator does not affect text content
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(3);
                            expect(section1Node.childNodes[0].textContent).toEqual('I');
                            expect(section1Node.childNodes[1].textContent).toEqual('c');
                            expect(section1Node.childNodes[2].textContent).toEqual('i est un jeux de test pour les décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[2].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(1);
                            expect(decorators[0].end).toEqual(2);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                    describe('range 5,max', function () {

                        beforeEach(function () {
                            addDecorator(5, section1Content.length);
                        });

                        it('should follow same rules as nominal case.', function () {
                            //Add a decorator does not affect text content
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(2);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici e');
                            expect(section1Node.childNodes[1].textContent).toEqual('st un jeux de test pour les décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(5);
                            expect(decorators[0].end).toEqual(44);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                    describe('range 0,max', function () {

                        beforeEach(function () {
                            addDecorator(0, section1Content.length);
                        });

                        it('should follow same rules as nominal case.', function () {
                            //Add a decorator does not affect text content
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(1);
                            expect(section1Node.childNodes[0].textContent).toEqual(section1Content);
                            expect(section1Node.childNodes[0].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(0);
                            expect(decorators[0].end).toEqual(44);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });
                });

                describe('using a collapsed selection', function () {

                    describe('nominal case : (word offset : start of starting word)', function () {
                        beforeEach(function () {
                            addDecorator(0, 0);
                        });

                        it('should not affect the content of the section', function () {
                            //Add a decorator does not affect text content
                            expect(section1Node.content).toEqual(section1Content);
                        });

                        it('the word witch has the caret in should be decorated, as well as if he is selected by a range', function () {
                            expect(section1Node.childNodes.length).toEqual(2);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici');
                            expect(section1Node.childNodes[1].textContent).toEqual(' est un jeux de test pour les décorateurs');
                        });

                        it('the span matching the seleciton should have a new added class (classname = decorator.type)', function () {
                            expect(section1Node.childNodes[1].className).not.toEqual('bold');
                            expect(section1Node.childNodes[0].className).toEqual('bold');
                        });

                        it('the decorator model is updated', function () {
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(0);
                            expect(decorators[0].end).toEqual(3);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                    function nominaleRules() {
                        expect(section1Node.content).toEqual(section1Content);
                        expect(section1Node.childNodes.length).toEqual(2);
                        expect(section1Node.childNodes[0].textContent).toEqual('Ici');
                        expect(section1Node.childNodes[1].textContent).toEqual(' est un jeux de test pour les décorateurs');
                        expect(section1Node.childNodes[1].className).not.toEqual('bold');
                        expect(section1Node.childNodes[0].className).toEqual('bold');
                        var decorators = section1Node.decorators;
                        expect(decorators.length).toEqual(1);
                        expect(decorators[0].start).toEqual(0);
                        expect(decorators[0].end).toEqual(3);
                        expect(decorators[0].type).toEqual('bold');
                    }

                    describe('word offset : middle of start word', function () {
                        beforeEach(function () {
                            addDecorator(0, 0);
                        });

                        it('should verify nominal rules', function () {
                            nominaleRules();
                        });
                    });

                    describe('word offset : end os start word', function () {
                        beforeEach(function () {
                            addDecorator(3, 3);
                        });

                        it('should verify nominal rules', function () {
                            nominaleRules();
                        });
                    });

                    describe('word offset : start of middle word', function () {
                        beforeEach(function () {
                            addDecorator(4, 4);
                        });

                        it('should verify nominal rules', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(3);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici ');
                            expect(section1Node.childNodes[1].textContent).toEqual('est');
                            expect(section1Node.childNodes[2].textContent).toEqual(' un jeux de test pour les décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[2].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(4);
                            expect(decorators[0].end).toEqual(7);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });


                    describe('word offset : middle of a middle word', function () {
                        beforeEach(function () {
                            addDecorator(5, 5);
                        });

                        it('should verify nominal rules', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(3);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici ');
                            expect(section1Node.childNodes[1].textContent).toEqual('est');
                            expect(section1Node.childNodes[2].textContent).toEqual(' un jeux de test pour les décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[2].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(4);
                            expect(decorators[0].end).toEqual(7);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });


                    describe('word offset : end of a middle word', function () {
                        beforeEach(function () {
                            addDecorator(7, 7);
                        });

                        it('should verify nominal rules', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(3);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici ');
                            expect(section1Node.childNodes[1].textContent).toEqual('est');
                            expect(section1Node.childNodes[2].textContent).toEqual(' un jeux de test pour les décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[2].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(4);
                            expect(decorators[0].end).toEqual(7);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });


                    describe('word offset : start of end word', function () {
                        beforeEach(function () {
                            addDecorator(33, 33);
                        });

                        it('should verify nominal rules', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(2);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici est un jeux de test pour les ');
                            expect(section1Node.childNodes[1].textContent).toEqual('décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(33);
                            expect(decorators[0].end).toEqual(section1Content.length);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                    describe('word offset : middle of end word', function () {
                        beforeEach(function () {
                            addDecorator(section1Content.length - 2, section1Content.length - 2);
                        });

                        it('should verify nominal rules', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(2);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici est un jeux de test pour les ');
                            expect(section1Node.childNodes[1].textContent).toEqual('décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(33);
                            expect(decorators[0].end).toEqual(section1Content.length);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                    describe('word offset : end of end word', function () {
                        beforeEach(function () {
                            addDecorator(section1Content.length, section1Content.length);
                        });

                        it('should verify nominal rules', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(2);
                            expect(section1Node.childNodes[0].textContent).toEqual('Ici est un jeux de test pour les ');
                            expect(section1Node.childNodes[1].textContent).toEqual('décorateurs');
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            expect(section1Node.childNodes[1].className).toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(1);
                            expect(decorators[0].start).toEqual(33);
                            expect(decorators[0].end).toEqual(section1Content.length);
                            expect(decorators[0].type).toEqual('bold');
                        });
                    });

                });
            });

            describe('already a decorator', function () {
                var section1Node;
                var decorator;

                function addDecorator(start, end, type) {
                    //create a decorator
                    decorator = new Redige.Decorator();
                    decorator.type = type;

                    section1Node = document.getElementById(section1.id);

                    //Set the seletion to the range start,end
                    Redige.setSelection(section1.id, start, section1.id, end);

                    //call the decoratorHandler
                    DecoratorHandler.handleEvent(decorator.type);
                }

                describe('using a collapsed selection', function () {

                    describe('nominal case : the caret is in a word with a decorator', function () {
                        beforeEach(function () {
                            //create the first decorator
                            addDecorator(0, 10, 'bold');
                            //call the decoratorHandler inside the range of the first one.
                            addDecorator(5, 5, 'bold');
                        });

                        it('should remove the existing decorator if it has the same type', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(1);
                            expect(section1Node.childNodes[0].className).not.toEqual('bold');
                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(0);
                        });

                    });


                    describe('the caret is in a word with a decorator with a different type', function () {
                        beforeEach(function () {
                            //create the first decorator
                            addDecorator(0, 10, 'bold');
                            //call the decoratorHandler inside the range of the first one.
                            addDecorator(5, 5, 'italic');
                        });

                        it('should add the new one', function () {
                            expect(section1Node.content).toEqual(section1Content);
                            expect(section1Node.childNodes.length).toEqual(4);

                            expect(section1Node.childNodes[0].innerHTML).toEqual('Ici ');
                            expect(section1Node.childNodes[0].className).toEqual('bold');

                            expect(section1Node.childNodes[1].innerHTML).toEqual('est');
                            expect(section1Node.childNodes[1].className).toEqual('bold italic');

                            expect(section1Node.childNodes[2].innerHTML).toEqual(' un');
                            expect(section1Node.childNodes[2].className).toEqual('bold');

                            expect(section1Node.childNodes[3].innerHTML).toEqual(' jeux de test pour les décorateurs');
                            expect(section1Node.childNodes[3].className).toEqual('');

                            var decorators = section1Node.decorators;
                            expect(decorators.length).toEqual(2);
                            expect(decorators[0].type).toEqual('bold');
                            expect(decorators[0].start).toEqual(0);
                            expect(decorators[0].end).toEqual(10);
                            expect(decorators[1].type).toEqual('italic');
                            expect(decorators[1].start).toEqual(4);
                            expect(decorators[1].end).toEqual(7);
                        });
                    });
                    describe('using a range selection', function () {
                        describe('the selection is in a word with a decorator with a different type', function () {
                            beforeEach(function () {
                                //create the first decorator
                                addDecorator(0, 10, 'bold');
                                //call the decoratorHandler inside the range of the first one.
                                addDecorator(5, 8, 'italic');
                            });

                            it('should add the new one', function () {
                                expect(section1Node.content).toEqual(section1Content);
                                expect(section1Node.childNodes.length).toEqual(4);

                                expect(section1Node.childNodes[0].innerHTML).toEqual('Ici e');
                                expect(section1Node.childNodes[0].className).toEqual('bold');

                                expect(section1Node.childNodes[1].innerHTML).toEqual('st ');
                                expect(section1Node.childNodes[1].className).toEqual('bold italic');

                                expect(section1Node.childNodes[2].innerHTML).toEqual('un');
                                expect(section1Node.childNodes[2].className).toEqual('bold');

                                expect(section1Node.childNodes[3].innerHTML).toEqual(' jeux de test pour les décorateurs');
                                expect(section1Node.childNodes[3].className).toEqual('');

                                var decorators = section1Node.decorators;
                                expect(decorators.length).toEqual(2);
                                expect(decorators[0].type).toEqual('bold');
                                expect(decorators[0].start).toEqual(0);
                                expect(decorators[0].end).toEqual(10);
                                expect(decorators[1].type).toEqual('italic');
                                expect(decorators[1].start).toEqual(5);
                                expect(decorators[1].end).toEqual(8);
                            });
                        });

                        describe('the selection is next to a word with a decorator with a different type', function () {
                            beforeEach(function () {
                                //create the first decorator
                                addDecorator(0, 10, 'bold');
                                //call the decoratorHandler inside the range of the first one.
                                addDecorator(15, 20, 'italic');
                            });

                            it('should add the new one', function () {
                                expect(section1Node.content).toEqual(section1Content);
                                expect(section1Node.childNodes.length).toEqual(4);

                                expect(section1Node.childNodes[0].innerHTML).toEqual('Ici est un');
                                expect(section1Node.childNodes[0].className).toEqual('bold');

                                expect(section1Node.childNodes[1].innerHTML).toEqual(' jeux');
                                expect(section1Node.childNodes[1].className).toEqual('');

                                expect(section1Node.childNodes[2].innerHTML).toEqual(' de t');
                                expect(section1Node.childNodes[2].className).toEqual('italic');

                                expect(section1Node.childNodes[3].innerHTML).toEqual('est pour les décorateurs');
                                expect(section1Node.childNodes[3].className).toEqual('');

                                var decorators = section1Node.decorators;
                                expect(decorators.length).toEqual(2);
                                expect(decorators[0].type).toEqual('bold');
                                expect(decorators[0].start).toEqual(0);
                                expect(decorators[0].end).toEqual(10);
                                expect(decorators[1].type).toEqual('italic');
                                expect(decorators[1].start).toEqual(15);
                                expect(decorators[1].end).toEqual(20);
                            });
                        });
                        describe('the selection next to a word with a decorator with a same type', function () {
                            beforeEach(function () {
                                //create the first decorator
                                addDecorator(0, 10, 'bold');
                                //call the decoratorHandler inside the range of the first one.
                                addDecorator(15, 20, 'bold');
                            });

                            it('should add the new one', function () {
                                expect(section1Node.content).toEqual(section1Content);
                                expect(section1Node.childNodes.length).toEqual(4);

                                expect(section1Node.childNodes[0].innerHTML).toEqual('Ici est un');
                                expect(section1Node.childNodes[0].className).toEqual('bold');

                                expect(section1Node.childNodes[1].innerHTML).toEqual(' jeux');
                                expect(section1Node.childNodes[1].className).toEqual('');

                                expect(section1Node.childNodes[2].innerHTML).toEqual(' de t');
                                expect(section1Node.childNodes[2].className).toEqual('bold');

                                expect(section1Node.childNodes[3].innerHTML).toEqual('est pour les décorateurs');
                                expect(section1Node.childNodes[3].className).toEqual('');

                                var decorators = section1Node.decorators;
                                expect(decorators.length).toEqual(2);
                                expect(decorators[0].type).toEqual('bold');
                                expect(decorators[0].start).toEqual(0);
                                expect(decorators[0].end).toEqual(10);
                                expect(decorators[1].type).toEqual('bold');
                                expect(decorators[1].start).toEqual(15);
                                expect(decorators[1].end).toEqual(20);
                            });
                        });
                        describe('the selection overlapp a word with a decorator with a same type', function () {
                            beforeEach(function () {
                                //create the first decorator
                                addDecorator(0, 10, 'bold');
                                //call the decoratorHandler inside the range of the first one.
                                addDecorator(8, 20, 'bold');
                            });

                            it('should add the new one', function () {
                                expect(section1Node.content).toEqual(section1Content);
                                expect(section1Node.childNodes.length).toEqual(2);

                                expect(section1Node.childNodes[0].innerHTML).toEqual('Ici est un jeux de t');
                                expect(section1Node.childNodes[0].className).toEqual('bold');

                                expect(section1Node.childNodes[1].innerHTML).toEqual('est pour les décorateurs');
                                expect(section1Node.childNodes[1].className).toEqual('');

                                var decorators = section1Node.decorators;
                                expect(decorators.length).toEqual(1);
                                expect(decorators[0].type).toEqual('bold');
                                expect(decorators[0].start).toEqual(0);
                                expect(decorators[0].end).toEqual(20);
                            });
                        });

                        describe('the selection overlapp a word with a decorator with a same type', function () {
                            beforeEach(function () {
                                //create the first decorator
                                addDecorator(5, 10, 'bold');
                                //call the decoratorHandler inside the range of the first one.
                                addDecorator(3, 20, 'bold');
                            });

                            it('should add the new one', function () {
                                expect(section1Node.content).toEqual(section1Content);
                                expect(section1Node.childNodes.length).toEqual(3);

                                expect(section1Node.childNodes[0].innerHTML).toEqual('Ici');
                                expect(section1Node.childNodes[0].className).toEqual('');

                                expect(section1Node.childNodes[1].innerHTML).toEqual(' est un jeux de t');
                                expect(section1Node.childNodes[1].className).toEqual('bold');

                                expect(section1Node.childNodes[2].innerHTML).toEqual('est pour les décorateurs');
                                expect(section1Node.childNodes[2].className).toEqual('');

                                var decorators = section1Node.decorators;
                                expect(decorators.length).toEqual(1);
                                expect(decorators[0].type).toEqual('bold');
                                expect(decorators[0].start).toEqual(3);
                                expect(decorators[0].end).toEqual(20);
                            });
                        });
                    });
                });
            });
        });
    });
});

