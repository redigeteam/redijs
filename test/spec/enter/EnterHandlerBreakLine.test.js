'use strict';

describe('EnterHandler:breakline', function () {

    var section1Content = "Ici est un jeux de test pour les décorateurs";
    var section1;

    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = section1Content;

        var decorator = new Redige.Decorator();
        decorator.type = 'toto';
        decorator.start = 0;
        decorator.end = 1;
        section1.decorators.push(decorator);

        decorator = new Redige.Decorator();
        decorator.type = 'bold';
        decorator.start = 2;
        decorator.end = 5;
        section1.decorators.push(decorator);


        decorator = new Redige.Decorator();
        decorator.type = 'italic';
        decorator.start = 15;
        decorator.end = 20;
        section1.decorators.push(decorator);

        section1.refreshContent();

        var div = new Redige.Division();
        div.appendChild(section1);

        TestUtils.addToEditor(div);

    });

    describe('typing Enter', function () {
        describe('with a collapsed selection', function () {

            function typeEnter(offset) {

                //Set the seletion to the range start,end
                Redige.setSelection(section1.id, offset, section1.id, offset);

                var event = document.createEvent('CustomEvent');
                event.initCustomEvent('keypress', true, false, null);
                event.which = 13;
                event.ctrlKey = false;
                window.getSelection().focusNode.dispatchEvent(event);
            }

            var decorators;
            var section1decorators;
            describe('nominal case (split middle section)', function () {

                beforeEach(function () {
                    typeEnter(3);
                    section1decorators = section1.decorators;
                    decorators = section1.nextSibling.decorators;
                });
                it('should split the current content', function () {
                    expect(section1.content).toEqual('Ici');
                    expect(section1.nextSibling.content).toEqual(' est un jeux de test pour les décorateurs');
                });

                it('should not modify previous decorators', function () {
                    expect(section1decorators.length).toEqual(2);
                    //before decorator did not change
                    expect(section1decorators[0].start).toEqual(0);
                    expect(section1decorators[0].end).toEqual(1);
                });

                it('should split the current decorators', function () {
                    expect(section1decorators.length).toEqual(2);

                    expect(section1decorators[1].start).toEqual(2);
                    expect(section1decorators[1].end).toEqual(3);
                });
                it('should make split decorators following the content', function () {
                    expect(decorators.length).toEqual(2);
                    expect(decorators[0].start).toEqual(0);
                    expect(decorators[0].end).toEqual(2);
                });

                it('should make followed decorators following the content', function () {
                    expect(decorators.length).toEqual(2);
                    expect(decorators[1].start).toEqual(12);
                    expect(decorators[1].end).toEqual(17);
                });
            });

            describe('split section from 0', function () {
                beforeEach(function () {
                    typeEnter(0);
                    decorators = section1.nextSibling.decorators;
                    section1decorators = section1.decorators;
                });
                it('should split the current and leave a x01 has content', function () {
                    expect(section1.textContent).toEqual("\x01");
                    expect(section1.nextSibling.content).toEqual(section1Content);
                });

                it('should move all decorators to the new section', function () {
                    expect(section1decorators.length).toEqual(0);
                    expect(decorators.length).toEqual(3);

                    expect(decorators[0].start).toEqual(0);
                    expect(decorators[0].end).toEqual(1);

                    expect(decorators[1].start).toEqual(2);
                    expect(decorators[1].end).toEqual(5);

                    expect(decorators[2].start).toEqual(15);
                    expect(decorators[2].end).toEqual(20);
                });
            });

            describe('split section from the end', function () {
                beforeEach(function () {
                    typeEnter(section1Content.length);
                    decorators = section1.nextSibling.decorators;
                });
                it('should split the current and leave a x01 has content', function () {
                    expect(section1.content).toEqual(section1Content);
                    expect(section1.nextSibling.textContent).toEqual("\x01");
                });
                it('should create a new section with no decorators', function () {
                    expect(decorators.length).toEqual(0);
                });
            });
        });
    });
});

