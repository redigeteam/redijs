'use strict';

describe('EnterHandler:breakline', function () {

    var section1Content = "Ici est un jeux de test pour les décorateurs";
    var section1;

    beforeEach(function () {
        Redige.reset();
        section1 = new Redige.Section();
        section1.content = section1Content;
        section1.type = 'olist';

        var div = new Redige.Division();
        div.appendChild(section1);

        TestUtils.addToEditor(div);

    });

    function typeEnter(offset) {

        //Set the seletion to the range start,end
        Redige.setSelection(section1.id, offset, section1.id, offset);

        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('keypress', true, false, null);
        event.which = 13;
        event.ctrlKey = false;
        window.getSelection().focusNode.dispatchEvent(event);
    }

    describe('typing Enter', function () {
        describe('in middle of olist/ulist (selection collapsed)', function () {
            describe('nominal case (olist:level0)', function () {

                var newSection;
                beforeEach(function () {
                    typeEnter(3);
                    newSection = section1.nextSibling;
                });

                it('should normally split the current section', function () {
                    expect(section1.content).toEqual('Ici');
                    expect(section1.nextSibling.content).toEqual(' est un jeux de test pour les décorateurs');
                });
                it('should create the new section with the same type list section', function () {
                    expect(section1.nextSibling.type).toEqual('olist');
                });

                it('should create the new section with the same type list section', function () {
                    expect(section1.nextSibling.level).toEqual('0');
                });
            });
            describe('level 3 / ulist', function () {

                var newSection;
                beforeEach(function () {
                    section1.type = 'ulist';
                    section1.level = '3';
                    typeEnter(3);
                    newSection = section1.nextSibling;
                });

                it('should normally split the current section', function () {
                    expect(section1.content).toEqual('Ici');
                    expect(section1.nextSibling.content).toEqual(' est un jeux de test pour les décorateurs');
                });
                it('should create the new section with the same type list section', function () {
                    expect(section1.nextSibling.type).toEqual('ulist');
                });

                it('should create the new section with the same type list section', function () {
                    expect(section1.nextSibling.level).toEqual(section1.level);
                });
            });
        });

        describe('in an empty xlist section', function () {
            describe('nominal case (olist:level0)', function () {

                var newSection;
                beforeEach(function () {
                    section1.content = "";
                    section1.refreshContent();
                    typeEnter(0);
                    newSection = section1.nextSibling;
                });

                it('should not add new section', function () {
                    expect(newSection).toEqual(null);
                });
                it('should change the section to default level 0', function () {
                    expect(section1.type).toEqual('hierarchical');
                    expect(section1.level).toEqual('0');
                });
            });
            describe('nominal case (ulist:level3)', function () {
                var newSection;
                beforeEach(function () {
                    section1.content = "";
                    section1.refreshContent();
                    typeEnter(0);
                    newSection = section1.nextSibling;
                });

                it('should not add new section', function () {
                    expect(newSection).toEqual(null);
                });
                it('should change the section to default level 0', function () {
                    expect(section1.type).toEqual('hierarchical');
                    expect(section1.level).toEqual('0');
                });
            });
        });
    });
});

