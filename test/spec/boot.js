'use strict';

describe('Editor ping', function () {

    beforeEach(function () {
        TestUtils.openDefaultDocument();
        consoleHistory.clear();
    });

    /**
     * check if Redige has been loaded.
     */
    it('should have a Redige var as global', function () {
        expect(Redige).not.toBe(undefined);
        expect(consoleHistory.hasError()).toBe(false);
    });

    /**
     * check if the editor template has been included in
     * the test file.
     */
    it('should find an #editor', function () {
        var editor = document.getElementById('editor');
        expect(editor).not.toBe(null);
        expect(editor).not.toBe(undefined);
    });

    /**
     * check if Redige has been initialized with a document
     */
    it('should find a document structure', function () {
        var editor = document.getElementById('editor');
        expect(editor.querySelector("rd-document")).not.toBe(null);
        expect(editor.querySelector("rd-division")).not.toBe(null);
        expect(editor.querySelector("rd-section")).not.toBe(null);
    });

});
