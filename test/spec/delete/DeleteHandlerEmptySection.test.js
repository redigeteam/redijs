'use strict';

describe('DeleteHandler:EmptySection', function () {
    var div;
    var section1;
    var section2;
    var section3;
    var decorator;
    var decoratorStartIndex = 10;
    var decoratorEndIndex = 14;
    var section2content = "Section 2";

    beforeEach(function () {
        Redige.reset();

        section1 = new Redige.Section();
        section1.content = "Section 1";

        section2 = new Redige.Section();
        section2.content = section2content;

        section3 = new Redige.Section();
        section3.content = "Section 3 with decorators";

        decorator = new Redige.Decorator();
        decorator.type = 'bold';
        decorator.start = decoratorStartIndex;
        decorator.end = decoratorEndIndex;
        section3.decorators.push(decorator);
        section3.refreshContent();

        div = new Redige.Division();
        div.appendChild(section1);
        div.appendChild(section2);
        div.appendChild(section3);

        TestUtils.addToEditor(div);

    });

    var event;

    function typeDelete(offset, id) {
        //Set the selection to the range start,end
        Redige.setSelection(id || section1.id, offset, id || section1.id, offset);

        event = document.createEvent('CustomEvent');
        event.initCustomEvent('keydown', true, true, null);
        event.which = 8;
        event.keyCode = 8;
        event.ctrlKey = false;
        window.getSelection().focusNode.dispatchEvent(event);

        if (offset !== 0) {
            var oldContent = window.getSelection().focusNode.textContent;
            var newContent = oldContent.substring(0, offset - 1) + oldContent.substring(offset, oldContent.length);
            window.getSelection().focusNode.textContent = newContent;
            event = document.createEvent('CustomEvent');
            event.initCustomEvent('input', true, true, null);
            Redige.setSelection(id || section1.id, offset, id || section1.id, offset);
            window.getSelection().focusNode.dispatchEvent(event);
        }

    }

    function typeEnter(offset, id) {
        //Set the selection to the range start,end
        Redige.setSelection(id || section1.id, offset, id || section1.id, offset);

        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('keypress', true, false, null);
        event.which = 13;
        event.keyCode = 13;
        event.ctrlKey = false;
        window.getSelection().focusNode.dispatchEvent(event);
    }

    describe('typing Delete', function () {
        describe('in not empty section', function () {
            describe('nominal case (not the only section in doc, followed)', function () {
                var divContentLength;
                beforeEach(function () {
                    typeDelete(0);
                    divContentLength = section1.content.length + section2.content.length;
                });

                it('should prevented default delete', function () {
                    expect(event.defaultPrevented).toEqual(true);
                });
                it('should remove anything', function () {
                    expect(section1.content.length + section2.content.length).toEqual(divContentLength);
                });
            });
            describe('nominal case (not the only section in doc, preceded)', function () {
                beforeEach(function () {
                    typeDelete(0, section2.id);
                });

                it('should have one section (DeleteHandler.mergeSection should be called)', function () {
                    /*
                     The second section should be null and merged with the first one.
                     Here the content of the two section is merged but the second one is kept but not displayed on the browser
                     We should test if mergeSection is called in DeleteHandler thanks to Jasmine.spies
                     */
                    expect(section2.parentNode).toBe(null);
                });
            });
        });
        describe('in section with decorators and after typing Enter', function () {
            describe('borderline case', function () {
                /*
                 Cas de test :
                 - (2 sections non vides) curseur en début de 2ème section, presse 2fois entrée, puis Delete jusqu'à fusionner les 2 sections
                 et supprimer le dernier caractère de la première, puis entrée --> Révèle que les indices des décorateurs ont changés
                 */
                beforeEach(function () {
                    typeEnter(0, section3.id);
                    typeEnter(0, Redige.getDocumentSelection().startSection.id);
                    for (var i = 0; i < 3; i++) {
                        typeDelete(0, Redige.getDocumentSelection().startSection.id);
                    }
                    typeDelete(section2content.length, section2.id);
                    typeEnter(section2content.length, section2.id);

                });
                it('should keep decorators index', function () {
                    expect(section3.decorators[0].start).toEqual(decoratorStartIndex - 1);
                    expect(section3.decorators[0].end).toEqual(decoratorEndIndex - 1);
                });
            });
        });
    });
});