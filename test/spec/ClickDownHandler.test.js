'use strict';

describe('ClickDownHandler', function () {

    beforeEach(function () {
        TestUtils.openDefaultDocument();
    });

    describe('ClickDownHandler.onHTMLClick', function () {

        it('should set the selection in the document', function () {
            var html = document.getElementsByTagName('HTML')[0];
            Redige.dispatchEventByName(html, 'mousedown');
            var result = window.getSelection().anchorNode;
            expect(result.nodeName).toEqual('#text');
            expect(result.parentNode.parentNode.classList.contains('section')).toBe(true);
            expect(result.parentNode.parentNode.nextSibling).toBe(null);
        });

    });

    describe('ClickDownHandler.onBodyClick', function () {

        it('should set the selection in the document', function () {
            var html = document.getElementsByTagName('BODY')[0];
            Redige.dispatchEventByName(html, 'mousedown');
            var result = window.getSelection().anchorNode;
            expect(result.nodeName).toEqual('#text');
            expect(result.parentNode.parentNode.classList.contains('section')).toBe(true);
            expect(result.parentNode.parentNode.nextSibling).toBe(null);

        });

    });

    describe('ClickDownHandler.onFirstSectionClick', function () {

        it('should not select the last one', function (done) {
            var firstSection = document.querySelectorAll(".section")[0];
            var evt = document.createEvent("MouseEvents");
            evt.initMouseEvent('mousedown', true, true, document.defaultView, 0, 0, 0, 0, 0, false, false, false, 0, null, null);
            firstSection.dispatchEvent(evt);
            //Anyway the click event is dispatched. That test that clickDownHandler does nothing
            expect(window.getSelection().anchorNode).toBe(null);
            done();

        });

    });

});
