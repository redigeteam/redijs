'use strict';

describe('import HTML', function () {

    function parse(html) {
        var node = document.createElement('div');
        node.innerHTML = html;
        return HTMLParser.parse(node);
    }

    function expectLI(obj, type, level, content) {
        expect(obj.level).toEqual(level);
        expect(obj.type).toEqual(type);
        expect(obj.content.trim()).toEqual(content);
    }

    describe('UL', function () {
        describe('flat simple ul', function () {
            it('should create a section by li with the same level & type', function () {
                var doc = parse('<div><ul><li>ulist 1</li><li>ulist 2</li><li>ulist 3</li></ul></div>');
                expect(doc.childNodes.length).toEqual(1);
                var sections = doc.childNodes[0].childNodes;
                expect(sections.length).toEqual(3);

                expectLI(sections[0], 'ulist', '0', 'ulist 1');
                expectLI(sections[1], 'ulist', '0', 'ulist 2');
                expectLI(sections[2], 'ulist', '0', 'ulist 3');

            });
        });
        describe('flat 2 level ul', function () {
            it('should create a section by li with the same level & type', function () {
                var doc = parse(`<div>
                                        <ul>
                                            <li>ulist 1
                                                <ul>
                                                    <li>ulist 1.1</li>
                                                    <li>ulist 1.2</li>
                                                    <li>ulist 1.3</li>
                                                </ul>
                                            </li>
                                            <li>ulist 2</li>
                                            <li>ulist 3</li>
                                        </ul>
                                     </div>`);
                expect(doc.childNodes.length).toEqual(1);
                var sections = doc.childNodes[0].childNodes;
                expect(sections.length).toEqual(6);


                expectLI(sections[0], 'ulist', '0', 'ulist 1');
                expectLI(sections[1], 'ulist', '1', 'ulist 1.1');
                expectLI(sections[2], 'ulist', '1', 'ulist 1.2');
                expectLI(sections[3], 'ulist', '1', 'ulist 1.3');
                expectLI(sections[4], 'ulist', '0', 'ulist 2');
                expectLI(sections[5], 'ulist', '0', 'ulist 3');
            });
        });

        describe('flat 3 level ul', function () {
            it('should create a section by li with the same level & type', function () {
                var doc = parse(`<div>
                        <ul>
                        <li>ulist 1
                            <ul>
                                <li>ulist 1.1</li>
                                <li>ulist 1.2</li> 
                                <li>ulist 1.3 
                                    <ul> 
                                        <li>ulist 1.3.1</li> 
                                        <li>ulist 1.3.2</li> 
                                        <li>ulist 1.3.3</li> 
                                    </ul> 
                                </li> 
                                <li>ulist 1.4</li> 
                            </ul> 
                        </li> 
                        <li>ulist 2</li>
                        </ul> 
                        </div>`);
                expect(doc.childNodes.length).toEqual(1);
                var sections = doc.childNodes[0].childNodes;

                expect(sections.length).toEqual(9);

                expectLI(sections[0], 'ulist', '0', 'ulist 1');
                expectLI(sections[1], 'ulist', '1', 'ulist 1.1');
                expectLI(sections[2], 'ulist', '1', 'ulist 1.2');
                expectLI(sections[3], 'ulist', '1', 'ulist 1.3');
                expectLI(sections[4], 'ulist', '2', 'ulist 1.3.1');
                expectLI(sections[5], 'ulist', '2', 'ulist 1.3.2');
                expectLI(sections[6], 'ulist', '2', 'ulist 1.3.3');
                expectLI(sections[7], 'ulist', '1', 'ulist 1.4');
                expectLI(sections[8], 'ulist', '0', 'ulist 2');
            });
        });
    });
    describe('mixed', function () {
        describe('mixed following', function () {
            it('should create a section by li with the same level & type', function () {
                var doc = parse('<div><ul><li>ulist 1</li><li>ulist 2</li><li>ulist 3</li></ul><ol><li>olist 1</li><li>olist 2</li><li>olist 3</li></ol></div>');
                expect(doc.childNodes.length).toEqual(1);
                var sections = doc.childNodes[0].childNodes;
                expect(sections.length).toEqual(6);

                expectLI(sections[0], 'ulist', '0', 'ulist 1');
                expectLI(sections[1], 'ulist', '0', 'ulist 2');
                expectLI(sections[2], 'ulist', '0', 'ulist 3');
                expectLI(sections[3], 'olist', '0', 'olist 1');
                expectLI(sections[4], 'olist', '0', 'olist 2');
                expectLI(sections[5], 'olist', '0', 'olist 3');

            });

        });
        describe('mixed nested', function () {
            it('should create a section by li with the same level & type', function () {
                var doc = parse('<div><ul><li>ulist 1</li><li>ulist 2<ol><li>olist 1</li><li>olist 2</li><li>olist 3</li></ol></li><li>ulist 3</li></ul></div>');
                expect(doc.childNodes.length).toEqual(1);
                var sections = doc.childNodes[0].childNodes;
                expect(sections.length).toEqual(6);

                expectLI(sections[0], 'ulist', '0', 'ulist 1');
                expectLI(sections[1], 'ulist', '0', 'ulist 2');
                expectLI(sections[2], 'olist', '1', 'olist 1');
                expectLI(sections[3], 'olist', '1', 'olist 2');
                expectLI(sections[4], 'olist', '1', 'olist 3');
                expectLI(sections[5], 'ulist', '0', 'ulist 3');
            });
        });
    });
});