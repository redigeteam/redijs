'use strict';

describe('ImageInput:insert', function () {

    var imageUrl = 'http://soocurious.com/fr/wp-content/uploads/2014/07/personne-redige-rapport.jpg';
    var defaultDiv, inputDiv, imageDiv, selection, inputField;

    beforeEach(function () {
        Redige.reset();

        var defaultDivId = document.getElementsByTagName('rd-division')[0].id;
        defaultDiv = document.getElementById(defaultDivId);
        Redige.setSelection(defaultDiv.firstChild.id, 0);


        selection = Redige.getDocumentSelection();
    });

    function insertImageUrl(url) {
        inputField = document.querySelector('input');
        inputField.value = url || imageUrl;
        inputDiv = document.querySelector('.imageInput');
    }

    function isImageUrl(imgPath) {
        return (imgPath.toLowerCase().match(/^http(s)?:\/{2}(.)*(jpeg|jpg|gif|svg|png)$/) !== null);
    }

    function typeEnter() {
        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('keyup', true, false, null);
        event.which = 13;
        event.ctrlKey = false;
        inputField.dispatchEvent(event);
    }

    describe('insert image from URL', function () {
        describe('in empty editor', function () {
            it('should create a div with inputs', function () {
                expect(defaultDiv.childElementCount).toEqual(1);
                Toolbar.onImageButtonClick();
                expect(defaultDiv.childElementCount).toEqual(2);
            });

            it('should create a div with image and replace the one with inputs', function () {
                Toolbar.onImageButtonClick();
                var nbElt = defaultDiv.childElementCount;
                insertImageUrl();
                typeEnter();
                expect(defaultDiv.childElementCount).toEqual(nbElt);
                expect(document.getElementById(inputDiv.id)).toBe(null);
                expect(document.getElementsByClassName('section image image-wrapper').length).toEqual(1);
            });

            it('should be a div with img tag in it', function () {
                Toolbar.onImageButtonClick();
                insertImageUrl();
                typeEnter();
                imageDiv = document.getElementsByTagName('rd-image')[0];
                expect(imageDiv.firstChild.tagName).toEqual("IMG");
            });

            it('should create img with url as src', function () {
                Toolbar.onImageButtonClick();
                insertImageUrl();
                typeEnter();
                expect(isImageUrl(selection.startSection.nextSibling.querySelector("img").src)).toBe(true);
            });

            it('should not create img if not image URL', function () {
                Toolbar.onImageButtonClick();
                insertImageUrl("fakeURL");
                typeEnter();
                expect(inputDiv.parentNode).not.toBe(null);
                expect(document.getElementsByClassName('section image image-wrapper').length).toEqual(0);
            });
        });
        describe('in a section', function () {
            var sectionToInsert;
            beforeEach(function () {
                TestUtils.openDefaultDocument();
                sectionToInsert = document.getElementsByClassName("section hierarchical level-0")[0];
                Redige.setSelection(sectionToInsert.id, 2);
                selection = Redige.getDocumentSelection();
            });

            it('should create a div with inputs after current section', function () {
                expect(selection.startSection.nextSibling.className.includes('imageInput')).toBe(false);
                Toolbar.onImageButtonClick();
                expect(selection.startSection.nextSibling.className.includes('imageInput')).toBe(true);

            });

            it('should create a div with image after current section', function () {
                Toolbar.onImageButtonClick();
                insertImageUrl();
                typeEnter();
                expect(selection.startSection.nextSibling.firstChild.tagName).toEqual('IMG');
            });

            it('should create a div with image after current section and have only one div more', function () {
                expect(selection.startSection.parentElement.childElementCount).toBe(4);
                Toolbar.onImageButtonClick();
                expect(selection.startSection.parentElement.childElementCount).toBe(5);
                insertImageUrl();
                expect(selection.startSection.parentElement.childElementCount).toBe(5);
            });

            it('should create img with url as src', function () {
                Toolbar.onImageButtonClick();
                insertImageUrl();
                typeEnter();
                expect(isImageUrl(selection.startSection.nextSibling.firstChild.src)).toBe(true);
            });
        });
    });
});