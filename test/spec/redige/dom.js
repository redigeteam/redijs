'use strict';

describe('Redige selection utils', function () {

    beforeEach(function () {
        Redige.reset();
        consoleHistory.clear();
    });

    it('should append a node after another one', function () {

        var section = document.createElement('p');

        var span0 = document.createElement('span');
        span0.textContent = "a";
        section.appendChild(span0);

        var span1 = document.createElement('span');
        span1.innerHTML = "b";

        Redige.insertAfter(span1, span0);

        expect(section.innerHTML).toEqual('<span>a</span><span>b</span>');
        expect(consoleHistory.hasError()).toBe(false);

    });

});
