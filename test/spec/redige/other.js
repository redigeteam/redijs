'use strict';

describe('Redige selection utils', function () {

    afterEach(function () {
        consoleHistory.clear();
    });

    it('should return somethings (getGUID)', function () {
        var result = Redige.getGUID();
        expect(result.length).toEqual(37);
        expect(consoleHistory.hasError()).toBe(false);

    });

    it('should return somethings (toArray)', function () {
        var result = Redige.toArray([]);
        expect(result).toEqual(jasmine.any(Array));
        expect(consoleHistory.hasError()).toBe(false);

    });

    it('should not produce errors', function () {
        var result;
        Redige.dispatchEventByName({
            dispatchEvent: function (e) {
                result = e;
            }
        }, "type");
        expect(result).toBeDefined();
        expect(result).toEqual(jasmine.any(Event));
        expect(result.type).toEqual("type");
        expect(result.bubbles).toBe(true);
        expect(consoleHistory.hasError()).toBe(false);

    });


});
