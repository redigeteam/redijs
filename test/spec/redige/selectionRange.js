'use strict';

describe('Redige selection range util', function () {
    var section1;
    var section2;
    var section3;
    var section4;
    beforeEach(function () {
        consoleHistory.clear();

        var div = new Redige.Division();

        section1 = new Redige.Section();
        section1.id = 's1';
        section1.content = "hey salut";
        div.appendChild(section1);

        section2 = new Redige.Section();
        section2.id = 's2';
        section2.content = "hey salut";
        div.appendChild(section2);

        section3 = new Redige.Image();
        section3.id = 's3';
        div.appendChild(section3);

        section4 = new Redige.Section();
        section4.content = "hey salut";
        section4.id = 's4';
        div.appendChild(section4);


        var editor = document.getElementById('editor');
        editor.appendChild(div);
    });

    describe('Redige.getSectionInRange s1-s4', function () {


        it('should from s1 to s4 including image', function () {
            Redige.setSelection("s1", 0, "s4", 1);
            var selection = Redige.getDocumentSelection();
            var result = Redige.getSectionsInRange(selection.startSection, selection.endSection);

            expect(result.indexOf(section1)).toEqual(0);
            expect(result.indexOf(section2)).toEqual(1);
            expect(result.indexOf(section3)).toEqual(2);
            expect(result.indexOf(section4)).toEqual(3);
        });

    });

});
