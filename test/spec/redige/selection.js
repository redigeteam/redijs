'use strict';

describe('Redige selection utils', function () {

    beforeEach(function () {
        TestUtils.openDefaultDocument();
        consoleHistory.clear();
    });
    afterEach(function () {
        TestUtils.openDefaultDocument();
        consoleHistory.clear();
    });

    describe('Redige.getSection', function () {
        /**
         * retreive the parent section from a subelement.
         */
        it('should return the first section element in hierarchy', function () {
            var editor = document.getElementById('editor');
            var span = editor.getElementsByTagName('span')[0];
            var result = Redige.$$.getSection(span);

            var section = editor.getElementsByClassName('section')[0];
            expect(result).toEqual(section);
        });

        /**
         * retreive the parent section from a subelement : self case
         */
        it('should return the first section element in hierarchy : self case', function () {
            var editor = document.getElementById('editor');
            var section = editor.getElementsByClassName('section')[0];
            var result = Redige.$$.getSection(section);
            expect(result).toEqual(section);
        });
    });

    describe('Redige.$$.getSpanOffset', function () {
        /**
         * retreive span offset in the section :
         * a section is a set of spans, so to get selection index at the section level, we need to
         * calculate the span offset to add is to the current selection offset (in the span)
         *
         * <p><span>123</span><span>45][6</span><span>7890</span></p>
         * section :
         *    {
         *      content = "123456789"
         *    }
         * In this example the selection index will be 2 (caret between 5 & 6)
         * but at the section level, the offset should be : 5
         *
         * This test only test each span offset (not the caret position for instance)
         */
        it('should return the span offset at the section level', function () {

            var section = document.createElement('rd-section');
            section.id = "selectionTestId";
            //force to remove the 0x01 span added by default
            section.innerHTML = "";

            var span1 = document.createElement('span');
            span1.textContent = "123";
            section.appendChild(span1);

            var span2 = document.createElement('span');
            span2.textContent = "45600";
            section.appendChild(span2);

            var span3 = document.createElement('span');
            span3.textContent = "7890";
            section.appendChild(span3);

            expect(Redige.$$.getSpanOffset(span1)).toEqual(0);
            expect(Redige.$$.getSpanOffset(span2)).toEqual(3);
            expect(Redige.$$.getSpanOffset(span3)).toEqual(8);
        });
    });
    describe('Redige.$$.getSelectionOffset', function () {
        /**
         * retreive span offset in the section :
         * a section is a set of spans, so to get selection index at the section level, we need to
         * calculate the span offset to add is to the current selection offset (in the span)
         *
         * <p><span>123</span><span>45][6</span><span>7890</span></p>
         * section :
         *    {
         *      content = "123456789"
         *    }
         * In this example the selection index will be 2 (caret between 5 & 6)
         * but at the section level, the offset should be : 5
         *
         * This test test as well a spanOffset and caretOffset
         */
        it('should return the caret offset at the section level', function () {

            var section = document.createElement('rd-section');

            //force to remove the 0x01 span added by default
            section.innerHTML = "";

            var span1 = document.createElement('span');
            span1.textContent = "123";
            section.appendChild(span1);

            var span2 = document.createElement('span');
            span2.textContent = "45600";
            section.appendChild(span2);

            var span3 = document.createElement('span');
            span3.textContent = "7890";
            section.appendChild(span3);

            for (var index = 0; index < 4; index++) {
                expect(Redige.$$.getSelectionOffset(span1, index)).toEqual(index);
                expect(Redige.$$.getSelectionOffset(span1.childNodes[0], index)).toEqual(index);
            }
            for (var index = 0; index < 6; index++) {
                expect(Redige.$$.getSelectionOffset(span2, index)).toEqual(index + span1.textContent.length);
                expect(Redige.$$.getSelectionOffset(span2.childNodes[0], index)).toEqual(index + span1.textContent.length);
            }

            for (var index = 0; index < 6; index++) {
                expect(Redige.$$.getSelectionOffset(span3, index)).toEqual(index + span1.textContent.length + span2.textContent.length);
                expect(Redige.$$.getSelectionOffset(span3.childNodes[0], index)).toEqual(index + span1.textContent.length + span2.textContent.length);
            }

        });

        it('should return the caret offset at the section level : corner cases : empty span', function () {

            var section = document.createElement('rd-section');

            //force to remove the 0x01 span added by default
            section.innerHTML = "";

            var span1 = document.createElement('span');
            span1.textContent = "";
            section.appendChild(span1);

            var span2 = document.createElement('span');
            span2.textContent = "45600";
            section.appendChild(span2);

            expect(Redige.$$.getSelectionOffset(span1, 0)).toEqual(0);

            for (var index = 0; index < 6; index++) {
                expect(Redige.$$.getSelectionOffset(span2, index)).toEqual(index + span1.textContent.length);
                expect(Redige.$$.getSelectionOffset(span2.childNodes[0], index)).toEqual(index + span1.textContent.length);
            }

        });

        it('should return the caret offset at the section level : corner cases : 0x01', function () {

            var section = document.createElement('rd-section');

            var span1 = section.childNodes[0];

            var span2 = document.createElement('span');
            span2.textContent = "45600";
            section.appendChild(span2);

            expect(Redige.$$.getSelectionOffset(span1, 0)).toEqual(0);

            for (var index = 0; index < 6; index++) {
                expect(Redige.$$.getSelectionOffset(span2, index)).toEqual(index + span1.textContent.length);
                expect(Redige.$$.getSelectionOffset(span2.childNodes[0], index)).toEqual(index + span1.textContent.length);
            }

        });

        it('should return the caret offset at the section level : corner cases : P', function () {

            var section = document.createElement('rd-section');

            expect(Redige.$$.getSelectionOffset(section, 0)).toEqual(0);
            expect(Redige.$$.getSelectionOffset(section, 5)).toEqual(5);

        });

        it('should return the caret offset at the section level : corner cases : WTF', function () {

            //prevent error logs to appear in jasmin task console
            consoleHistory.preventDefault(true);

            expect(Redige.$$.getSelectionOffset(document.getElementsByTagName('div')[0], 4)).toBeUndefined();
            expect(Redige.$$.getSelectionOffset(document.body, 0)).toBeUndefined();
            expect(Redige.$$.getSelectionOffset(document.body, 5)).toBeUndefined();

            expect(consoleHistory.countError()).toEqual(3);

            consoleHistory.preventDefault(false);
        });
    });
    describe('Redige.$$.getDocumentSelection', function () {
        //NTM : set position to index 0 of a span make the selection at the end of the previous...
        it('should detect either selection is reversed or not', function () {

            var span1 = document.createElement('span');
            span1.id = "a";
            span1.innerHTML = "aaa";

            var span2 = document.createElement('span');
            span2.id = "b";
            span2.textContent = "bb";

            var span = document.getElementsByTagName("span")[0];
            span.parentNode.appendChild(span1);
            span.parentNode.appendChild(span2);


            var range = document.createRange();
            range.setStart(span2.childNodes[0], 1);
            range.setEnd(span1.childNodes[0], 1);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);

            expect(Redige.getDocumentSelection().anchorNode).toEqual(span1.childNodes[0]);

            range = document.createRange();
            range.setStart(span1.childNodes[0], 0);
            range.setEnd(span2.childNodes[0], 1);
            sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);

            expect(Redige.getDocumentSelection().anchorNode).toEqual(span1.childNodes[0]);
            expect(consoleHistory.hasError()).toBe(false);

            //clean
            span.parentNode.removeChild(span1);
            span.parentNode.removeChild(span2);

        });
        //NTM : set position to index 0 of a span make the selection at the end of the previous...
        it('should detect either selection is reversed or not', function () {

            var span1 = document.createElement('span');
            span1.id = "a";
            span1.innerHTML = "aaa";

            var span = document.getElementsByTagName("span")[0];
            span.parentNode.appendChild(span1);

            var range = document.createRange();
            range.setStart(span1.childNodes[0], 1);
            range.setEnd(span1.childNodes[0], 1);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);

            expect(Redige.getDocumentSelection().isCollapsed).toEqual(true);
            expect(consoleHistory.hasError()).toBe(false);
            //clean
            span.parentNode.removeChild(span1);

        });

        it('should handle no selection case', function () {
            var sel = window.getSelection();
            sel.removeAllRanges();

            var result = Redige.getDocumentSelection();
            expect(result).toBeUndefined();
            expect(consoleHistory.hasError()).toBe(false);
        });
    });
    describe('Redige.$$.isGoodSpan', function () {
        it('should check of the span is the selected one', function () {
            //1234567
            var section = document.createElement('rd-section');
            section.innerHTML = "";

            var span1 = document.createElement('span');
            span1.textContent = "123";
            section.appendChild(span1);

            var span2 = document.createElement('span');
            span2.textContent = "45";
            section.appendChild(span2);

            var span3 = document.createElement('span');
            span3.textContent = "67";
            section.appendChild(span3);

            expect(Redige.$$.isGoodSpan(span1, 0)).toBe(true);
            expect(Redige.$$.isGoodSpan(span1, 1)).toBe(true);
            expect(Redige.$$.isGoodSpan(span1, 2)).toBe(true);
            expect(Redige.$$.isGoodSpan(span1, 3)).toBe(true);
            expect(Redige.$$.isGoodSpan(span1, 4)).toBe(false);

            expect(Redige.$$.isGoodSpan(span2, 3)).toBe(false);
            expect(Redige.$$.isGoodSpan(span2, 4)).toBe(true);
            expect(Redige.$$.isGoodSpan(span2, 5)).toBe(true);
            expect(Redige.$$.isGoodSpan(span2, 6)).toBe(false);

            expect(Redige.$$.isGoodSpan(span3, 6)).toBe(true);
            expect(Redige.$$.isGoodSpan(span3, 7)).toBe(true);
            expect(Redige.$$.isGoodSpan(span3, 8)).toBe(false);

            expect(consoleHistory.hasError()).toBe(false);
        });

        it('should check of the span is the selected one with 0x01', function () {

            var section = document.createElement('rd-section');

            var span1 = section.childNodes[0];

            expect(Redige.$$.isGoodSpan(span1, 0)).toBe(true);
            expect(Redige.$$.isGoodSpan(span1, 1)).toBe(true);
            expect(Redige.$$.isGoodSpan(span1, 2)).toBe(false);

            expect(consoleHistory.hasError()).toBe(false);
        });
    });

    describe('Redige.$$.findSpan', function () {
        it('should get the matching span', function () {
            var editor = document.getElementById("editor");

            var section = document.createElement('rd-section');
            section.id = "selectionTestId";
            section.innerHTML = "";

            var span0 = document.createElement('span');
            span0.innerHTML = "0";

            var span1 = document.createElement('span');
            span1.innerHTML = "1";

            editor.appendChild(section);
            section.appendChild(span0);
            section.appendChild(span1);

            expect(Redige.$$.findSpan(section.id, 0)).not.toEqual(span1);
            expect(Redige.$$.findSpan(section.id, 1)).not.toEqual(span1);
            expect(Redige.$$.findSpan(section.id, 2)).toEqual(span1);
            expect(Redige.$$.findSpan(section.id, 3)).toBeUndefined();

            expect(consoleHistory.hasError()).toBe(false);

            //clean
            section.removeChild(span1);
        });
    });

    describe('Redige.$$.correctOffset', function () {
        it('should Calculate the node #text offset in the span from the sectionOffset', function () {

            var section = document.createElement('rd-section');
            section.id = "selectionTestId";


            var span0 = section.childNodes[0];

            //0115s999
            var span1 = document.createElement('span');
            span1.innerHTML = "115s";

            var span2 = document.createElement('span');
            span2.innerHTML = "999";

            section.appendChild(span0);
            section.appendChild(span1);
            section.appendChild(span2);

            //in first 0x01 span
            expect(Redige.$$.correctOffset(section.childNodes[0], 0)).toEqual(0);
            expect(Redige.$$.correctOffset(section.childNodes[0], 1)).toEqual(1);

            //in second span 115a
            expect(Redige.$$.correctOffset(span1, 2)).toEqual(1);
            expect(Redige.$$.correctOffset(span1, 3)).toEqual(2);
            expect(Redige.$$.correctOffset(span1, 4)).toEqual(3);
            expect(Redige.$$.correctOffset(span1, 5)).toEqual(4);
            //in second span 999
            expect(Redige.$$.correctOffset(span2, 6)).toEqual(1);
            expect(Redige.$$.correctOffset(span2, 7)).toEqual(2);
            expect(Redige.$$.correctOffset(span2, 8)).toEqual(3);

            expect(consoleHistory.hasError()).toBe(false);

            //clean
            section.removeChild(span1);
            section.removeChild(span2);
        });
    });

});
