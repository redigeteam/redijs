'use strict';

describe('Code Detector', function () {

    describe('nominal case', function () {

        it('should detect html', function () {
            var languages = LanguageDetector.detect("<html><div></div>");
            expect(languages[0].language).toEqual("html");
        });

        it('should detect javascript', function () {
            var languages = LanguageDetector.detect("var function(){}");
            expect(languages[0].language).toEqual("javascript");
        });

        it('should detect php', function () {
            var languages = LanguageDetector.detect("<?php echo():");
            expect(languages[0].language).toEqual("php");
        });
    });
});

