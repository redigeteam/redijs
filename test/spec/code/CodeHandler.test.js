'use strict';

describe('Code Handler', function () {

    var section1Content = "var section1Content = function(){console.log()}";
    var section1;

    beforeEach(function () {
        Redige.reset();
        section1 = new Redige.Section();
        section1.content = section1Content;

        var div = new Redige.Division();
        div.appendChild(section1);
        div.type = "code";

        TestUtils.addToEditor(div);

    });

    function fireInputEvent() {
        Redige.setSelection(section1.id, 0, section1.id, 0);
        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('input', true, false, null);
        window.getSelection().focusNode.dispatchEvent(event);
    }

    describe('nominal case', function () {

        beforeEach(function () {
            fireInputEvent();
        });

        it('should add code decorators', function () {
            expect(section1.content).toEqual(section1Content);
            expect(section1.decorators.length).toEqual(11);
        });
    });
});

