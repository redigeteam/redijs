'use strict';

describe('Division', function () {
    var codediv, sectionBase, sectionInserted;
    beforeEach(function () {
        Redige.reset();
        codediv = new Redige.Division();
        codediv.type = 'code';
        sectionBase = new Redige.Section();
        sectionBase.content = 'Section de base';
        codediv.appendChild(sectionBase);
        TestUtils.addToEditor(codediv);
    });
    
    function insertFirstChild() {
        sectionInserted = new Redige.Section();
        sectionInserted.content = 'Section insérée';
        codediv.firstChild = sectionInserted;
    }

    describe('getting first child', function () {
        describe('in code division', function () {
            beforeEach(function () {});
            it('should be section and not code control div', function () {
                expect(codediv.firstChild).toEqual(sectionBase);
                expect(codediv.firstChild).not.toEqual(codediv.children[0]);
            });
        });
    });

    describe('setting first child', function () {
        describe('in code division', function () {
            beforeEach(function () {});
            it('should replace current first child (and not code control div)', function () {
                expect(codediv.children[1]).toEqual(sectionBase);
                insertFirstChild();
                expect(codediv.children[1]).toEqual(sectionInserted);
                expect(codediv.children[1]).not.toEqual(sectionBase);
            });
            it('should be set in childNodes and children', function () {
                insertFirstChild();
                expect(codediv.children[1]).toEqual(codediv.childNodes.item(1));
            });
        });
    });
});