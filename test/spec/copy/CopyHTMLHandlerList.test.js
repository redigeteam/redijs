'use strict';
describe('export HTML', function () {

    function parse(html) {
        var node = document.createElement('div');
        node.innerHTML = '<div>' + html + '</div>';
        return HTMLParser.parse(node).childNodes[0].childNodes;
    }

    function expectSection(obj, type, level, content) {
        expect(obj.level).toEqual(level);
        expect(obj.type).toEqual(type);
        expect(obj.content.trim()).toEqual(content);
    }

    describe('export HTML', function () {
        describe('UL', function () {
            describe('flat simple ul', function () {
                it('should create a list according to type', function () {
                    var listHTML = '<ul><li>ulist 1</li><li>ulist 2</li><li>ulist 3</li></ul>';
                    var sections = parse(listHTML);
                    var html = HTMLExporter.exportSectionList(sections);
                    expect(html).toEqual(listHTML);
                });
            });
            describe('separated ul', function () {
                it('should create two different list according to type', function () {
                    var listHTML = '<ul><li>ulist 1</li></ul><p>coucou</p><ul><li>ulist 2</li></ul>';
                    var sections = parse(listHTML);
                    var html = HTMLExporter.exportSectionList(sections);
                    expect(html).toEqual(listHTML);
                });
            });
            describe('nested ul', function () {
                it('should create two different list according to type & level', function () {
                    var listHTML = '<ul><li>ulist 1</li><li>ulist 2<ul><li>ulist 2.1</li><li>ulist 2.2</li><li>ulist 2.3</li></ul></li></ul>';
                    var sections = parse(listHTML);
                    var html = HTMLExporter.exportSectionList(sections);
                    expect(html).toEqual(listHTML);
                });
            });
        });
        describe('mixed', function () {
            describe('followed lists', function () {
                it('should create two different list according to type', function () {
                    var listHTML = '<ul><li>ulist 1</li></ul><ol><li>ulist 2</li></ol>';
                    var sections = parse(listHTML);
                    var html = HTMLExporter.exportSectionList(sections);
                    expect(html).toEqual(listHTML);
                });
            });
            describe('nested ul', function () {
                it('should create two different list according to type', function () {
                    var listHTML = '<ul><li>ulist 1<ol><li>ulist 2</li></ol></li></ul>';
                    var sections = parse(listHTML);
                    var html = HTMLExporter.exportSectionList(sections);
                    expect(html).toEqual(listHTML);
                });
            });
            describe('fuuuckkking lists', function () {
                it('should do the job', function () {
                    var listHTML = '<ul><li>ulist 1<ol><li>olist 1.1<ol><li>olist 1.1.1</li></ol></li><li>olist 1.2<ol><li>olist 1.2.1</li></ol><ul><li>ulist 1.2.1</li></ul></li></ol></li><li>ulist 2<ul><li>ulist 2.1<ol><li>olist 2.1.1<ol><li>olist 2.1.1.1</li></ol></li></ol></li></ul></li></ul>';
                    var sections = Redige.toArray(parse(listHTML)).map(function (s) {
                        s.content = s.content.trim();
                        return s;
                    });
                    var html = HTMLExporter.exportSectionList(sections);
                    expect(html).toEqual(listHTML);
                });
            });
        });
    });
});