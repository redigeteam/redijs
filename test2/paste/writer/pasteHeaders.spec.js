(function () {
    "use strict";

    var expect = require('chai').expect;
    var _ = require('lodash');
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/WriterParser/WriterHTMLParser');

    describe('paste/writer/pasteHeaders.spec.js', function () {

        describe('Simple Headers', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/writer/samples/4.1.2_headers.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result.children.length).to.deep.equal(9);
                //"<h1><span>Titre 1</span></h1>"
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "Titre 1",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 2,
                    content: "Titre 1.1",
                    decorators: []
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 3,
                    content: "Titre 1.1.1",
                    decorators: []
                });
                expect(result.children[3]).to.deep.equal({
                    type: 'hierarchical',
                    level: 4,
                    content: "Titre 1.1.1.1",
                    decorators: []
                });
                expect(result.children[4]).to.deep.equal({
                    type: 'hierarchical',
                    level: 5,
                    content: "Titre 1.1.1.1.1",
                    decorators: []
                });
                expect(result.children[5]).to.deep.equal({
                    type: 'hierarchical',
                    level: 6,
                    content: "Titre 1.1.1.1.1.1",
                    decorators: []
                });
                expect(result.children[6]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "Section",
                    decorators: []
                });
                expect(result.children[7]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "Titre 2",
                    decorators: []
                });
                expect(result.children[8]).to.deep.equal({
                    type: 'hierarchical',
                    level: 2,
                    content: "Titre 2.1",
                    decorators: []
                });
            });
        });

        describe('Numbered Headers', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/writer/samples/4.1.2_NumberedHeaders.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result.children.length).to.deep.equal(9);
                //"<h1><span>1Titre 1</span></h1>"
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "1Titre 1",
                    decorators: []
                });
                //"<h2><span>1.1Titre 1.1</span></h2>"
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 2,
                    content: "1.1Titre 1.1",
                    decorators: []
                });
                //"<h3><span>1.1.1Titre 1.1.1</span></h3>"
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 3,
                    content: "1.1.1Titre 1.1.1",
                    decorators: []
                });
                //"<h4><span>1.1.1.1Titre 1.1.1.1</span></h4>"
                expect(result.children[3]).to.deep.equal({
                    type: 'hierarchical',
                    level: 4,
                    content: "1.1.1.1Titre 1.1.1.1",
                    decorators: []
                });
                //"<h5><span>1.1.1.1.1Titre 1.1.1.1.1</span></h5>"
                expect(result.children[4]).to.deep.equal({
                    type: 'hierarchical',
                    level: 5,
                    content: "1.1.1.1.1Titre 1.1.1.1.1",
                    decorators: []
                });
                //"<h6><span>1.1.1.1.1.1Titre 1.1.1.1.1.1</span></h6>"
                expect(result.children[5]).to.deep.equal({
                    type: 'hierarchical',
                    level: 6,
                    content: "1.1.1.1.1.1Titre 1.1.1.1.1.1",
                    decorators: []
                });
                //"<p><span>Section</span></p>"
                expect(result.children[6]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "Section",
                    decorators: []
                });
                //"<h1><span>2Titre 2</span></h1>"
                expect(result.children[7]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "2Titre 2",
                    decorators: []
                });
                //"<h2><span>2.1Titre 2.1</span></h2>
                expect(result.children[8]).to.deep.equal({
                    type: 'hierarchical',
                    level: 2,
                    content: "2.1Titre 2.1",
                    decorators: []
                });
            });
        });
    });
})();