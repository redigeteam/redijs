(function () {
    "use strict";

    var expect = require('chai').expect;
    var _ = require('lodash');
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/WriterParser/WriterHTMLParser');

    describe('paste/writer/pasteText.spec.js', function () {
        describe('Simple char', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/writer/samples/4.1.2_simpleChar.html").then(function (content) {
                    result = HTMLParser.parse(content);
                });
            });
            it('should detect inline Past', function () {
                //<P><span>C</span><P>
                expect(result.copyType).to.equal('inline');
            });
            it('should retreive the char', function () {
                //<P><span>C</span><P>
                expect(result.content).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "C",
                    decorators: []
                });
            });
        });

        describe('MultiLines', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/writer/samples/4.1.2_multiLines.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "A",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "B",
                    decorators: []
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "C",
                    decorators: []
                });
            });
        });


        describe('Decorators', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/writer/samples/4.1.2_decorators.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                // <P><span>A</span></P>
                // <P><span>B ici je met un </span><a href="http://www.google.fr">lien</a> du <b>gras</b> et de <i>l'italique</i></P>
                // <P><span>C</span><P>
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "A",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "B ici je met un lien du gras et de l'italique",
                    decorators: [
                        {
                            data: "http://www.google.fr",
                            end: 20,
                            start: 16,
                            type: "link"
                        },
                        {
                            end: 28,
                            start: 24,
                            type: "bold"
                        },
                        {
                            end: 45,
                            start: 35,
                            type: "italic"
                        }]
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "C",
                    decorators: []
                });
            });
        });
    });
})();