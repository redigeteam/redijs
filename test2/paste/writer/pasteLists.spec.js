(function () {
    "use strict";

    var expect = require('chai').expect;
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/WriterParser/WriterHTMLParser');

    describe('paste/writer/pasteLists.spec.js', function () {
        describe('Simple OList ', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/writer/samples/4.1.2_lists.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive Olist Item 1 level0 at index 0', function () {
                //"<p level=\"0\" type=\"olist\"><span>Item 1</span></p>"
                expect(result.children[0]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 1",
                    decorators: []
                });
            });
            it('should retreive Olist Item 2 level0 at index 1', function () {
                //"<p level=\"0\" type=\"olist\"><span>Item 2</span></p>"
                expect(result.children[1]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 2",
                    decorators: []
                });
            });
            it('should retreive Olist Item 3 level0 at index 2', function () {
                //"<p level=\"0\" type=\"olist\"><span>Item 3</span></p>"
                expect(result.children[2]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 3",
                    decorators: []
                });
            });
            it('should retreive P index 3', function () {
                //"<p></p>"
                expect(result.children[3]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "",
                    decorators: []
                });
            });
            it('should retreive Item 1 olist level0 at index 4', function () {
                //"<p level=\"0\" type=\"olist\"><span>Item 1</span></p>"
                expect(result.children[4]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 1",
                    decorators: []
                });
            });
            it('should retreive Item 1.1 olist level 1 at index 5', function () {
                //"<p level=\"1\" type=\"olist\"><span>Item 1.1</span></p>"
                expect(result.children[5]).to.deep.equal({
                    type: 'olist',
                    level: 1,
                    content: "Item 1.1",
                    decorators: []
                });
            });
            it('should retreive Item 1.2 olist level 1 at index 6', function () {

                //"<p level=\"1\" type=\"olist\"><span>Item 1.2</span></p>"
                expect(result.children[6]).to.deep.equal({
                    type: 'olist',
                    level: 1,
                    content: "Item 1.2",
                    decorators: []
                });
            });
            it('should retreive Item 2 olist level 0 at index 7', function () {

                //"<p level=\"0\" type=\"olist\"><span>Item 2</span></p>"
                expect(result.children[7]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 2",
                    decorators: []
                });
            });
            it('should retreive Item 2.2 olist level 1 at index 8', function () {
                //"<p level=\"1\" type=\"olist\"><span>Item 2.2</span></p>"
                expect(result.children[8]).to.deep.equal({
                    type: 'olist',
                    level: 1,
                    content: "Item 2.2",
                    decorators: []
                });
            });
            it('should retreive Item 2.2.1 olist level 2 at index 9', function () {
                //"<p level=\"2\" type=\"olist\"><span>Item 2.2.1</span></p>"
                expect(result.children[9]).to.deep.equal({
                    type: 'olist',
                    level: 2,
                    content: "Item 2.2.1",
                    decorators: []
                });
            });
            it('should retreive P index 10', function () {
                //"<p></p>"
                expect(result.children[10]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "",
                    decorators: []
                });
            });
            it('should retreive Item a ulist level 0 at index 11', function () {
                //"<p level=\"0\" type=\"ulist\"><span>Item a</span></p>"
                expect(result.children[11]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item a",
                    decorators: []
                });
            });
            it('should retreive Item b ulist level 0 at index 12', function () {
                //"<p level=\"0\" type=\"ulist\"><span>Item b</span></p>"
                expect(result.children[12]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item b",
                    decorators: []
                });
            });
            it('should retreive Item c ulist level 0 at index 13', function () {
                //"<p level=\"0\" type=\"ulist\"><span>Item c</span></p>"
                expect(result.children[13]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item c",
                    decorators: []
                });
            });
            it('should retreive P index 14', function () {
                //"<p></p>"
                expect(result.children[14]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "",
                    decorators: []
                });

            });
            it('should retreive Item a ulist level 0 at index 15', function () {
                //"<p level=\"0\" type=\"ulist\"><span>Item a</span></p>"
                expect(result.children[15]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item a",
                    decorators: []
                });
            });
            it('should retreive Item a.a ulist level 1 at index 16', function () {
                //"<p level=\"1\" type=\"ulist\"><span>Item a.a</span></p>"
                expect(result.children[16]).to.deep.equal({
                    type: 'ulist',
                    level: 1,
                    content: "Item a.a",
                    decorators: []
                });
            });
            it('should retreive Item a.a.a ulist level 2 at index 17', function () {
                //"<p level=\"2\" type=\"ulist\"><span>Item a.a.a</span></p>"
                expect(result.children[17]).to.deep.equal({
                    type: 'ulist',
                    level: 2,
                    content: "Item a.a.a",
                    decorators: []
                });
            });
            it('should retreive Item a.b ulist level 1 at index 18', function () {
                //"<p level=\"1\" type=\"ulist\"><span>Item a.b</span></p>"
                expect(result.children[18]).to.deep.equal({
                    type: 'ulist',
                    level: 1,
                    content: "Item a.b",
                    decorators: []
                });
            });
        });
    });
})();