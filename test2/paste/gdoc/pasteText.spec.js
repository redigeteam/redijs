(function () {
    "use strict";

    var expect = require('chai').expect;
    var _ = require('lodash');
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/GdocParser/GdocHTMLParser');

    describe('paste/gdoc/pasteText.spec.js', function () {
        describe('Simple char', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/gdoc/samples/simpleChar.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "C",
                    decorators: []
                });
            });
        });

        describe('MultiLines', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/gdoc/samples/multiLines.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {

                expect(result.children.length).to.equal(3);
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "A",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "B",
                    decorators: []
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "C",
                    decorators: []
                });
            });
        });


        describe('Decorators', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/gdoc/samples/decorators.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                // <P><span>A</span></P>
                // <P><span>B ici je met un </span><a href="http://www.google.fr">lien</a> du <b>gras</b> et de <i>l'italique</i></P>
                // <P><span>C</span><P>
                expect(result.children.length).to.equal(3);
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "A",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "B ici je met un lien du gras et de l’italique",
                    decorators: [
                        {
                            data: "http://www.google.fr",
                            end: 20,
                            start: 16,
                            type: "link"
                        },
                        {
                            end: 28,
                            start: 24,
                            type: "bold"
                        },
                        {
                            end: 45,
                            start: 35,
                            type: "italic"
                        }]
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "C",
                    decorators: []
                });
            });
        });


        describe('Image Mixed', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/gdoc/samples/imageMixed.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                // <P><span>Ss</span></P>
                // <img src="https://lh3.googleusercontent.com/zZZjGBZKq74y1z6e3qUSCN5VS-9GPkaBkE7rPRnXO7eDdc2WJiXcVJpv7a1giPVDO3Grum7Munu_5d-hf3eT26iyiDKox6WrKlTlQu3LtM7w3sNg0IFB0R-8-bUfDh9ghQV7mpkq">
                // <P><span>ss</span><P>

                expect(result.children.length).to.equal(3);
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "Ss",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'image',
                    data: "https://lh3.googleusercontent.com/zZZjGBZKq74y1z6e3qUSCN5VS-9GPkaBkE7rPRnXO7eDdc2WJiXcVJpv7a1giPVDO3Grum7Munu_5d-hf3eT26iyiDKox6WrKlTlQu3LtM7w3sNg0IFB0R-8-bUfDh9ghQV7mpkq"
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "ss",
                    decorators: []
                });

            });
        });
    });
})();