(function () {
    "use strict";

    var expect = require('chai').expect;
    var _ = require('lodash');
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/GdocParser/GdocHTMLParser');

    describe('paste/gdoc/pasteHeaders.spec.js', function () {

        describe('Simple Headers', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/gdoc/samples/headers.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(_.get(result, 'children.length')).to.equal(9);
                //"<h1><span>Titre 1</span></h1>"
                expect(result.children[0]).to.deep.equal({
                    content: "Titre 1",
                    type: 'hierarchical',
                    level: 1,
                    decorators: []
                });
            });
        });

        describe('Numbered Headers', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/gdoc/samples/NumberedHeaders.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(_.get(result, 'children.length')).to.equal(10);
                //"<h1><span>Titre 1</span></h1>"
                expect(result.children[0]).to.deep.equal({
                    content: "Titre 1",
                    type: 'hierarchical',
                    level: 1,
                    decorators: []
                });
                //"<h2><span>Titre 1.1</span></h2>"
                expect(result.children[1]).to.deep.equal({
                    content: "Titre 1.1",
                    type: 'hierarchical',
                    level: 2,
                    decorators: []
                });
                //"<h3><span>Titre 1.1.1</span></h3>"
                expect(result.children[2]).to.deep.equal({
                    content: "Titre 1.1.1",
                    type: 'hierarchical',
                    level: 3,
                    decorators: []
                });
                //"<h4><span>Titre 1.1.1.1</span></h4>"
                expect(result.children[3]).to.deep.equal({
                    content: "Titre 1.1.1.1",
                    type: 'hierarchical',
                    level: 4,
                    decorators: []
                });
                //"<h5><span>Titre 1.1.1.1.1</span></h5>"
                expect(result.children[4]).to.deep.equal({
                    content: "Titre 1.1.1.1.1",
                    type: 'hierarchical',
                    level: 5,
                    decorators: []
                });
                //"<h6><span>Titre 1.1.1.1.1.1</span></h6>"
                expect(result.children[5]).to.deep.equal({
                    content: "Titre 1.1.1.1.1.1",
                    type: 'hierarchical',
                    level: 6,
                    decorators: []
                });
                //"<p><span>Section</span></p>"
                expect(result.children[6]).to.deep.equal({
                    content: "Section",
                    type: 'hierarchical',
                    level: 0,
                    decorators: []
                });
                //"<h1><span>Titre 2</span></h1>"
                expect(result.children[7]).to.deep.equal({
                    content: "Titre 2",
                    type: 'hierarchical',
                    level: 1,
                    decorators: []
                });
                //"<h2><span>Titre 2.1</span></h2>"
                expect(result.children[8]).to.deep.equal({
                    content: "Titre 2.1",
                    type: 'hierarchical',
                    level: 2,
                    decorators: []
                });
                //"<p></p>"
                expect(result.children[9]).to.deep.equal({
                    content: "",
                    type: 'hierarchical',
                    level: 0,
                    decorators: []
                });
            });
        });
    });
})();