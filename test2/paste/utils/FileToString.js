(function () {
    var Bluebird = require('bluebird');
    var fs = Bluebird.promisifyAll(require("fs"));

    module.exports = {

        read: function (fileName) {
            return fs.readFileAsync(fileName, "utf8");
        }
    };

})();
