(function () {
    "use strict";

    var expect = require('chai').expect;
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/WordParser/WordHTMLParser');

    describe('paste/word/pasteLists.spec.js', function () {
        describe('Simple OList ', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/word/samples/w2010_lists.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });

            it('should retreive the char', function () {
                //"<p level=\"0\" type=\"olist\"><span>Item 1</span></p>"
                expect(result.children[0]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 1",
                    decorators: []
                });
                //"<p level=\"0\" type=\"olist\"><span>Item 2</span></p>"
                expect(result.children[1]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 2",
                    decorators: []
                });
                //"<p level=\"0\" type=\"olist\"><span>Item 3</span></p>"
                expect(result.children[2]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 3",
                    decorators: []
                });
                //"<p></p>"
                expect(result.children[3]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "",
                    decorators: []
                });
                //"<p level=\"0\" type=\"olist\"><span>Item 1</span></p>"
                expect(result.children[4]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 1",
                    decorators: []
                });
                //"<p level=\"1\" type=\"olist\"><span>Item 1.1</span></p>"
                expect(result.children[5]).to.deep.equal({
                    type: 'olist',
                    level: 1,
                    content: "Item 1.1",
                    decorators: []
                });
                //"<p level=\"1\" type=\"olist\"><span>Item 1.2</span></p>"
                expect(result.children[6]).to.deep.equal({
                    type: 'olist',
                    level: 1,
                    content: "Item 1.2",
                    decorators: []
                });
                //"<p level=\"0\" type=\"olist\"><span>Item 2</span></p>"
                expect(result.children[7]).to.deep.equal({
                    type: 'olist',
                    level: 0,
                    content: "Item 2",
                    decorators: []
                });
                //"<p level=\"1\" type=\"olist\"><span>Item 2.2</span></p>"
                expect(result.children[8]).to.deep.equal({
                    type: 'olist',
                    level: 1,
                    content: "Item 2.2",
                    decorators: []
                });
                //"<p level=\"2\" type=\"olist\"><span>Item 2.2.1</span></p>"
                expect(result.children[9]).to.deep.equal({
                    type: 'olist',
                    level: 2,
                    content: "Item 2.2.1",
                    decorators: []
                });
                //"<p></p>"
                expect(result.children[10]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "",
                    decorators: []
                });
                //"<p level=\"0\" type=\"ulist\"><span>Item a</span></p>"
                expect(result.children[11]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item a",
                    decorators: []
                });
                //"<p level=\"0\" type=\"ulist\"><span>Item b</span></p>"
                expect(result.children[12]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item b",
                    decorators: []
                });
                //"<p level=\"0\" type=\"ulist\"><span>Item c</span></p>"
                expect(result.children[13]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item c",
                    decorators: []
                });
                //"<p></p>"
                expect(result.children[14]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "",
                    decorators: []
                });
                //"<p level=\"0\" type=\"ulist\"><span>Item a</span></p>"
                expect(result.children[15]).to.deep.equal({
                    type: 'ulist',
                    level: 0,
                    content: "Item a",
                    decorators: []
                });
                //"<p level=\"1\" type=\"ulist\"><span>Item a.a</span></p>"
                expect(result.children[16]).to.deep.equal({
                    type: 'ulist',
                    level: 1,
                    content: "Item a.a",
                    decorators: []
                });
                //"<p level=\"2\" type=\"ulist\"><span>Item a.a.a</span></p>"
                expect(result.children[17]).to.deep.equal({
                    type: 'ulist',
                    level: 2,
                    content: "Item a.a.a",
                    decorators: []
                });
                //"<p level=\"1\" type=\"ulist\"><span>Item a.b</span></p>"
                expect(result.children[18]).to.deep.equal({
                    type: 'ulist',
                    level: 1,
                    content: "Item a.b",
                    decorators: []
                });
            });
        });
    });
})();