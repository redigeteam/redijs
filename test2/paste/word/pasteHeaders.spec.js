(function () {
    "use strict";

    var expect = require('chai').expect;
    var _ = require('lodash');
    var fileToString = require('../utils/FileToString');
    var HTMLParser = require('../../../app/scripts/features/paste/WordParser/WordHTMLParser');

    describe('paste/word/pasteHeaders.spec.js', function () {

        describe('Simple Headers', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/word/samples/w2010_headers.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result.children.length).to.equal(9);
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "Titre 1",
                    decorators: []
                });
            });
        });

        describe('Numbered Headers', function () {
            var result;
            before(function () {
                return fileToString.read("./test2/paste/word/samples/w2010_NumberedHeaders.html").then(function (content) {
                    result = HTMLParser.parse(content).content;
                });
            });
            it('should retreive the char', function () {
                expect(result.children.length).to.deep.equal(9);
                //"<h1><span>Titre 1</span></h1>"
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "Titre 1",
                    decorators: []
                });
                expect(result.children[1]).to.deep.equal({
                    type: 'hierarchical',
                    level: 2,
                    content: "Titre 1.1",
                    decorators: []
                });
                expect(result.children[2]).to.deep.equal({
                    type: 'hierarchical',
                    level: 3,
                    content: "Titre 1.1.1",
                    decorators: []
                });
                expect(result.children[3]).to.deep.equal({
                    type: 'hierarchical',
                    level: 4,
                    content: "Titre 1.1.1.1",
                    decorators: []
                });
                expect(result.children[4]).to.deep.equal({
                    type: 'hierarchical',
                    level: 5,
                    content: "Titre 1.1.1.1.1",
                    decorators: []
                });
                expect(result.children[5]).to.deep.equal({
                    type: 'hierarchical',
                    level: 6,
                    content: "Titre 1.1.1.1.1.1",
                    decorators: []
                });
                expect(result.children[6]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "Section",
                    decorators: []
                });
                expect(result.children[7]).to.deep.equal({
                    type: 'hierarchical',
                    level: 1,
                    content: "Titre 2",
                    decorators: []
                });
                expect(result.children[8]).to.deep.equal({
                    type: 'hierarchical',
                    level: 2,
                    content: "Titre 2.1",
                    decorators: []
                });
            });
        });
    });
})();