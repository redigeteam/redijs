(function () {
    "use strict";

    var expect = require('chai').expect;
    var HTMLParser = require('../../../app/scripts/features/paste/WordParser/WordHTMLParser');


    describe('paste/word/pasteWordTricks.spec.js', function () {
        describe('Recursive Spans', function () {
            var result;
            before(function () {
                result = HTMLParser.parse("<body><p class=MsoQuote>gddd <span class=CitationCar><span style='font-style:normal'>h<a href='http://www.google.fr'>ff</a>f</span></span> gh<o:p></o:p></p></body>").content;
            });
            it('should retreive the char', function () {
                //<P><span>gddd</span><span class="bold">h<span class="link bold">ff</span>f</span><span>gh</span><P>
                expect(result.children.length).to.equal(1);
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "gddd hfff gh",
                    decorators: [{type: 'italic', start: 5, end: 9}, {
                        type: 'link',
                        start: 6,
                        data: 'http://www.google.fr',
                        end: 8
                    }]
                });
            });
        });

        describe('Recursive Spans', function () {
            it('should retreive the char', function () {
                var result = HTMLParser.parse("<body><p><span>kk</span></p></body>").content;
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "kk",
                    decorators: []
                });
            });
            it('should retreive the char', function () {
                var result = HTMLParser.parse("<body><p><span>kk</span><span>aa</span></p></body>").content;
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "kkaa",
                    decorators: []
                });
            });
            it('should retreive the char', function () {
                var result = HTMLParser.parse("<body><p><span>kk</span><span>aa<span>bb</span></span></p></body>").content;
                expect(result.children[0]).to.deep.equal({
                    type: 'hierarchical',
                    level: 0,
                    content: "kkaabb",
                    decorators: []
                });
            });
        });
    });

})();