(function () {
    "use strict";

    const selectionChangeHandler = require('./SelectionChangeHandler').init(Redige);
    const DecoratorHandler = require('../features/toolbar/DecoratorHandler');

    module.exports = function (Redige) {

        Redige.$$.getSection = getSection;
        function getSection(currentElement, elementTagToGet) {
            var tag = elementTagToGet || "RD-";
            var result;
            while (!result && currentElement) {
                if (!Redige._editor.contains(currentElement)) {
                    return;
                }
                if (currentElement.nodeName.indexOf(tag) === 0) {
                    result = currentElement;
                }
                else {
                    currentElement = currentElement.parentElement;
                }
            }
            return result;
        }

        Redige.getCellFirstSection = function (currentElement) {
            var result;
            while (!result && currentElement) {
                if (!Redige._editor.contains(currentElement)) {
                    return;
                }
                if (currentElement.nodeName === "RD-SECTION") {
                    result = currentElement;
                } else if (currentElement.nodeName === "RD-TABLE" || currentElement.nodeName === "TABLE" || currentElement.nodeName === "TBODY" || currentElement.nodeName === "TR" || currentElement.nodeName === "TD") {
                    currentElement = currentElement.firstChild;
                } else {
                    currentElement = currentElement.parentElement;
                }
            }

            return result;
        };

        Redige.getCellSections = function (currentElement) {
            var cell, sections;
            while (!cell && currentElement) {
                if (!Redige._editor.contains(currentElement)) {
                    return;
                }
                if (currentElement.nodeName === "TD") {
                    cell = currentElement;
                } else if (currentElement.nodeName === "RD-TABLE" || currentElement.nodeName === "TABLE" || currentElement.nodeName === "TBODY" || currentElement.nodeName === "TR") {
                    currentElement = currentElement.firstChild;
                } else {
                    currentElement = currentElement.parentElement;
                }
            }

            sections = Redige.queryAll(cell, "rd-section");

            return sections;
        };


        Redige.$$.getSpanOffset = function (span) {
            var section = span.parentElement;
            var children = Redige.toArray(section.childNodes);
            var offset = 0;
            for (var index = 0; index < children.indexOf(span); index++) {
                if (children[index].nodeName === "SPAN") {
                    offset += children[index].textContent.length;
                }
            }
            return offset;
        };

        Redige.$$.getSelectionOffset = function (element, elementOffset) {
            if (element.nodeName === "#text") {
                if (element.parentElement.childNodes[0] === element) {
                    return Redige.$$.getSpanOffset(element.parentElement) + elementOffset;
                }
            }
            else if (element.nodeName === "SPAN") {
                return Redige.$$.getSpanOffset(element) + elementOffset;
            }
            else if (element.nodeName === "RD-SECTION") {
                return elementOffset;
            } else if (element.nodeName === "RD-TABLE") {
                return elementOffset;
            } else {
                return 0;
            }
        };

        Redige.isInDocSelection = function () {
            return Redige._editor.contains(window.getSelection().anchorNode) && Redige._editor.contains(window.getSelection().focusNode);
        };

        Redige.isInSection = function (node) {
            return !!getSection(node || window.getSelection().anchorNode);
        };

        Redige.isInTable = function (node) {
            return getSection(node || window.getSelection().anchorNode, "RD-TABLE");
        };

        Redige.isImage = function () {
            return window.getSelection().anchorNode.tagName === "RD-IMAGE" && window.getSelection().anchorNode.classList.contains('image');
        };

        Redige.isImageInSelection = function () {
            if (!window.getSelection().isCollapsed) {
                var sections = Redige.getDocumentSelection().sections;
                if (sections) {
                    for (var i = 0; i < sections.length; i++) {
                        if (sections[i].tagName === "RD-IMAGE") {
                            return true;
                        }
                    }
                }
            }
            return false;
        };

        Redige.isImageInput = function (node) {
            var currentElement = node || window.getSelection().anchorNode;
            return !!getSection(currentElement, "RD-IMAGE-INPUT");
        };

        Redige.isAttachment = function (node) {
            var currentElement = node || window.getSelection().anchorNode;
            return !!getSection(currentElement, "RD-ATTACHMENT");
        };

        Redige.isAttachmentInput = function () {
            return document.getElementById("attachmentInput") && document.getElementById("attachmentInput").contains(window.getSelection().anchorNode);
        };

        Redige.isLinkInput = function () {
            return !!document.getElementById('linkInput');
        };

        Redige.isMacOs = function () {
            return window.navigator.platform.toLowerCase().indexOf("mac") > -1;
        };

        Redige.isBlock = function (blockType) {
            var elt = window.getSelection().anchorNode;
            while (elt) {
                if (elt.tagName === 'RD-DIVISION' && (elt.type && elt.type === blockType)) {
                    return true;
                }
                elt = elt.parentElement;
            }
            return false;
        };

        Redige.getDocumentSelection = function () {

            var selection = window.getSelection();
            var anchorOffset = getAnchorOffset(selection);

            if (selection.type === "None" || !Redige.isInDocSelection()) {
                return;
            }

            if (Redige.isImage() || Redige.isImageInput() || !Redige.isInSection()) {
                return {
                    startSection: selection.anchorNode,
                    startOffset: 0,
                    endSection: selection.anchorNode,
                    endOffset: 0,
                    isCollapsed: selection.isCollapsed,
                    anchorNode: selection.anchorNode,
                    anchorOffset: anchorOffset,
                    focusOffset: selection.focusOffset,
                    focusNode: selection.anchorNode
                };
            }

            var endSection;
            var endOffset;
            var startSection = Redige.isInTable() ? Redige.getCellFirstSection(selection.anchorNode) : getSection(selection.anchorNode);
            var startOffset = Redige.$$.getSelectionOffset(selection.anchorNode, anchorOffset);
            if (selection.isCollapsed) {
                endSection = startSection;
                endOffset = startOffset;
            }
            else {
                endSection = getSection(selection.focusNode);
                endOffset = Redige.$$.getSelectionOffset(selection.focusNode, selection.focusOffset);
            }
            keepLastSelection(startSection, startOffset, endSection, endOffset);
            if (Redige.$$.isReversed(startSection, startOffset, endSection, endOffset)) {
                return {
                    startSection: endSection,
                    startOffset: endOffset,
                    startDivision: endSection.parent,
                    endSection: startSection,
                    endOffset: startOffset,
                    endDivision: startSection.parent,
                    isCollapsed: selection.isCollapsed,
                    anchorNode: selection.focusNode,
                    anchorOffset: selection.focusOffset,
                    focusOffset: anchorOffset,
                    focusNode: selection.anchorNode,
                    sections: Redige.getSectionsInRange(endSection, startSection)
                };
            }
            else {
                return {
                    startSection: startSection,
                    startOffset: startOffset,
                    startDivision: startSection.parent,
                    endSection: endSection,
                    endOffset: endOffset,
                    endDivision: endSection.parent,
                    isCollapsed: selection.isCollapsed,
                    anchorNode: selection.anchorNode,
                    anchorOffset: anchorOffset,
                    focusOffset: selection.focusOffset,
                    focusNode: selection.focusNode,
                    sections: Redige.getSectionsInRange(startSection, endSection)
                };
            }
        };

        Redige.getSectionsInRange = function (startSection, endSection) {
            var sections = [];
            if (endSection.parent === startSection.parent && startSection.parent !== undefined) {
                var brothers = Redige.toArray(startSection.parent.childNodes);
                var startIndex = brothers.indexOf(startSection);
                var endIndex = brothers.indexOf(endSection);
                for (var index = startIndex; index <= endIndex; index++) {
                    if (brothers[index].tagName === "RD-SECTION" || brothers[index].tagName === "RD-IMAGE") {
                        sections.push(brothers[index]);
                    }
                }
            }
            return sections;
        };

        Redige.$$.isReversed = function (startSection, startOffset, endSection, endOffset) {
            var sectionArray = Redige.queryAll(Redige._editor, "rd-section");
            var startSectionIndex = sectionArray.indexOf(startSection);
            var endSectionIndex = sectionArray.indexOf(endSection);
            if (startSectionIndex !== endSectionIndex) {
                return startSectionIndex >= endSectionIndex;
            }
            if (startOffset !== endOffset) {
                return startOffset >= endOffset;
            }
            return false;
        };

        Redige.$$.isGoodSpan = function (span, offset) {
            var startSpanOffset = Redige.$$.getSpanOffset(span);
            var length = span.textContent.length;
            var end = length + startSpanOffset;

            //If the span is not the first we should not include the lower limit
            if (span.previousSibling) {
                //console.log("is offset(" + offset + ") in ]" + startSpanOffset + "," + (length + startSpanOffset) + "] = " + (offset > startSpanOffset && offset <= end));
                return offset > startSpanOffset && offset <= end;
            }
            //If the span is the first offset 0 is in the span.
            else {
                //console.log("is offset(" + offset + ") in [" + startSpanOffset + "," + (length + startSpanOffset) + "] = " + (offset >= startSpanOffset && offset <= end));
                return offset >= startSpanOffset && offset <= end;
            }
        };

        /*
         * Find the Span in the given section that contains the offset
         * If the offset is to high (0x01), the last is returned
         */
        Redige.$$.findSpan = function (sectionId, offset) {
            var spans = Redige.toArray(Redige.getById(sectionId).childNodes);
            var result = spans.filter(function (element) {
                return Redige.$$.isGoodSpan(element, offset);
            });
            return result[0];
        };

        /*
         * Calculate the node #text offset in the span from the sectionOffset
         */
        Redige.$$.correctOffset = function (span, initialOffset) {
            if (span) {
                var offset = Redige.$$.getSpanOffset(span);
                initialOffset = initialOffset - offset;
                if (initialOffset > span.textContent.length) {
                    console.error("WTF ? offset is too big ?!");
                }
            }
            return initialOffset;
        };

        function defineTarget(span, offset) {
            if (span && span.firstChild && span.firstChild.nodeName === "#text") {
                return { element: span.firstChild, offset: offset };
            } else {
                console.error("WTF ? set selection to ?? a section with no span ??");
            }
        }

        Redige.setSelection = function (startSectionId, startOffset, endSectionId, endOffset) {
            var range;
            if (arguments.length === 4) {
                range = Redige.$$.setRangeSelection(startSectionId, startOffset, endSectionId, endOffset);
            }
            else if (arguments.length === 2) {
                range = Redige.$$.setCollapsedSelection(startSectionId, startOffset);
            }
            var sel = window.getSelection();
            sel.removeAllRanges();
            if (range) {
                sel.addRange(range);
            }
            selectionChangeHandler.fireEvent();
        };

        Redige.setSelectionToPreviousSection = function (element) {
            var previousElement = element.previousSibling;
            if (previousElement && previousElement.tagName === "RD-DIVISION") {
                var previousSection = previousElement.lastChild;
                Redige.setSelection(previousSection.id, previousSection.content.length);
            } else if (previousElement && previousElement.tagName === "RD-SECTION") {
                Redige.setSelection(previousElement.id, previousElement.content.length);
            }
        };

        Redige.setSelectionToNextSection = function (element) {
            var nextElement = element.nextSibling;
            if (nextElement && nextElement.tagName === "RD-DIVISION") {
                var nextSection = nextElement.lastChild;
                Redige.setSelection(nextSection.id, 0);
            } else if (nextElement && nextElement.tagName === "RD-SECTION") {
                Redige.setSelection(nextElement.id, 0);
            }
        };

        Redige.$$.setRangeSelection = function (startSectionId, startOffset, endSectionId, endOffset) {

            var startSpan = Redige.$$.findSpan(startSectionId, startOffset);
            var endSpan = Redige.$$.findSpan(endSectionId, endOffset);
            var startSpanOffset = Redige.$$.correctOffset(startSpan, startOffset);
            var endSpanOffset = Redige.$$.correctOffset(endSpan, endOffset);

            var target;
            var range = document.createRange(startSectionId, startOffset);

            target = defineTarget(startSpan, startSpanOffset, startSectionId);
            range.setStart(target.element, target.offset);

            target = defineTarget(endSpan, endSpanOffset, endSectionId);
            range.setEnd(target.element, target.offset);

            return range;
        };

        Redige.$$.setCollapsedSelection = function (startSectionId, startOffset) {
            var startSpan = Redige.$$.findSpan(startSectionId, startOffset);
            var startSpanOffset = Redige.$$.correctOffset(startSpan, startOffset);
            var target = defineTarget(startSpan, startSpanOffset, startSectionId);

            var range = document.createRange();
            range.setStart(target.element, target.offset);
            range.setEnd(target.element, target.offset);

            return range;
        };

        function keepLastSelection(startSection, startOffset, endSection, endOffset) {
            Redige.$$.previousSelection = {
                startSection: startSection,
                startOffset: startOffset,
                endSection: endSection,
                endOffset: endOffset
            };
        }

        Redige.restoreSelection = function () {
            if (!Redige.$$.previousSelection) {
                return;
            }
            Redige.setSelection(Redige.$$.previousSelection.startSection.id, Redige.$$.previousSelection.startOffset, Redige.$$.previousSelection.endSection.id, Redige.$$.previousSelection.endOffset);
        };

        /*
         In empty section, there is a \n character and window.getSelection() return anchorOffset = 1 instead of 0
         because content length is 1
         */
        function getAnchorOffset(selection) {
            if (selection.anchorOffset === 1 && selection.anchorNode.textContent === '\n') {
                return 0;
            }
            return selection.anchorOffset;
        }

        Redige.getWholeWord = function (sectionId, oldDecorator) {
            const section = Redige.getById(sectionId);
            const decorator = new Redige.Decorator();
            _.merge(decorator, oldDecorator);
            return DecoratorHandler.getWholeWord(section, decorator);
        }
    };
})();
