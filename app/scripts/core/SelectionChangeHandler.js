(function () {
    "use strict";
    /**
     * This handler is responsible for notify when the selection is changing by a text editor-ways (from mouse or keyboard)
     * All event that are modifying the selection except those from Redige.setSelection or Redige.patch.
     */
    var lastSelection = {};
    var Redige;

    function SelectionChangeHandler() {
    }

    function onEvent() {
        var selection = window.getSelection();
        if (hasChanged(selection)) {
            saveSelection(selection);
            fireEvent();
        }
    }

    function hasChanged(selection) {
        return selection.focusNode !== lastSelection.focusNode ||
            selection.anchorNode !== lastSelection.anchorNode ||
            selection.anchorOffset !== lastSelection.anchorOffset ||
            selection.focusOffset !== lastSelection.focusOffset;
    }

    function saveSelection(selection) {
        lastSelection.focusNode = selection.focusNode;
        lastSelection.anchorNode = selection.anchorNode;
        lastSelection.anchorOffset = selection.anchorOffset;
        lastSelection.focusOffset = selection.focusOffset;
    }

    function fireEvent() {
        Redige.dispatchEventByName( 'selectionChange');
    }

    SelectionChangeHandler.prototype.fireEvent = fireEvent;

    SelectionChangeHandler.prototype.init = function (aRedige) {
        Redige = aRedige;
        document.addEventListener("mouseup", onEvent);
        document.addEventListener("keyup", onEvent);
        return this;
    };

    module.exports = new SelectionChangeHandler();
})();