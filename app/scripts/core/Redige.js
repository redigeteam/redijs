(function () {

    "use strict";
    var Redige = {
        cache: {},
        $$: {}
    };
    window.Redige = Redige;
    require('./RedigeSelection')(Redige);
    require('./Redige.patch')(Redige);
    require('../model/Attachment')(Redige);
    require('../model/Decorator')(Redige);
    require('../model/Image')(Redige);
    require('../model/Section')(Redige);
    require('../model/Table')(Redige);
    require('../model/Document')(Redige);
    require('../model/Division')(Redige);
    require('../features/section/InsertHandler')(Redige);

    Redige.reset = function (newDoc) {
        Redige.cache = {};
        delete Redige.$$.previousSelection;
        Redige._editor.innerHTML = "";
        editor.appendChild(newDoc || Redige.Document.getEmptyDoc());
        Redige.dispatchEventByName(Redige._editor, "structureChanged");
    };

    Redige.getGUID = function () {
        return 'rxxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };

    Redige.init = function (editor, toolbar) {

        Redige._editor = editor;
        require('../features/clickDown/ClickDownHandler').init(editor);
        require('../features/copy/CopyHandler').init(editor);
        require('../features/cut/CutHandler').init(editor);
        require('../features/decorators/ShortcutHandler').init(editor);
        require('../features/links/LinkHandler').init(editor);
        require('../features/section/InputHandler').init(editor);
        Redige.$$.PasteHandler = require('../features/paste/PasteHandler').init(editor);
        require('../features/codeHighlight/CodeHighlight').init(editor);
        Redige.$$.NumberingHandler = require('../features/numbering/NumberingHandler').init(editor);
        require('../features/section/DeleteHandler').init(editor);
        require('../features/section/EnterHandler').init(editor);
        require('../features/section/SpaceHandler').init(editor);
        require('../features/section/SupprHandler').init(editor);
        require('../features/section/TabHandler').init(editor);
        require('../features/division/CtrlEnterHandler').init(editor);
        require('../features/rangeAction/RangeActionHandler').init(editor);
        Redige._toolbar = toolbar;
        editor.appendChild(Redige.Document.getEmptyDoc());

        Redige.dispatchEventByName(document, "rdg-ready-event");
    };

    //called by Redijswrapper (redige/redige-saas)
    Redige.parse = function () {
        return Redige._editor.querySelector("rd-document").parse();
    };

    Redige.insertAfter = function (element, node) {
        node.parentElement.insertBefore(element, node.nextSibling);
    };

    Redige.insertBefore = function (element, node) {
        node.parentElement.insertBefore(element, node);
    };

    Redige.replace = function (newElement, oldElement) {
        oldElement.parentElement.replaceChild(newElement, oldElement);
    };

    Redige.forEach = function (nodeList, iteratorFn) {
        Redige.toArray(nodeList).forEach(iteratorFn);
    };

    Redige.queryAll = function (node, query) {
        return Redige.toArray(node.querySelectorAll(query));
    };

    Redige.query = function (node, query) {
        return node.querySelector(query);
    };

    Redige.getById = function (id) {
        return Redige._editor.querySelector("#" + id);
    };

    Redige.getDocument = function () {
        return Redige._editor.querySelector("RD-DOCUMENT");
    };

    Redige.isEmpty = function () {
        const children = Redige._editor.querySelector("RD-DOCUMENT").childNodes;
        return children.length === 0 || children.length === 1 && children[0].content === "";
    };

    Redige.toArray = function (nodeList) {
        return Array.prototype.slice.call(nodeList);
    };

    Redige.remove = function (element) {
        element.parentNode.removeChild(element);
    };

    Redige.dispatchEventByName = function (node, eventName, params) {
        if (typeof node === "string") {
            eventName = node;
            node = Redige._editor;
        }
        /*
         * According to this PhanstomJS issue we can not use new Event(name,isbubble,isCancelable,any)
         * https://github.com/ariya/phantomjs/issues/11289
         */
        var evt = document.createEvent('CustomEvent');
        for (var key in params) {
            if (params.hasOwnProperty(key)) {
                evt[key] = params[key];
            }
        }
        evt.initCustomEvent(eventName, true, true, null);
        return node.dispatchEvent(evt);
    };

    module.exports = Redige;
})();