(function () {
    "use strict";

    var Promise = require('bluebird');
    var _ = require('lodash');
    var DecoratorHandler = require('../features/toolbar/DecoratorHandler');
    var DivisionHandler = require('../features/toolbar/DivisionHandler');
    var LinkHandler = require('../features/links/LinkHandler');

    module.exports = function (Redige) {
        Redige.patch = function (command) {
            var commandFn = commandHandlers[command.action];
            if (commandFn) {
                return commandHandlers[command.action](command);
            } else {
                return Promise.reject(new Error('command not found \'' + command.action + '\''));
            }
        };
    };

    var commandHandlers = {
        /**
         * {
         *   action:"load"
         *   data:<Object>|<String>|<Redige.Document>
         * }
         **/
        load: function (command) {
            return new Promise(function (resolve, reject) {
                var redigeDoc;

                if (command.data instanceof Redige.Document) {
                    redigeDoc = command.data;
                } else if (command.data instanceof Object) {
                    redigeDoc = new Redige.Document();
                } else {
                    redigeDoc = Redige.Document.getEmptyDoc();
                }

                try {
                    Redige.reset(redigeDoc);
                    if (command.data instanceof Object) {
                        redigeDoc.init(command.data);
                    }
                    Redige.dispatchEventByName("structureChanged");
                    resolve();
                } catch (err) {
                    console.debug("Fail to load content.", err);
                    reject(err);
                }
            });
        },
        /**
         * {
         *   action:"removeDecorator"
         *   sectionId:<String>
         *   decoratorId:<String>
         * }
         **/
        removeDecorator: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.sectionId);
                if (section) {
                    _.remove(section.decorators, { id: command.decoratorId });
                    section.refreshContent();
                    resolve();
                } else {
                    reject(new Error('no such id found ' + command.sectionId));
                }
            });
        },
        /**
         * {
         *   action:"removeDecorators"
         *   sectionId:<String>
         *   decoratorIds:[<String>]
         * }
         **/
        removeDecorators: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.sectionId);
                if (section) {
                    command.decoratorIds.forEach((id) => {
                        _.remove(section.decorators, { id: id });
                    });
                    section.refreshContent();
                    resolve();
                } else {
                    reject(new Error('no such id found ' + command.sectionId));
                }
            });
        },
        /**
         * {
         *   action:"addDecorator"
         *   sectionId:<String>
         *   decorator:{"end":number,"id":string,"start":number,"type":string}
         * }
         **/
        addDecorator: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.sectionId);
                if (section) {
                    var decorator = new Redige.Decorator(command.decorator);
                    section.decorators.push(decorator);
                    section.refreshContent();
                    resolve(decorator);
                } else {
                    reject(new Error('no such id found ' + command.sectionId));
                }
            });
        },
        /**
         * {
         *   action:"addDecorators"
         *   sectionId:<String>
         *   decorators:{"end":number,"id":string,"start":number,"type":string}
         * }
         **/
        addDecorators: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.sectionId);
                if (section) {
                    command.decorators.forEach((decorator) => {
                        section.decorators.push(new Redige.Decorator(decorator));
                    });
                    section.refreshContent();
                    resolve();
                } else {
                    reject(new Error('no such id found ' + command.sectionId));
                }
            });
        },
        /**
         * {
         *   action:"updateDecorator"
         *   sectionId:<String>
         *   decorator:{"end":number,"id":string,"start":number,"type":string,data:any}
         * }
         **/
        updateDecorator: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.sectionId);
                if (section) {
                    var decorator = _.find(section.decorators, { id: command.decorator.id });
                    if (decorator) {
                        decorator.start = _.defaultTo(command.decorator.start, decorator.start);
                        decorator.end = _.defaultTo(command.decorator.end, decorator.end);
                        decorator.type = command.decorator.type || decorator.type;
                        decorator.data = command.decorator.data || decorator.data;
                        section.refreshContent();
                        resolve();
                    }
                } else {
                    reject(new Error('no such id found ' + command.sectionId));
                }
            });
        },
        /**
         * {
         *   action:"toggleDecorator"
         *   sectionId:<String>
         *   decorator:{"end":number,"start":number,"type":string}
         * }
         **/
        toggleDecorator: function (command) {
            var section = Redige.getById(command.sectionId);
            var decorator = new Redige.Decorator();
            _.merge(decorator, command.decorator);
            return DecoratorHandler.toggleDecorator(section, decorator);
        },
        /**
         * {
         *   action:"updateSection"
         *   section:{id:string,level:string,type:string,content:string}
         * }
         **/
        updateSection: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.section.id);
                if (section) {
                    section.content = command.section.content || section.content;
                    section.type = command.section.type || section.type;
                    section.level = command.section.level || section.level;
                    section.data = command.section.data || section.data;
                    section.decorators = command.section.decorators || section.decorators;
                    section.refreshContent();
                    if (command.section.type || command.section.level) {
                        Redige.dispatchEventByName("structureChanged");
                    }
                    resolve(section);
                } else {
                    reject(new Error('no such id found ' + command.sectionId));
                }
            });
        },
        /**
         * {
         *   action:"updateDivision"
         *   division: {
         *      id: oldDivision.id,
          *     type: type
          *  }}
         * }
         **/
        updateDivision: function (command) {
            return new Promise(function (resolve, reject) {
                var division = Redige.getById(command.division.id);
                if (division) {
                    division.type = command.division.type || division.type;
                    resolve();
                } else {
                    reject(new Error('no such id found ' + command.division.id));
                }
            });
        },
        /**
         * {
         *   action:"deleteSection"
         *   sectionsIds: [string]
         * }
         **/
        deleteSection: function (command) {
            return new Promise(function (resolve) {
                command.sectionsIds.forEach(function (sectionId) {
                    Redige.remove(Redige.getById(sectionId));
                });
                resolve();
            });
        },
        /**
         * {
         *   action:"insertSections"
         *   divisionId:string | afterId:string
         *   sections: [{id:string,level:string,type:string,content:string,decorators:[{id:string,start:string,end:string,data:string,type:string}]}]
         * }
         **/
        insertSections: function (command) {
            function isLevelChanged(sections) {
                return sections.some(function (section) {
                    return section.level !== 0;
                });
            }

            return new Promise(function (resolve, reject) {
                var division = Redige.getById(command.divisionId);
                var after = Redige.getById(command.afterId);
                var createdSection = [];
                if (division) {
                    command.sections.forEach(function (section) {
                        var newSection = new Redige.Section();
                        newSection.init(section);
                        createdSection.push(newSection);
                        division.appendChild(newSection);
                    });

                    if (isLevelChanged(command.sections)) {
                        Redige.dispatchEventByName("structureChanged");
                    }
                    resolve(createdSection);
                } else if (after) {
                    command.sections.forEach(function (section) {
                        var newSection = new Redige.Section();
                        newSection.init(section);
                        createdSection.push(newSection);
                        Redige.insertAfter(newSection, after);
                        after = newSection;
                    });
                    if (isLevelChanged(command.sections)) {
                        Redige.dispatchEventByName("structureChanged");
                    }
                    resolve(createdSection);
                } else {
                    reject();
                }
            });
        },
        /**
         * {
         *   action:"insertSection"
         *   section:{id:string,level:string,type:string,content:string,decorators:[{id:string,start:string,end:string,data:string,type:string}]}
         *   beforeId: referenceSectionId
         *   (afterId: referenceSectionId)
         * }
         **/
        insertSection: function (command) {
            return new Promise(function (resolve) {

                let section = new Redige.Section();
                section.init(command.section);
                let ref;

                if (command.beforeId) {
                    ref = Redige.query(Redige._editor, '#' + command.beforeId);
                    if (ref) {
                        Redige.insertBefore(section, ref);
                    }
                } else if (command.afterId) {
                    ref = Redige.query(Redige._editor, '#' + command.afterId);
                    if (ref) {
                        Redige.insertAfter(section, ref);
                    }
                } else {
                    Redige.getDocument().appendChild(section);
                }
                if (section.level) {
                    Redige.dispatchEventByName("structureChanged");
                }
                resolve(section);
            });
        },
        /**
         * {
         *   action:"insertDivision"
         *   division: newDivision,
         *   afterId: oldDivision.id
         * }
         **/
        insertDivision: function (command) {
            return new Promise(function (resolve, reject) {
                var after = Redige.getById(command.afterId);
                if (after) {
                    var blankLine;
                    if (after instanceof Redige.Division && after.type !== 'default') {
                        blankLine = new Redige.Section();
                        Redige.insertAfter(blankLine, after);
                    }
                    var division = new Redige.Division();
                    division.init(command.division);
                    Redige.insertAfter(division, blankLine || after);
                    Redige.dispatchEventByName("structureChanged");
                    resolve(division);
                } else {
                    reject();
                }
            });
        },
        /**
         * {
         *   action:"deleteDivision"
         *   divisionId:string
         * }
         **/
        deleteDivision: function (command) {
            return new Promise(function (resolve, reject) {
                var division = Redige.getById(command.divisionId);
                if (division) {
                    Redige.remove(division);
                    resolve();
                } else {
                    reject();
                }
            });
        },
        /**
         * {
         *   action:"wrapInDivision"
         *   startSectionId:string
         *   endSectionId:string
         *   divisionType:string
         * }
         **/
        wrapInDivision: function (command) {
            var startSection = Redige.getById(command.startSectionId);
            var endSection = Redige.getById(command.endSectionId);
            if (startSection && endSection && startSection.parent === endSection.parent) {
                return DivisionHandler.wrapInDivision(command.divisionType, startSection, endSection);
            } else {
                return Promise.reject(new Error("missing sectionID or not in same Div Range"));
            }
        },
        /**
         * {
         *   action:"editLinkDecorator"
         *   decoratorId:string
         *   sectionId:string
         * }
         **/
        editLinkDecorator: function (command) {
            var section = Redige.getById(command.sectionId);
            if (section) {
                var decorator = _.find(section.decorators, { id: command.decoratorId });
                if (decorator) {
                    LinkHandler.handleEvent(decorator);
                }
            }
        },
        /**
         * {
         *   action:"insertImage"
         *   afterId:string
         *   data:string
         * }
         **/
        insertImage: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.getById(command.afterId);
                if (section) {
                    var result = new Redige.Image();
                    if (command.data) {
                        result.data = command.data;
                    }
                    Redige.insertAfter(result, section);
                    resolve(result);
                } else {
                    reject('no such id found ' + command.afterId);
                }
            });
        },
        /**
         * {
         *   action:"editImage"
         *   sectionId:string
         * }
         **/
        editImage: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.query(Redige._editor, '#' + command.sectionId);
                if (section) {
                    section.edit();
                    resolve(section);
                } else {
                    reject('no such id found ' + command.afterId);
                }
            });
        },
        /**
         * {
         *   action:"insertAttachment"
         *   afterId:string
         *   data:string
         * }
         **/
        insertAttachment: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.query(Redige._editor, '#' + command.afterId);
                if (section) {
                    var result = new Redige.Attachment();
                    result.data = command.data;
                    result.name = command.name;
                    Redige.insertAfter(result, section);
                    resolve(result);
                } else {
                    reject('no such id found ' + command.afterId);
                }
            });
        },
        /**
         * {
         *   action:"editAttachment"
         *   sectionId:string
         * }
         **/
        editAttachment: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.query(Redige._editor, '#' + command.sectionId);
                if (section) {
                    section.edit();
                    resolve(section);
                } else {
                    reject('no such id found ' + command.afterId);
                }
            });
        },
        /**
         * {
         *   action:"insertTable"
         *   afterId:string
         * }
         **/
        insertTable: function (command) {
            return new Promise(function (resolve, reject) {
                var section = Redige.query(Redige._editor, '#' + command.afterId);
                if (section) {
                    var result = new Redige.Table();
                    Redige.insertAfter(result, section);
                    resolve(result);
                } else {
                    reject('no such id found ' + command.afterId);
                }
            });
        },
    };
})();