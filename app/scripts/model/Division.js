(function () {
    "use strict";
    var CodeHighlight = require("../features/codeHighlight/CodeHighlight");
    module.exports = function (Redige) {
        Redige.Division = (function () {

            var proto = Object.create(HTMLDivElement.prototype);
            proto.createdCallback = function () {
                this.id = Redige.getGUID();
                this.setAttribute("type", "default");
                this.classList.add("division", "default");

                Object.defineProperty(this, 'type', {
                    get: function () {
                        return this.getAttribute("type");
                    },
                    set: function (newType) {
                        var oldType = this.getAttribute("type");

                        //remove the old type from classList
                        this.classList.remove(this.getAttribute("type"));
                        //update the type attribute
                        this.setAttribute("type", newType);
                        //update the classList
                        this.classList.add(newType);

                        if (newType === "blockcode") {
                            CodeHighlight.hightlightDivision(this);
                        }
                        if (oldType === 'blockcode' && newType !== "blockcode") {
                            CodeHighlight.cleanCodeDecorators(this);
                        }

                        if (newType !== "default") {
                            this.classList.remove("default");
                            addBlockControlElements(this);
                            if (newType === "blockcode") {
                                this.setAttribute("spellcheck", false);
                            }
                        } else {
                            removeBlockControl(this);
                            this.removeAttribute("spellcheck");
                        }
                        if (newType === 'default' || oldType === "default") {
                            Redige.dispatchEventByName(this, "structureChanged");
                        }
                    }

                });

                Object.defineProperty(this, 'children', {
                    get: function () {
                        return Redige.toArray(this.querySelectorAll("rd-section, rd-image, rd-attachment"));
                    }
                });

                Object.defineProperty(this, 'parent', {
                    get: function () {
                        return this.parentElement;
                    }
                });

                Object.defineProperty(this, 'content', {
                    get: function () {
                        return this.textContent;
                    }
                });

                Object.defineProperty(this, 'firstChild', {
                    get: function () {
                        if (this.getAttribute('type') !== "default" && this.children.length > 1) {
                            return this.children[1];
                        } else {
                            return this.children[0];
                        }
                    },
                    set: function (newFirstChild) {
                        var index = (this.getAttribute('type') !== 'default') ? 0 : 1;
                        if (this.children[index].nextSibling) {
                            this.children[index].parentNode.replaceChild(newFirstChild, this.children[index].nextSibling);
                        } else {
                            this.children[index].insertBefore(newFirstChild, this.children[index]);
                            this.children[index].removeChild(this.children[index]);
                        }
                    }
                });
            };

            var Division = document.registerElement('rd-division', { prototype: proto });

            Division.prototype.init = function (data) {
                if (data.type) {
                    this.type = data.type;
                }
                if (data.id) {
                    this.id = data.id;
                }
                if (Array.isArray(data.children)) {
                    for (var index = 0; index < data.children.length; index++) {
                        if (data.children[index].type === 'image') {
                            this.appendChild(new Redige.Image().init(data.children[index]));
                        } else if (data.children[index].type === 'attachment') {
                            this.appendChild(new Redige.Attachment().init(data.children[index]));
                        } else if (data.children[index].type === 'table') {
                            this.appendChild(new Redige.Table().init(data.children[index]));
                        } else {
                            this.appendChild(new Redige.Section().init(data.children[index]));
                        }
                    }
                }
                if (this.type === 'blockcode') {
                    CodeHighlight.hightlightDivision(this);
                }

                return this;
            };

            Division.prototype.parse = function () {
                var result = {};
                result.id = this.id;
                result.type = this.type;
                result.children = [];
                Redige.forEach(this.childNodes, function (sectionElement) {
                    if (sectionElement.nodeName === "RD-SECTION" || sectionElement.nodeName === "RD-IMAGE" || sectionElement.nodeName === "RD-ATTACHMENT") {
                        result.children.push(sectionElement.parse());
                    }
                });
                return result;
            };

            Division.getEmptyDivision = function () {
                var result = new Division();
                var section = new Redige.Section();
                result.appendChild(section);
                return result;
            };

            return Division;
        })();


        function removeBlockControl(div) {
            var blockControls = div.getElementsByClassName('block-control');
            if (blockControls.length > 0) {
                div.removeChild(blockControls[0]);
            }
        }

        function addBlockControlElements(div) {
            var blockControlDiv = tomplate(
                '<div class="block-control" contenteditable="false">' +
                '<button class="block-control-remove" onclick="{onRemove()}"><i class="mdi mdi-minus"></i></button>' +
                '</div>',
                {
                    onRemove: function () {
                        Redige.dispatchEventByName("blockRemoved");
                        Redige.setSelectionToPreviousSection(div);
                        div.parentElement.removeChild(div);
                    }
                });
            div.appendChild(blockControlDiv);
        };
    }
})();