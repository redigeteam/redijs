(function () {
    "use strict";
    module.exports = function (Redige) {
        Redige.Table = (function () {

            var proto = Object.create(HTMLTableElement.prototype);
            proto.createdCallback = function () {
                this.id = Redige.getGUID();
                this.classList.add("section", "table");
                this.setAttribute("type", "table");
                this.setAttribute("contenteditable", false);
                initContent(this);

                Object.defineProperty(this, 'type', {
                    get: function () {
                        return this.getAttribute("type");
                    }
                });

                Object.defineProperty(this, 'columnsCount', {
                    get: function () {
                        return this.firstChild.rows[0].cells.length;
                    }
                });

                Object.defineProperty(this, 'rowsCount', {
                    get: function () {
                        return this.firstChild.rows.length;
                    }
                });

                Object.defineProperty(this, 'rows', {
                    get: function () {
                        return this.firstChild.rows;
                    }
                });

                Object.defineProperty(this, 'lastRow', {
                    get: function () {
                        var rows = this.firstChild.rows;
                        return rows[rows.length - 1];
                    }
                });

                Object.defineProperty(this, 'lastCell', {
                    get: function () {
                        var rows = this.firstChild.rows;
                        var cells = rows[rows.length - 1].cells;
                        return cells[cells.length - 2];
                    }
                });

                Object.defineProperty(this, 'firstCell', {
                    get: function () {
                        var rows = this.firstChild.rows;
                        var cells = rows[0].cells;
                        return cells[0];
                    }
                });
            };

            var rdTable = document.registerElement('rd-table', {prototype: proto});

            rdTable.prototype.init = function (data) {
                if (data.type) {
                    this.type = data.type;
                }
                return this;
            };

            rdTable.prototype.insertRow = function (index) {
                var position = index || -1;
                var row = this.firstChild.insertRow(position);
                for (var i = 0; i < this.columnsCount; i++) {
                    var cell = row.insertCell();
                    cell.appendChild(new Redige.Section());
                }
                addRowRemove(row, this);

                return row;
            };

            rdTable.prototype.insertColumn = function () {
                var rows = this.firstChild.rows;
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    var cell = row.insertCell(row.cells.length - 1);
                    cell.appendChild(new Redige.Section());
                    setRowWidth(row);
                }
            };

            function initContent(elem) {
                var table = document.createElement('TABLE');
                var firstRow = table.insertRow();
                var firstRowFirstCell = firstRow.insertCell();
                firstRowFirstCell.appendChild(new Redige.Section());
                var firstRowSecondCell = firstRow.insertCell();
                firstRowSecondCell.appendChild(new Redige.Section());
                addRowRemove(firstRow, elem);
                setRowWidth(firstRow);

                var secondRow = table.insertRow();
                var secondRowFirstCell = secondRow.insertCell();
                secondRowFirstCell.appendChild(new Redige.Section());
                var secondRowSecondCell = secondRow.insertCell();
                secondRowSecondCell.appendChild(new Redige.Section());
                addRowRemove(secondRow, elem);
                setRowWidth(secondRow);

                elem.appendChild(table);

                elem.appendChild(tomplate(
                    '<div class="block-control" contenteditable="false">' +
                    '   <button class="block-control-add-row" onclick="{onAddRow()}">' +
                    '       <i class="mdi mdi-table-row-plus-after"></i>' +
                    '   </button>' +
                    '   <button class="block-control-add-column" onclick="{onAddColumn()}">' +
                    '       <i class="mdi mdi-table-column-plus-after"></i>' +
                    '   </button>' +
                    '   <button class="block-control-remove" onclick="{onRemove()}">' +
                    '       <i class="mdi mdi-minus"></i>' +
                    '   </button>' +
                    '</div>', {
                        onAddRow: function () {
                            elem.insertRow();
                        },
                        onAddColumn: function () {
                            elem.insertColumn();
                        },
                        onRemove: function () {
                            Redige.setSelectionToPreviousSection(elem);
                            elem.parentElement.removeChild(elem);
                        }
                    }
                ));
            }

            rdTable.prototype.parse = function () {
                return {
                    id: this.id,
                    rows: this.rows,
                    type: 'table'
                };
            };

            function addRowRemove(row, rdtable) {
                row.appendChild(tomplate(
                    '<div class="row-control" contenteditable="false">' +
                    '   <button class="row-control-remove" onclick="{onRemoveRow()}">' +
                    '       <i class="mdi mdi-close"></i>' +
                    '   </button>' +
                    '</div>', {
                        onRemoveRow: function () {
                            row.parentElement.removeChild(row);
                            if (rdtable.rowsCount === 0 || rdtable.columnsCount === 0) {
                                rdtable.parentElement.removeChild(rdtable);
                            }

                        }
                    }
                ));
            }

            function setRowWidth(row) {
                var width = 100 / row.cells.length + 1;
                for (var i = 0; i < row.cells.length; i++) {
                    row.cells[i].style.width = width + "%";
                }
            }

            return rdTable;
        })();
    };
})();