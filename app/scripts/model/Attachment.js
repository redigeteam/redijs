(function () {
    "use strict";

    var attachmentInput = require('../features/attachment/attachmentInput');

    module.exports = function (Redige) {
        Redige.Attachment = (function () {
            var tomplate = require('tomplate');

            var proto = Object.create(HTMLDivElement.prototype);
            proto.createdCallback = function () {
                this.id = Redige.getGUID();
                this.className = "section attachment";
                this.setAttribute("type", "attachment");
                this.setAttribute("contenteditable", "false");
                this._data = "";
                this._name = "";
                this._renderElement = initContent(this);
                this._editElement = attachmentInput.createAttachmentInput(this);
                var link = this._renderElement.querySelector('a');
                Object.defineProperty(this, 'data', {
                    get: function () {
                        return this._data;
                    },
                    set: function (newData) {
                        this._data = newData;
                        link.href = this._data;
                    }
                });
                Object.defineProperty(this, 'parent', {
                    get: function () {
                        return this.parentNode;
                    }
                });
                Object.defineProperty(this, 'name', {
                    get: function () {
                        return this._name;
                    },
                    set: function (newName) {
                        this._name = newName;
                        link.download = this._name;
                        link.innerHTML = this._name;
                    }
                });
                this.appendChild(this._renderElement);
            };

            proto.edit = function () {
                this._renderElement.style.display = 'none';
                this.appendChild(this._editElement);
                this.classList.add('edit');
            };

            proto.render = function () {
                if (this._editElement.parentNode === this) this.removeChild(this._editElement);
                this.classList.remove('edit');
                this._renderElement.style.display = 'inherit';
            };

            proto.refreshContent = function () {
                this.render();
            };

            proto.parse = function () {
                return {
                    id: this.id,
                    data: this.data,
                    name: this.name,
                    type: 'attachment'
                };
            };

            proto.init = function (data) {
                if (data.id) {
                    this.id = data.id;
                }

                if (data.data) {
                    this.data = data.data;
                }

                if (data.name) {
                    this.name = data.name;
                }
                return this;
            };

            function initContent(attachment) {
                return tomplate('<div>' +
                    '<i class="attachment-icon mdi mdi-paperclip"></i>' +
                    '<a class="attachment-link" download="' + attachment.name + '" href="' + attachment.data + '"/>' + attachment.name + '</a>' +
                    '<div class="spacer"></div>' +
                    '<div class="block-control" contenteditable="false"><button class="block-control-remove" onclick="{onRemove()}"><i class="mdi mdi-minus"></i></button></div>' +
                    '</div>', {
                        onRemove: function () {
                            Redige.setSelectionToPreviousSection(attachment);
                            Redige.patch({action: 'deleteSection', sectionsIds: [attachment.id]});
                        }
                    }
                );
            }

            return document.registerElement('rd-attachment', {prototype: proto});
        })();
    };
})();