(function () {
    "use strict";
    const DefaultTemplate = require("../../template/default/index");
    const _ = require('lodash');

    module.exports = function (Redige) {
        Redige.Document = (function () {

            const proto = Object.create(HTMLDivElement.prototype);
            proto.createdCallback = function () {
                this.id = Redige.getGUID();
                this.type = "default";
                Object.defineProperty(this, 'children', {
                    get: function () {
                        return Redige.toArray(this.childNodes);
                    }
                });
            };

            const Document = document.registerElement('rd-document', {prototype: proto});

            Document.prototype.init = function (data) {
                Object.assign(this, _.omit(data, ['children']));
                if (!data.children) {
                    this.appendChild(new Redige.Section());
                } else if (Array.isArray(data.children)) {
                    for (let index = 0; index < data.children.length; index++) {
                        if (data.children[index].type === 'image') {
                            this.appendChild(new Redige.Image().init(data.children[index]));
                        } else if (data.children[index].type === 'attachment') {
                            this.appendChild(new Redige.Attachment().init(data.children[index]));
                        } else if (data.children[index].type === 'table') {
                            this.appendChild(new Redige.Table().init(data.children[index]));
                        } else if (data.children[index].children) {
                            this.appendChild(new Redige.Division().init(data.children[index]));
                        } else {
                            this.appendChild(new Redige.Section().init(data.children[index]));
                        }
                    }
                }
                return this;
            };

            Document.prototype.parse = function () {
                const result = {};
                result.id = this.id;
                if (this.template !== undefined) result.template = {name: this.template.name};
                Object.assign(result, _.pick(this, _.keys(this)));
                result.children = [];
                Redige.forEach(this.childNodes, function (divisionElement) {
                    result.children.push(divisionElement.parse());
                });
                return result;
            };

            Document.getEmptyDoc = function () {
                const result = new Document();
                result.appendChild(new Redige.Section());
                return result;
            };

            function observeDocument(doc) {
                var observer = new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        mutation.addedNodes.forEach(function (node) {
                            // Observing Divisions modifications
                            if (node.nodeName === "RD-DIVISION" && node.type !== "default") {
                                if (!node.previousSibling || node.previousSibling.type !== "default") {
                                    Redige.insertBefore(new Redige.Division.getEmptyDivision(), node);
                                } else if (node.previousSibling && node.previousSibling.lastChild.nodeName !== "RD-SECTION") {
                                    Redige.insertAfter(new Redige.Section(), node.previousSibling.lastChild);
                                }
                                if (!node.nextSibling || node.nextSibling.type !== "default") {
                                    Redige.insertAfter(new Redige.Division.getEmptyDivision(), node);
                                } else if (node.nextSibling && node.nextSibling.firstChild.nodeName !== "RD-SECTION") {
                                    Redige.insertBefore(new Redige.Section(), node.nextSibling.firstChild);
                                }
                            }
                            // Observing Sections modifications
                            else if (node.nodeName === "RD-ATTACHMENT" || node.nodeName === "RD-IMAGE" || node.nodeName === "RD-TABLE" || node.nodeName === "RD-ATTACHMENT-INPUT" || node.nodeName === "RD-IMAGE-INPUT") {
                                if (!node.previousSibling || node.previousSibling.nodeName !== "RD-SECTION") {
                                    Redige.insertBefore(new Redige.Section(), node);
                                }
                                if (!node.nextSibling || node.nextSibling.nodeName !== "RD-SECTION") {
                                    Redige.insertAfter(new Redige.Section(), node);
                                }
                            }
                        });

                        mutation.removedNodes.forEach(function (node) {
                            if (node.nodeName === "RD-DIVISION" && node.type !== "default") {
                                if (!doc.contains(node)) {
                                    var prev = mutation.previousSibling;
                                    var next = mutation.nextSibling;
                                    if (doc.contains(prev) && prev.type === "default" && doc.contains(next) && next.type === "default" && prev.nextSibling === next) {
                                        Redige.toArray(next.childNodes).forEach(function (element) {
                                            prev.appendChild(element);
                                        });
                                        next.parentElement.removeChild(next);
                                    }
                                }
                            }
                        });
                    });
                });

                observer.observe(doc, {childList: true, subtree: true});
            }

            return Document;
        })();
    }
})();