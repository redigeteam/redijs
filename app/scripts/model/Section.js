(function () {
    "use strict";
    module.exports = function (Redige) {
        Redige.Section = (function () {

            var proto = Object.create(HTMLParagraphElement.prototype);
            proto.createdCallback = function () {
                this.id = Redige.getGUID();
                this.setAttribute("contenteditable", true);
                this.decorators = [];
                this.className = "section";

                Object.defineProperty(this, 'type', {
                    get: function () {
                        return this.getAttribute("type");
                    },
                    set: function (newType) {
                        //remove the old type from classList
                        this.classList.remove(this.getAttribute('type'));
                        //update the type attribute
                        this.setAttribute("type", newType);
                        //update the classList
                        this.classList.add(newType);
                    }
                });

                Object.defineProperty(this, 'level', {
                    get: function () {
                        return this.getAttribute("level");
                    },
                    set: function (newLevel) {
                        //remove the old type from classList
                        this.classList.remove("level-" + this.getAttribute('level'));
                        //update the type attribute
                        this.setAttribute("level", newLevel);
                        //update the classList
                        this.classList.add("level-" + newLevel);
                    }
                });

                Object.defineProperty(this, 'content', {
                    get: function () {
                        return this.textContent.replace("\n", "");
                    },
                    set: function (content) {
                        contentToElement(this, content.replace("\n", ""));
                    }
                });

                Object.defineProperty(this, 'parent', {
                    get: function () {
                        return this.parentNode;
                    }
                });

                Object.defineProperty(this, 'numbering', {
                    set: function (number) {
                        if (number != null) {
                            this.setAttribute("numbering", number);
                        } else {
                            this.removeAttribute("numbering")
                        }
                    },
                    get: function () {
                        return this.getAttribute("numbering");
                    }
                });
                Object.defineProperty(this, 'localNumbering', {
                    set: function (number) {
                        if (number != null) {
                            this.setAttribute("local-numbering", number);
                        } else {
                            this.removeAttribute("local-numbering")
                        }
                    },
                    get: function () {
                        return this.getAttribute("local-numbering");
                    }
                });

                this.level = "0";
                this.type = "hierarchical";

                contentToElement(this, "");
            };

            var Section = document.registerElement('rd-section', {prototype: proto});

            Section.prototype.init = function (data) {
                if (data.id) {
                    this.id = data.id;
                }
                if (data.type) {
                    this.type = data.type;
                }
                if (data.level) {
                    this.level = data.level;
                }
                if (Array.isArray(data.decorators)) {
                    this.decorators = data.decorators.map(addDecoratorIds);
                }
                if (data.content) {
                    this.content = data.content;
                }
                return this;
            };

            function addDecoratorIds(decorator) {
                if (!decorator.id) {
                    decorator.id = Redige.getGUID();
                }
                return decorator;
            }

            /*
             * From DOM to Model object (parse DOM)
             */
            Section.prototype.parse = function () {
                var result = {};
                result.id = this.id;
                result.type = this.type;
                result.level = this.level;
                result.numbering = this.numbering;
                result.decorators = [];
                this.decorators.forEach(function (dec) {
                    if (!dec.transient) {
                        result.decorators.push(dec);
                    }
                });
                result.content = this.content;
                return result;
            };

            Section.prototype.refreshContent = function () {
                contentToElement(this, this.content);
            };

            /*
             * Section DOM representation is a ordered list of span with classes
             * This method is use to know if the decorator list is changing (if a new span is required)
             */
            function isDecoratorChange(decorators, offset) {
                var result = false;
                decorators.some(function (decorator) {
                    if ((offset === decorator.start) || (offset === decorator.end)) {
                        result = true;
                        return true;
                    }
                });
                return result;
            };
            /*
             * This method will define the list of decorators class for a given offset
             */
            function buildDecoratorClass(decorators, offset) {
                var result = [];
                decorators.forEach(function (decorator) {
                    if ((offset >= decorator.start) && (offset < decorator.end)) {
                        result.push(decorator.type);
                    }
                });
                return result;
            };
            /*
             * This method will define the list of decorators id for a given offset
             */
            function buildDecoratorIdList(decorators, offset) {
                var result = [];
                decorators.forEach(function (decorator) {
                    if ((offset >= decorator.start) && (offset < decorator.end)) {
                        result.push(decorator.id);
                    }
                });
                return result;
            };

            /*
             * Apply a span change : a new decorator lists require to split section text.
             */
            function changeSpan(decorators, offset) {
                var result = document.createElement("SPAN");
                result.className = buildDecoratorClass(decorators, offset).join(" ");
                result.setAttribute("decoratoridlist", buildDecoratorIdList(decorators, offset).join(","));
                return result;
            };

            /*
             * Build the P section content from textContent and decorator list
             */
            function contentToElement(elem, content) {
                elem.innerHTML = "";
                var currentSpan;
                for (var index = 0; index < content.length; index++) {
                    var currentChar = content.substring(index, index + 1);
                    if (!currentSpan || isDecoratorChange(elem.decorators, index)) {
                        currentSpan = changeSpan(elem.decorators, index);
                        elem.appendChild(currentSpan);
                    }
                    currentSpan.textContent += currentChar;
                }
                if (!currentSpan) {
                    currentSpan = document.createElement("SPAN");
                    currentSpan.innerHTML = "\n";
                    elem.appendChild(currentSpan);
                }
            };

            return Section;
        })();
    };
})();