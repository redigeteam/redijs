(function () {
    const _ = require('lodash');
    "use strict";
    module.exports = function (Redige) {
        Redige.Decorator = (function () {
            function Decorator(toCopy) {
                this.id = Redige.getGUID();
                _.merge(this, toCopy);
            }

            return Decorator;
        })();
    }
})();