(function () {
    "use strict";
    /*
     * this code prevent default shortcut in editablecontent
     * Ctrl-U, Ctrl-B, Ctrl-I : which add Underline, Bold and Italic html tags.
     */
    function ShortcutHandler() {
    }

    ShortcutHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("keydown", function (event) {
                if (event.ctrlKey || (event.metaKey && Redige.isMacOs()) && !event.altKey) {
                    //Ctrl+B
                    if (event.keyCode === 66) {
                        toggleDecoratorCommand('bold');
                        event.preventDefault();
                        return true;
                    }
                    //Ctrl+I
                    else if (event.keyCode === 73) {
                        toggleDecoratorCommand('italic');
                        event.preventDefault();
                        return true;
                    }
                    //Ctrl+U
                    else if (event.keyCode === 85) {
                        event.preventDefault();
                        return false;
                    }
                }
            }
        );
    };

    function toggleDecoratorCommand(type) {
        var currentSelection = Redige.getDocumentSelection();
        Redige.patch({
            action: 'toggleDecorator',
            sectionId: currentSelection.startSection.id,
            decorator: {start: currentSelection.startOffset, end: currentSelection.endOffset, type: type}
        });
    }

    module.exports = new ShortcutHandler();
})();