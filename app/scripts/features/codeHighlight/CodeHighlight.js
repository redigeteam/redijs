(function () {
    //color code syntax
    "use strict";
    var languageDetector = require("./LanguageDetector");
    var Prism = require("prismjs");
    var _ = require("lodash");
    var editor;

    function CodeHighlight() {
    }

    CodeHighlight.prototype.init = function (rootNode) {
        editor = rootNode;
        rootNode.addEventListener("input", function () {
            var section = Redige.getDocumentSelection().startSection;
            if (section.parent && section.parent.type === "blockcode") {
                var offset = Redige.getDocumentSelection().startOffset;
                hightlight(section);
                Redige.setSelection(section.id, offset);
            }
        });
        rootNode.addEventListener("paste", function () {
            var section = Redige.getDocumentSelection().startSection;
            if (section.parent.type === "blockcode") {
                hightlightDivision(section.parent);
            }
        });
    };

    CodeHighlight.prototype.hightlightDivision = hightlightDivision;

    function hightlightDivision(div) {
        var language = languageDetector.detect(div.content);
        if (language && Prism.languages[language]) {
            div.language = language;
            div.children.forEach(function (aSection) {
                addDecorators(aSection, div.language);
            });
        }
    }

    CodeHighlight.prototype.cleanCodeDecorators = function (division) {
        division.children.forEach(function (section) {
            removeCodeDecorators(section);
            section.refreshContent();
        });
    };

    function removeCodeDecorators(section) {
        section.decorators = _.filter(section.decorators, function (decorator) {
            return decorator.type.indexOf("code-") !== 0;
        });
    }

    function hightlight(section) {
        var divisionLanguage = section.parent.language;
        var fistDetection = false;
        if (!divisionLanguage) {

            var language = languageDetector.detect(section.parentNode.textContent);
            if (language && Prism.languages[language]) {
                section.parent.language = language;
                divisionLanguage = language;
                section.parent.children.forEach(function (aSection) {
                    if (aSection instanceof Redige.Section) {
                        addDecorators(aSection, divisionLanguage);
                    }
                });
                fistDetection = true;
            } else {
                console.log("unknown language");
            }
        }
        if (divisionLanguage && !fistDetection) {
            removeCodeDecorators(section);
            addDecorators(section, divisionLanguage);
        }
    }

    function addDecorators(section, language) {
        var tokens = Prism.tokenize(section.content, Prism.languages[language]);
        removeCodeDecorators(section.decorators);
        var currentIndex = 0;
        var flattenTokens = flattenToken(tokens);
        flattenTokens.forEach(function (token) {
            if (typeof token !== 'string') {
                var decorator = new Redige.Decorator();
                decorator.type = 'code-syntax-' + token.type;
                decorator.transient = true;
                decorator.start = currentIndex;
                currentIndex += token.matchedStr.length;
                decorator.end = currentIndex;
                section.decorators.push(decorator);
            } else {
                currentIndex += token.length;
            }
        });
        section.refreshContent();
    }

    function flattenToken(tokenArray, parentType) {
        var result = [];
        tokenArray.forEach(function (token) {
            if (Array.isArray(token.content)) {
                result.push.apply(result, flattenToken(token.content, token.type));
            } else if (typeof token !== 'string') {
                result.push(token);
            } else {
                if (parentType) {
                    result.push({ type: parentType, matchedStr: token });
                } else {
                    result.push(token);
                }
            }
        });
        return result;
    }

    module.exports = new CodeHighlight();
})();