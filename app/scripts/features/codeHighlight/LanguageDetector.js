(function () {
    "use strict";
    var detectLang = require('lang-detector');


    var languagePrismMapping = {
        'JavaScript': 'javascript',
        'C': 'clike',
        'C++': 'cpp',
        'Python': 'python',
        'Java': 'java',
        'HTML': 'html',
        'CSS': 'css',
        'Ruby': 'ruby',
        'Go': 'go',
        'PHP': 'php'
    };

    function detect(input) {
        var result = detectLang(input);
        return languagePrismMapping[result];
    }

    module.exports = {detect: detect};

})();