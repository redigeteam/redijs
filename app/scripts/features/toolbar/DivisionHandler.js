(function () {
    "use strict";

    function DivisionHandler() {
    }

    DivisionHandler.wrapInDivision = function (type, startSection, endSection) {
        var oldDivision = startSection.parentElement;
        //Si la cible et la division courante sont de types différents
        if (oldDivision.type !== type) {
            //La selection concerne déjà une division spécifique, on change juste son type
            if (!isDefault(oldDivision)) {
                return Redige.patch({
                    action: 'updateDivision',
                    division: {id: oldDivision.id, type: type}
                }).then(function () {
                    Redige.dispatchEventByName("blockRemoved");
                    Redige.restoreSelection();
                });
            }
            else {
                return splitDefaultDivision(type, startSection, endSection).then(function () {
                    Redige.restoreSelection();
                });
            }
        }
        //Si on re-clique sur un bloc quote/code déjà fait alors on unwrap section to document
        else {
            if (!isDefault(oldDivision)) {
                var oldDivStartSection = oldDivision.children[0];
                var oldDivEndSection = oldDivision.children[oldDivision.children.length - 1];
                return unWrapDivision(oldDivision).then(function () {
                    Redige.setSelection(oldDivStartSection.id, 0, oldDivEndSection.id, oldDivEndSection.content.length);
                });
            }
        }
        return Promise.resolve();
    };

    function splitDefaultDivision(type, startSection, endSection) {

        var newDivision = {type: type, children: []};
        var sectionArray = startSection.parent.children;
        var startSectionIndex = sectionArray.indexOf(startSection);
        var endSectionIndex = sectionArray.indexOf(endSection);
        var sectionToRemove = [];

        for (var index = startSectionIndex; index <= endSectionIndex; index++) {
            if (index >= startSectionIndex && index <= endSectionIndex) {
                newDivision.children.push(sectionArray[index].parse());
                sectionToRemove.push(sectionArray[index].id);
            }
        }

        return Redige.patch({
            action: 'insertDivision', division: newDivision, afterId: endSection.id
        }).then(function () {
            return Redige.patch({action: 'deleteSection', sectionsIds: sectionToRemove});
        }).then(function () {
            const lastChild = newDivision.children[newDivision.children.length - 1];
            Redige.setSelection(lastChild.id, lastChild.content.length);
        });
    }

    function unWrapDivision(division) {
        return Redige.patch({
            action: 'insertSections',
            afterId: division.id,
            sections: division.children
        }).then(function () {
            return Redige.patch({action: 'deleteDivision', divisionId: division.id});
        });
    }

    function isDefault(divisionElement) {
        return divisionElement.type === "default";
    }

    module.exports = DivisionHandler;
})();