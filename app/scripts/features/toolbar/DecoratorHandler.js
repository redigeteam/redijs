(function () {
    "use strict";

    var Promise = require('bluebird');

    function DecoratorHandler() {
    }

    DecoratorHandler.toggleDecorator = function (section, decorator) {
        return new Promise(function (resolve, reject) {
            var isCollapsed = decorator.start === decorator.end;

            var decoratorAlreadyExists = isDecoratorConflict(section, decorator);
            if (isCollapsed && decoratorAlreadyExists !== null) {
                Redige.patch({
                    action: 'removeDecorator',
                    sectionId: section.id,
                    decoratorId: decoratorAlreadyExists.id
                }).then(function () {
                    Redige.setSelection(section.id, decorator.start);
                    resolve();
                });
            }
            /* If we add a new decorator to a current active word add the decorator to the current word (whitespace as stop char) */
            else if (isCollapsed && decoratorAlreadyExists === null) {
                var selectionOffset = decorator.start;
                decorator.start = getCurrentWordStartOffset(section, decorator);
                decorator.end = getCurrentWordEndOffset(section, decorator);
                Redige.patch({
                    action: 'addDecorator',
                    sectionId: section.id,
                    decorator: decorator
                }).then(function (decorator) {
                    Redige.setSelection(section.id, selectionOffset);
                    resolve(decorator);
                });
            }
            /* If we add decorator with no included same decorator, just add decorator */
            else if (!isCollapsed && decoratorAlreadyExists === null) {
                Redige.patch({
                    action: 'addDecorator',
                    sectionId: section.id,
                    decorator: decorator
                }).then(function () {
                    Redige.setSelection(section.id, decorator.start, section.id, decorator.end);
                    resolve();
                });
            }
            /*If there is conflict with decorator merge it*/
            else if (!isCollapsed && decoratorAlreadyExists !== null) {
                var promise;
                if (decoratorAlreadyExists.end === decorator.end && decoratorAlreadyExists.start === decorator.start) {
                    promise = Redige.patch({
                        action: 'removeDecorator',
                        sectionId: section.id,
                        decoratorId: decoratorAlreadyExists.id
                    });
                }
                else {
                    promise = mergeDecorators(section, decorator, decoratorAlreadyExists);
                }
                promise.then(function () {
                    Redige.setSelection(section.id, decorator.start, section.id, decorator.end);
                    resolve();
                });
            } else {
                reject(new Error("ToggleDecorator can't apply in this context."));
            }
        });
    };

    DecoratorHandler.getWholeWord = function (section, decorator) {
        return new Promise((resolve, reject) => {
            const isCollapsed = decorator.start === decorator.end;
            const decoratorAlreadyExists = isDecoratorConflict(section, decorator);
            
            if (isCollapsed && decoratorAlreadyExists === null) {
                decorator.start = getCurrentWordStartOffset(section, decorator);
                decorator.end = getCurrentWordEndOffset(section, decorator);
                resolve(decorator);
            } else {
                reject(new Error("GetWholeWord can't apply in this context"));
            }
        });
    };

    function getCurrentWordStartOffset(section, decorator) {
        var index = decorator.start;
        var text = section.content;
        var char = text.charAt(index);
        //In case of selected stop char : the word is the previous one, continue to iterate.
        if (isStopWord(char)) {
            index--;
            char = text.charAt(--index);
        }
        while (index > 0) {
            char = text.charAt(index);
            if (isStopWord(char)) {
                return ++index;
            }
            index--;
        }

        return index;
    }

    function getCurrentWordEndOffset(section, decorator) {
        var index = decorator.start;
        var text = section.content;
        var char = text.charAt(index);
        while (index < text.length && !isStopWord(char)) {
            char = text.charAt(++index);
        }
        return index;
    }

    function isStopWord(char) {
        return !char.toString().match(/[0-9a-z\u00E0-\u00FC\-\_\\\']/i);
    }

    function mergeDecorators(section, decorator, oldDecorator) {
        var newValues = { id: oldDecorator.id };
        if (decorator.start < oldDecorator.start) {
            newValues.start = decorator.start;
        }
        if (decorator.end > oldDecorator.end) {
            newValues.end = decorator.end;
        }
        return Redige.patch({
            action: 'updateDecorator',
            sectionId: section.id,
            decorator: newValues
        });
    }

    function isDecoratorConflict(section, decorator) {
        var start = decorator.start;
        var end = decorator.end;
        var result = null;
        section.decorators.forEach(function (dec) {
            if (dec.type === decorator.type) {
                if ((start >= dec.start && start <= dec.end) || (end >= dec.start && end <= dec.end) || (dec.start >= start && dec.end <= end)) {
                    result = dec;
                    return dec;
                }
            }
        });
        //TODO : conflit could concern several decorator in a range selection of the full section : <p>coucou<b>aaa</b> jj <b>lll</b></p>
        return result;
    }

    module.exports = DecoratorHandler;
}());