(function () {
    "use strict";

    function HTMLNormalize() {
    }


    HTMLNormalize.prototype.normalize = normalize;

    function normalize(root) {

        if (isOnlyInline(root)) {
            root.copyType = "inline";
        }

        //unwrap meaningless wrappers
        let result = unwrap(root);

        result = imgAsSection(result);

        bubbleBRFromSpans(result);
        //no span out of section
        result = wrapInSection(result);

        //flatten xList
        result = flattenLists(result);

        return result;
    }

    function isOnlyInline(node) {
        return !node.children.some(function (node) {
            return !isInline(node);
        });
    }

    function bubbleBRFromSpans(root) {

        root.children.forEach(function (node) {
            bubbleBRFromSpans(node);
        });

        let newChildren = [];
        root.children.forEach(function (node) {
            if (isBreakedInline(node)) {
                Array.prototype.push.apply(newChildren, splitSpan(node));
            } else {
                newChildren.push(node);
            }
        });
        root.children = newChildren;
    }

    function isBreakedInline(node) {
        return isInline(node) && node.children.some(function (child) {
                return child.name === "br";
            });
    }

    function splitSpan(node) {
        let currentSpan = {name: node.name, attr: node.attr, children: [], text: ''};
        let result = [currentSpan];
        node.children.forEach(function (child) {
            if (child.name === 'br') {
                currentSpan = {name: node.name, attr: node.attr, children: [], text: ''};
                result.push(currentSpan);
                result.push(child);
            } else {
                currentSpan.children.push(child);
            }
        });
        return result;
    }

    function wrapInSection(root) {
        let currentSection;
        let newChildren = [];

        function reset() {
            if (currentSection) {
                newChildren.push(currentSection);
            }
            currentSection = {name: 'p', children: [], text: ''};
        }

        root.children.forEach(function (node) {
            if (isBlock(node)) {
                newChildren.push(node);
                wrapInSection(node);
            } else if (isInline(node)) {
                if (!currentSection) {
                    reset();
                }
                currentSection.children.push(node);
            } else if (node.name === 'br') {
                reset();
            } else {
                newChildren.push(node);
                reset();
            }
        });
        if (currentSection) {
            newChildren.push(currentSection);
        }
        root.children = newChildren;
        return root;

    }

    function flattenLists(root) {
        var newChildren = [];
        root.children.forEach(function (node) {
            if (isList(node)) {
                handleXL(node, newChildren, 0);
            } else {
                newChildren.push(node);
            }
        });
        root.children = newChildren;
        return root;
    }

    function isList(node) {
        return node.name === "ol" || node.name === "ul";
    }

    function handleXL(xlNode, parentChildQueue, listLevel) {
        let listType = xlNode.name + "ist";
        xlNode.children.forEach(function (child) {
            handleLI(child, parentChildQueue, listLevel, listType);
        });
    }

    function handleLI(liNode, parentChildQueue, listLevel, listType) {
        let liRdNode = {name: listType, level: listLevel};
        parentChildQueue.push(liRdNode);
        liRdNode.children = [];
        liNode.children.forEach(function (child) {
            if (isList(child)) {
                handleXL(child, parentChildQueue, listLevel + 1);
            } else {
                liRdNode.children.push(child);
            }
        });
    }

    function imgAsSection(root) {

        root.children.forEach(function (node) {
            imgAsSection(node);
        });

        root.children.forEach(function (node) {
            if (node.name === "p" && node.children.length === 1 && node.children[0].name === "img") {
                node.name = 'img';
                node.attr = node.children[0].attr;
                node.children = [];
            }
        });
        return root;
    }

    function unwrap(divElement) {

        divElement.children.forEach(function (node) {
            unwrap(node);
        });

        var newChildren = [];
        divElement.children.forEach(function (node) {
            if (isUnwrappableTag(node)) {
                Array.prototype.push.apply(newChildren, node.children);
            } else {
                newChildren.push(node);
            }
        });
        divElement.children = newChildren;
        return divElement;
    }

    function isUnwrappableTag(node) {
        return ["div", "section", "article"].indexOf(node.name) !== -1;
    }

    function isInline(node) {
        return ["span", "#text", "b", "i", "a", "code"].indexOf(node.name) !== -1;
    }

    function isBlock(node) {
        return ["pre", "blockquote"].indexOf(node.name) !== -1;
    }

    module.exports = new HTMLNormalize();
})();