(function () {
    "use strict";

    var htmlparser = require("htmlparser2");
    var HTMLSaxHandler = require("./HTMLSaxHandler");
    var HTMLNormalize = require("./HTMLNormalize");
    var toRedigeModel = require("./toRedigeModel");

    function HTMLParser2() {
    }

    function parse(str) {
        var result = {};
        var htmlSaxHandler = new HTMLSaxHandler(result);
        var parser = new htmlparser.Parser(htmlSaxHandler, {
            decodeEntities: true,
            recognizeSelfClosing: true
        });

        var matches = /<body[\w\-\s"=]*>.*<\/body>/gi.exec(str);
        if (matches === null) {
            htmlSaxHandler.htmlHasBody(false);
        }

        parser.write(str);
        parser.end();
        return toRedigeModel.convert(HTMLNormalize.normalize(result));
    }

    HTMLParser2.prototype.parse = parse;

    module.exports = new HTMLParser2();
})();