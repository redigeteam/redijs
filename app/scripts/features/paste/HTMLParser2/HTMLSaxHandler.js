(function () {
    "use strict";

    let isInBody = false;
    let isInIngored = false;
    let currentTag;

    function HTMLSaxHandler(root) {
        currentTag = root;
        currentTag.text = '';
        currentTag.children = [];
        this.stack = [root];
    }

    HTMLSaxHandler.prototype.htmlHasBody = function (hasBody) {
        isInBody = !hasBody;
    };

    HTMLSaxHandler.prototype.onopentag = function (nameStr, attributesObj) {
        if (nameStr === "body") {
            isInBody = true;
        } else if (isIngoredTagTree(nameStr) && !isInIngored) {
            isInIngored = nameStr;
        } else if (isInBody && !isInIngored) {
            currentTag = {children: [], name: nameStr, attr: attributesObj, text: ''};
            let parent = this.stack[this.stack.length - 1];
            parent.children.push(currentTag);
            this.stack.push(currentTag);
        }
    };
    HTMLSaxHandler.prototype.ontext = function (text) {
        if (isInBody && !isInIngored) {
            if (currentTag.children[currentTag.children.length - 1] && currentTag.children[currentTag.children.length - 1].name === '#text') {
                currentTag.children[currentTag.children.length - 1].text += normalizeEOL(text);
            } else {
                currentTag.children.push({
                    children: [],
                    name: '#text',
                    attr: null,
                    text: normalizeEOL(text)
                });
            }
        }
    };

    function normalizeEOL(text) {
        return text.replace('\r', '');
    }

    HTMLSaxHandler.prototype.onclosetag = function (nameStr) {
        if (nameStr === "body") {
            isInBody = false;
        }
        if (isInBody && !isInIngored) {
            this.stack.pop();
            currentTag = this.stack[this.stack.length - 1];
        }
        if (isInIngored === nameStr) {
            isInIngored = false;
        }
    };

    HTMLSaxHandler.prototype.onerror = function (error) {
        console.log('onerror ', error);
    };
    HTMLSaxHandler.prototype.onreset = function () {
        console.log('onreset');
    };
    HTMLSaxHandler.prototype.onend = function () {
    };

    function isIngoredTagTree(nodeName) {
        //If this is a custome node (out of standard) ex: <o:p> (added from word Data)
        if (nodeName.indexOf(':') !== -1) {
            return true;
        }
        return ["head", "title", "style", "meta", 'iframe'].indexOf(nodeName) !== -1;
    }

    module.exports = HTMLSaxHandler;
})();