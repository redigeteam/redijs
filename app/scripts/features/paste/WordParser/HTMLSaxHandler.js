(function () {
    "use strict";

    let isInBody = false;
    let isInIngored = false;
    let isInProcessInstruction = false;
    let currentTag;

    const MS_IMG_PROCESSING_START_TAG = '![if !vml]';

    function HTMLSaxHandler(root) {
        currentTag = root;
        currentTag.text = '';
        currentTag.children = [];
        this.stack = [root];
    }

    HTMLSaxHandler.prototype.onopentag = function (nameStr, attributesObj) {
        if (nameStr === "body") {
            isInBody = true;
        } else if (isIngoredTagTree(nameStr) && !isInIngored) {
            isInIngored = nameStr;
        } else if (isInBody && !isInIngored && !isInProcessInstruction) {
            currentTag = {children: [], name: nameStr, attr: attributesObj, text: ''};

            if (nameStr === "b" || nameStr === "strong") {
                currentTag.name = 'span';
                currentTag.class = 'bold';
            } else if (nameStr === "i" || isMSEmphasisStyle(attributesObj) || isMSCitationChar(nameStr, attributesObj)) {
                currentTag.name = 'span';
                currentTag.class = 'italic';
            } else if (nameStr === "a") {
                currentTag.name = 'span';
                currentTag.class = 'link';
                currentTag.href = attributesObj.href;
            }

            let parent = this.stack[this.stack.length - 1];
            parent.children.push(currentTag);
            this.stack.push(currentTag);
        }
    };
    HTMLSaxHandler.prototype.ontext = function (text) {
        if (isInBody && !isInIngored && !isInProcessInstruction) {
            if (currentTag.children[currentTag.children.length - 1] && currentTag.children[currentTag.children.length - 1].name === '#text') {
                currentTag.children[currentTag.children.length - 1].text += normalizeEOL(text);
            } else {
                currentTag.children.push({
                    children: [],
                    name: '#text',
                    attr: null,
                    text: normalizeEOL(text)
                });
            }
        }
    };

    function normalizeEOL(text) {
        return text.replace('\r', '');
    }

    HTMLSaxHandler.prototype.onclosetag = function (nameStr) {
        if (nameStr === "body") {
            isInBody = false;
        }
        if (isInBody && !isInIngored && !isInProcessInstruction) {
            this.stack.pop();
            currentTag = this.stack[this.stack.length - 1];
        }
        if (isInIngored === nameStr) {
            isInIngored = false;
        }
    };
    HTMLSaxHandler.prototype.onprocessinginstruction = function (nameStr, dataStr) {
        if (!isInIngored) {
            if (nameStr.indexOf('![if') !== -1 && dataStr !== MS_IMG_PROCESSING_START_TAG) {
                isInProcessInstruction = true;
            } else if (isInProcessInstruction && nameStr.indexOf('![endif]') !== -1) {
                isInProcessInstruction = false;
            }
        }
    };
    HTMLSaxHandler.prototype.onerror = function (error) {
        console.log('onerror ', error);
    };
    HTMLSaxHandler.prototype.onreset = function () {
        console.log('onreset');
    };
    HTMLSaxHandler.prototype.onend = function () {
    };

    function isMSCitationChar(nodeName, attr) {
        return nodeName === "span" && attr.class && attr.class.indexOf('CitationCar') !== -1;
    }

    function isMSEmphasisStyle(attr) {
        return attr.class && attr.class.indexOf('MsoIntenseEmphasis') !== -1;
    }

    function isIngoredTagTree(nodeName) {
        //If this is a custome node (out of standard) ex: <o:p> (added from word Data)
        if (nodeName.indexOf(':') !== -1) {
            return true;
        }
        return ["head", "title", "style", "meta", "br"].indexOf(nodeName) !== -1;
    }

    module.exports = HTMLSaxHandler;
})();