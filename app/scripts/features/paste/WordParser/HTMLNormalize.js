(function () {
    "use strict";

    const _ = require('lodash');

    function HTMLNormalize() {
    }

    function normalize(root) {
        //unwrap meaningless wrappers
        let result = unwrap(root);

        if (isOnlyInline(root)) {
            result.copyType = "inline";
        }
        result = removeUnexpectedBlankLines(result, true);

        //Word include img in <p><span>
        result = imgAsSection(result);
        //no span out of section
        result = wrapInSection(result);

        result = handleWordList(result);
        return result;
    }

    function isOnlyInline(node) {
        return node.children.length === 3 && node.children[1].name === "span";
    }

    HTMLNormalize.prototype.normalize = normalize;

    function removeUnexpectedBlankLines(root, isDoc) {
        if (root.children) {
            root.children.forEach(function (node) {
                removeUnexpectedBlankLines(node, false);
            });

            if (isBlock(root) || isDoc) {
                root.children = _.filter(root.children, function (node) {
                    return !node.text || _.isEmpty(node.text.match(/^\n\s*$/));
                });
            }
        }
        if (!_.isEmpty(root.text) && !root.text.match(/^\n\s*$/)) {
            root.text = root.text.replace('\n', '').replace(/\s+/g, ' ');
        }
        return root;
    }


    function wrapInSection(root) {
        var newChildren = [];
        root.children.forEach(function (node) {
            if (isInline(node)) {
                newChildren.push({name: 'p', children: [node]});
            } else {
                newChildren.push(node);
            }
        });
        root.children = newChildren;
        return root;
    }

    function handleWordList(root) {
        root.children.forEach(function (node) {

            handleWordList(node);

            if (node.attr && node.attr.style && node.attr.class && node.attr.class.indexOf("MsoListParagraphCxSp") !== -1) {
                if (node.attr.style.indexOf("lfo1") !== -1) {
                    node.name = "olist";
                } else {
                    node.name = "ulist";
                }
                node.level = parseInt(/level(\d)/.exec(node.attr.style)[1]) - 1;
            }
        });
        return root;
    }

    function imgAsSection(root) {

        root.children.forEach(function (node) {
            imgAsSection(node);
        });

        root.children.forEach(function (node) {
            if (node.name === "p" && node.children.length === 1 && node.children[0].name === "span" && node.children[0].children.length === 1 && node.children[0].children[0].name === "img") {
                node.name = 'img';
                node.attr = node.children[0].children[0].attr;
                node.children = [];
            }
        });
        return root;
    }

    function unwrap(divElement) {

        divElement.children.forEach(function (node) {
            unwrap(node);
        });

        var newChildren = [];
        divElement.children.forEach(function (node) {
            if (isUnwrappableTag(node)) {
                Array.prototype.push.apply(newChildren, node.children);
            } else {
                newChildren.push(node);
            }
        });
        divElement.children = newChildren;
        return divElement;
    }

    function isUnwrappableTag(node) {
        return ["div", "section", "article"].indexOf(node.name) !== -1;
    }

    function isInline(node) {
        return ["span", "b", "i", "a", "code"].indexOf(node.name) !== -1;
    }

    function isBlock(node) {
        return ["pre", "blockquote"].indexOf(node.name) !== -1;
    }

    module.exports = new HTMLNormalize();
})();