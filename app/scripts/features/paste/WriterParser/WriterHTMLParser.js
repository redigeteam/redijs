(function () {
    "use strict";

    var htmlparser = require("htmlparser2");
    var HTMLSaxHandler = require("../HTMLParser2/HTMLSaxHandler");
    var HTMLNormalize = require("./HTMLNormalize");
    var toRedigeModel = require("../HTMLParser2/toRedigeModel");

    function HTMLParser2() {
    }

    function parse(str) {
        var result = {};
        var parser = new htmlparser.Parser(new HTMLSaxHandler(result), {
            decodeEntities: true,
            recognizeSelfClosing: true
        });
        parser.write(str);
        parser.end();
        HTMLNormalize.normalize(result);
        return toRedigeModel.convert(result);
    }

    HTMLParser2.prototype.parse = parse;
    HTMLParser2.prototype.couldParse = function (data) {
        return data.indexOf('META NAME="GENERATOR" CONTENT="OpenOffice') !== -1;
    };

    module.exports = new HTMLParser2();
})();