(function () {
    "use strict";

    const _ = require('lodash');

    function HTMLNormalize() {
    }

    function normalize(root) {
        //unwrap meaningless wrappers
        let result = unwrap(root);

        if (isOnlyInline(root)) {
            result.copyType = "inline";
        }

        result = removeUnexpectedBlankLines(result, true);

        //no span out of section
        result = wrapInSection(result);

        //flatten xList
        result = flattenLists(result);

        return result;
    }

    function isOnlyInline(node) {
        return node.children.length === 3 && node.children[1].name === "p";
    }

    HTMLNormalize.prototype.normalize = normalize;

    function removeUnexpectedBlankLines(root, isDoc) {
        if (root.children) {
            root.children.forEach(function (node) {
                removeUnexpectedBlankLines(node, false);
            });

            if (isBlock(root) || isDoc) {
                root.children = _.filter(root.children, function (node) {
                    return !node.text || _.isEmpty(node.text.match(/^\n\s*$/));
                });
            }
        }
        if (!_.isEmpty(root.text) && !root.text.match(/^\n\s*$/)) {
            root.text = root.text.replace('\n', '').replace(/\s+/g, ' ');
        }
        return root;
    }


    function wrapInSection(root) {
        let newChildren = [];
        root.children.forEach(function (node) {
            if (isInline(node)) {
                newChildren.push({name: 'p', children: [node]});
            } else {
                newChildren.push(node);
            }
        });
        root.children = newChildren;
        return root;
    }


    function flattenLists(root) {
        let newChildren = [];
        root.children.forEach(function (node) {
            if (isList(node)) {
                handleXL(node, newChildren, 0);
            } else {
                newChildren.push(node);
            }
        });
        root.children = newChildren;
        return root;
    }

    function isList(node) {
        return node.name === "ol" || node.name === "ul" || (node.type && node.type.indexOf("list") === 1);
    }

    function handleXL(xlNode, parentChildQueue, listLevel) {
        let listType = xlNode.name + "ist";
        xlNode.children.forEach(function (child) {
            if (child.name === "li") {
                handleLI(child, parentChildQueue, listLevel, listType);
            }
        });
    }

    function handleLI(liNode, parentChildQueue, listLevel, listType) {
        let liRdNode = {name: listType, level: listLevel};
        parentChildQueue.push(liRdNode);
        liRdNode.children = [];
        liNode.children.forEach(function (child) {
            if (isList(child)) {
                handleXL(child, parentChildQueue, listLevel + 1);
            } else if (child.name !== '#text') {
                liRdNode.children.push(child);
            }
        });
    }

    function unwrap(divElement) {

        divElement.children.forEach(function (node) {
            unwrap(node);
        });

        let newChildren = [];
        divElement.children.forEach(function (node) {
            if (isUnwrappableTag(node)) {
                Array.prototype.push.apply(newChildren, node.children);
            } else {
                newChildren.push(node);
            }
        });
        divElement.children = newChildren;
        return divElement;
    }

    function isUnwrappableTag(node) {
        return ["div", "section", "article"].indexOf(node.name) !== -1;
    }

    function isInline(node) {
        return ["span", "b", "i", "a", "code"].indexOf(node.name) !== -1;
    }

    function isBlock(node) {
        return ["pre", "blockquote"].indexOf(node.name) !== -1;
    }

    module.exports = new HTMLNormalize();
})();