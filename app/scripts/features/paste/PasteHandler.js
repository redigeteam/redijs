(function () {
    "use strict";

    const _ = require("lodash");
    const HTMLParser2 = require("./HTMLParser2/HTMLParser2");
    const writerHTMLParser = require("./WriterParser/WriterHTMLParser");
    const wordHTMLParser = require("./WordParser/WordHTMLParser");
    const gdocHTMLParser = require("./GdocParser/GdocHTMLParser");

    function PasteHandler() {
    }

    PasteHandler.prototype.$$ = {
        HTMLParser2: HTMLParser2
    };

    PasteHandler.prototype.init = function (editor) {

        editor.addEventListener("paste", function (event) {

            if (!Redige.isInSection(event.srcElement)) {
                return;
            }
            event.preventDefault();

            const selection = Redige.getDocumentSelection();
            const types = getAvailableClipboardDataTypes(event);

            if (types.redige) {
                return getDataFromRedigeType(selection, types);
            } else if (types.file) {
                return getDataFromFile(selection);
            } else if (types.html && selection.startDivision.type !== "blockcode") {
                return getDataFromHTML(selection, types);
            } else if (types.plain) {
                return getDataFromPlain(selection, types);
            } else if (types.image) {
                return getDataFromImage(selection, types);
            }
        });

        return this;
    };

    function getDataFromImage(selection, types) {
        let image = event.clipboardData.getData(types.image);
        if (image) {
            image.getAsFile(function (file) {
                readFile(selection, file);
            });
        }
    }

    function getDataFromPlain(selection, types) {
        let data = event.clipboardData.getData(types.plain);
        let lines = data.split('\n');
        insertPlainLine(selection, lines, data.indexOf('\n') === -1);
    }

    function getDataFromHTML(selection, types) {
        let data = event.clipboardData.getData(types.html);
        let extract = data.substring(0, 300);
        let importedDoc;
        if (writerHTMLParser.couldParse(extract)) {
            importedDoc = writerHTMLParser.parse(data);
        } else if (wordHTMLParser.couldParse(extract)) {
            importedDoc = wordHTMLParser.parse(data);
        } else if (gdocHTMLParser.couldParse(extract)) {
            importedDoc = gdocHTMLParser.parse(data);
        } else {
            importedDoc = HTMLParser2.parse(data);
        }
        insert(selection, importedDoc);
    }

    function getDataFromFile(selection) {
        const file = _.find(event.clipboardData.items, { kind: 'file' }).getAsFile();
        readFile(selection, file);
    }

    function readFile(selection, file) {
        const reader = new window.FileReader();
        reader.readAsDataURL(file);
        reader.onloadend = function () {
            if (file.type.match("^image/")) {
                Redige.patch({
                    action: 'insertImage',
                    afterId: selection.endSection.id,
                    data: reader.result
                });
            } else {
                Redige.patch({
                    action: 'insertAttachment',
                    afterId: selection.endSection.id,
                    data: reader.result
                });
            }
        };
    }

    function getDataFromRedigeType(selection, types) {
        const redigeImport = JSON.parse(event.clipboardData.getData(types.redige));

        if (redigeImport.copyType === 'divisions') {
            insert(selection, redigeImport);
        } else if (redigeImport.copyType === 'inline') {
            Redige.insertInSection(redigeImport.content, selection);
        }
    }

    function getAvailableClipboardDataTypes(event) {
        let availableType = {};
        event.clipboardData.types.forEach(function (type) {
            if (type.match("^text/html")) {
                availableType.html = type;
            }
            if (type.match("^text/plain")) {
                availableType.plain = type;
            }
            if (type.match("^image/")) {
                availableType.image = type;
            }
            if (type === "Files") {
                availableType.file = type;
            }
            if (type === "application/redige") {
                availableType.redige = type;
            }
        });
        return availableType;
    }

    function insert(selection, doc) {
        // check if the paste material is authorized
        // if not, it become an unknown division
        const checkedPaste = isItAuthorizedByTemplate(doc);

        if (selection.isCollapsed) {
            if (checkedPaste.copyType === 'inline') {
                Redige.insertInSection(checkedPaste.content, selection);
            } else if (isOnlySection(checkedPaste) && selection.startDivision.type !== 'default') {
                insertInDiv(checkedPaste.content.children, selection);
            } else if (isOnlyOneDivision(checkedPaste) && selection.startDivision.type !== 'default') {
                insertInDiv(checkedPaste.content.children[0].children, selection);
            } else if (selection.startDivision.type === 'default') {
                insertInDoc(checkedPaste.content.children, selection);
            }
        }
    }

    function isOnlySection(doc) {
        return !doc.content.children.some(function (elm) {
            return elm.children;
        });
    }

    function isOnlyOneDivision(doc) {
        return doc.content.children.filter(function (elm) {
            return elm.children && elm.type !== "default";
        }).length === 1;
    }

    function insertInDiv(sections, selection) {
        let promise = Promise.resolve(selection.startSection);
        sections.forEach(function (elm) {
            if (elm.type === "image") {
                promise = promise.then(function (lastSection) {
                    return Redige.patch({
                        action: 'insertImage',
                        afterId: lastSection.id,
                        data: elm.data
                    });
                });
            } else if (elm.type === "attachment") {
                promise = promise.then(function (lastSection) {
                    return Redige.patch({
                        action: 'insertAttachment',
                        afterId: lastSection.id,
                        data: elm.data,
                        name: elm.name
                    });
                });
            } else {
                promise = promise.then(function (lastSection) {
                    return Redige.patch({
                        action: 'insertSection',
                        afterId: lastSection.id,
                        section: elm
                    });
                });
            }
        });
        promise.then(function (lastSection) {
            Redige.setSelection(lastSection.id, lastSection.content.length);
        });
    }

    function insertInDoc(docElement, selection) {
        let promise = Promise.resolve(selection.startSection);
        docElement.forEach(function (elm) {
            promise = promise.then(function (lastSection) {
                if (elm.children) {
                    return Redige.patch({
                        action: 'insertDivision',
                        afterId: lastSection.id,
                        division: elm
                    });
                } else if (elm.type === "image") {
                    return Redige.patch({
                        action: 'insertImage',
                        afterId: lastSection.id,
                        data: elm.data
                    });
                } else if (elm.type === "attachment") {
                    return Redige.patch({
                        action: 'insertAttachment',
                        afterId: lastSection.id,
                        data: elm.data,
                        name: elm.name
                    });
                } else {
                    return Redige.patch({
                        action: 'insertSection',
                        afterId: lastSection.id,
                        section: elm
                    });
                }
            });
        });
        return promise.then(function (lastSection) {
            if (lastSection instanceof Redige.Division) {
                let lastSectionInNewDiv = lastSection.children[lastSection.children.length - 1];
                Redige.setSelection(lastSectionInNewDiv.id, lastSectionInNewDiv.content.length);
            } else {
                Redige.setSelection(lastSection.id, lastSection.content.length);
            }
        });
    }

    function insertPlainLine(selection, lines, isInlinePaste) {
        if (isInlinePaste) {
            Redige.insertInSection({ content: lines[0], decorators: [], type: 'hierarchical', level: "0" }, selection);
        } else {
            let promise = Promise.resolve();
            let lastSection = selection.startSection;
            lines.forEach(function (line) {
                promise = promise.then(function () {
                    return Redige.patch({
                        action: 'insertSection',
                        afterId: lastSection.id,
                        section: { content: line.trim() }
                    });
                }).then(function (newSection) {
                    lastSection = newSection;
                });
            });
            return promise.then(function () {
                Redige.setSelection(lastSection.id, lastSection.content.length);
            });
        }
    }

    function isItAuthorizedByTemplate(pastedThing) {
        const model = Redige.getDocument().template.model;
        if (pastedThing.content.children !== undefined) {
            pastedThing.content.children.forEach(section => {
                if (model.divisions.indexOf(section.type) === -1 &&
                    model.sections.indexOf(section.type) === -1) {
                    section.type = 'unknown';
                }
            });
        }
        return pastedThing;
    }

    module.exports = new PasteHandler();
})();
