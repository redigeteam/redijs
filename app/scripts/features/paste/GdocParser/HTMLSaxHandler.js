(function () {
    "use strict";

    let isInContent = false;
    let currentTag;

    function HTMLSaxHandler(root) {
        currentTag = root;
        currentTag.text = '';
        currentTag.children = [];
        this.stack = [root];
    }

    HTMLSaxHandler.prototype.onopentag = function (nameStr, attributesObj) {
        if (isGoogleDriveRootNode(nameStr, attributesObj)) {
            isInContent = true;
        } else if (isInContent) {
            currentTag = {children: [], name: nameStr, attr: attributesObj, text: ''};
            if (isGDocBoldSpan(nameStr, attributesObj)) {
                currentTag.name = 'span';
                currentTag.class = 'bold';
            } else if (isGDocItalicSpan(nameStr, attributesObj)) {
                currentTag.name = 'span';
                currentTag.class = 'italic';
            } else if (nameStr === "a") {
                currentTag.name = 'span';
                currentTag.class = 'link';
                currentTag.href = attributesObj.href;
            }
            let parent = this.stack[this.stack.length - 1];
            parent.children.push(currentTag);
            this.stack.push(currentTag);
        }
    };

    HTMLSaxHandler.prototype.ontext = function (text) {
        if (isInContent) {
            currentTag.text += text;
        }
    };
    HTMLSaxHandler.prototype.onclosetag = function (nameStr) {
        if (this.stack.length === 1) {
            isInContent = false;
        }
        if (isInContent) {
            this.stack.pop();
            currentTag = this.stack[this.stack.length - 1];
        }
    };

    function isGDocBoldSpan(nodeName, attr) {
        return nodeName === "span" && attr.style && attr.style.indexOf('font-weight:700') !== -1;
    }

    function isGDocItalicSpan(nodeName, attr) {
        return nodeName === "span" && attr.style && attr.style.indexOf('font-style:italic') !== -1;
    }

    function isGoogleDriveRootNode(name, attr) {
        //WTF google ?!
        return name === "b" && attr.id && attr.id.indexOf("docs-internal-guid") === 0;
    }

    module.exports = HTMLSaxHandler;
})();