(function () {
    "use strict";

    function HTMLNormalize() {
    }

    function normalize(root) {
        //Gdoc include img in <p><span>
        var result = imgAsSection(root);
        result = wrapInSection(result);
        //flatten xList (and unwrap headers (gdoc wrap numbered headers in OL>LI stack :-()
        result = flattenLists(result);
        //gdocs add br before & after images
        result = removeImageBR(result);
        result = brToEmptyP(result);
        return result;
    }

    function wrapInSection(root) {
        var newChildren = [];
        root.children.forEach(function (node) {
            if (isInline(node)) {
                newChildren.push({name: 'p', children: [node]});
            } else {
                newChildren.push(node);
            }
        });
        root.children = newChildren;
        return root;
    }


    function brToEmptyP(root) {
        root.children.forEach(function (node) {
            if (node.name === "br") {
                node.name = "p";
            } else {
                brToEmptyP(node);
            }
        });
        return root;
    }

    function removeImageBR(root) {
        root.children.forEach(function (node) {
            removeImageBR(node);
        });

        var newChildren = [];
        for (var index = 0; index < root.children.length; index++) {
            var node = root.children[index];
            if (!isImageBR(root.children, index)) {
                newChildren.push(node);
            }
        }
        root.children = newChildren;
        return root;
    }

    function isImageBR(nodes, index) {
        if (nodes[index].name === "br") {
            if (index + 1 < nodes.length && nodes[index + 1].name === "img") {
                return true;
            } else if (index - 1 >= 0 && nodes[index - 1].name === "img") {
                return true;
            }
        }
        return false;
    }

    HTMLNormalize.prototype.normalize = normalize;

    function unwrapInlineSpans(root) {
        root.children.forEach(function (node) {
            unwrapInlineSpans(node);
        });
        root.children.forEach(function (node) {
            if (isInline(node) && node.children.length === 1) {
                node.text = node.children[0].text;
            }
        });
        return root;
    }

    function flattenLists(root) {
        var newChildren = [];
        root.children.forEach(function (node) {
            if (isList(node)) {
                if (newChildren.length > 0 && isList(newChildren[newChildren.length - 1], node)) {
                    newChildren.push({name: "p", text: "", children: []});
                }
                var flattenContent = [];
                flattenListContent(node, flattenContent, 0);
                Array.prototype.push.apply(newChildren, flattenContent);
            } else {
                newChildren.push(node);
                flattenLists(node);
            }
        });
        root.children = newChildren;
        return root;
    }

    function isList(node) {
        return node.name === "ol" || node.name === "ul" || (node.type && node.type.indexOf("list") === 1);
    }

    function flattenListContent(node, resultList, level) {
        node.children.forEach(function (child) {
            if (isList(child)) {
                flattenListContent(child, resultList, level + 1);
            } else if (child.name === "li") {
                child.children.forEach(function (subchild) {
                    if (isList(subchild)) {
                        flattenListContent(subchild, resultList, level + 1);
                    } else {
                        if (!isHeader(subchild)) {
                            subchild.name = node.name + "ist";
                            subchild.level = level;
                        }
                        resultList.push(subchild);
                    }
                });
            }
        });
    }

    function imgAsSection(root) {

        root.children.forEach(function (node) {
            imgAsSection(node);
        });

        root.children.forEach(function (node) {
            if (node.name === "p" && node.children.length === 1 && node.children[0].name === "span" && node.children[0].children.length === 1 && node.children[0].children[0].name === "img") {
                node.name = 'img';
                node.attr = node.children[0].children[0].attr;
                node.children = [];
            }
        });
        return root;
    }

    function isHeader(node) {
        if (!node) {
            return false;
        }
        return ["h1", "h2", "h3", "h4", "h5", "h6"].indexOf(node.name) !== -1;
    }

    function isInline(node) {
        return ["span", "b", "i", "a", "code"].indexOf(node.name) !== -1;
    }


    module.exports = new HTMLNormalize();
})();