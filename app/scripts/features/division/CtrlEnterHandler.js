(function () {
    "use strict";

    const _ = require('lodash');

    function CtrlEnterHandler() {
    }

    CtrlEnterHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("keydown", function (event) {
            if (Redige.isInTable()) {
                return true;
            }
            return onKeyDown(event);
        });
    };

    function isCtrlEnter(event) {
        return (event.ctrlKey || (event.metaKey && Redige.isMacOs())) && event.keyCode === CtrlEnterHandler.KEY_ENTER;
    }

    function onCtrlEnterPress(event) {
        event.preventDefault();
        const selection = Redige.getDocumentSelection();
        const division = selection.startSection.parent;
        if (division.type !== "default") {
            if (isTheLastPositionInTheLastSection(selection)) {
                const nextDiv = getNextDocumentEntry(division);
                if (!nextDiv || nextDiv.type !== "default") {
                    appendBlankLineAfter(division);
                }
            } else if (isTheLastPositionInASection(selection) || isTheFirstPositionInANotFirstSection(selection)) {
                splitDivisionAndInsertBlankLineBetween(division, selection);
            }
        }
        return false;
    }

    function getNextDocumentEntry(division) {
        const documentChildren = division.parent.children;
        if (documentChildren.indexOf(division) === documentChildren.length) {
            return null;
        } else {
            return documentChildren[documentChildren.indexOf(division) + 1];
        }
    }

    function isTheLastPositionInASection(selection) {
        return selection.endOffset === selection.startSection.content.length;
    }

    function isTheFirstPositionInANotFirstSection(selection) {
        return selection.startOffset === 0 && selection.startDivision.children.indexOf(selection.startSection) > 0;
    }

    function isTheLastPositionInTheLastSection(selection) {
        return _.last(selection.startDivision.children) === selection.startSection && selection.endOffset === selection.startSection.content.length;
    }

    function splitDivisionAndInsertBlankLineBetween(division, selection) {
        return appendBlankLineAfter(division).then(function (newBlankLine) {
            return appendTypedDivisionAfter(division, newBlankLine.id, selection);
        });
    }

    function appendBlankLineAfter(div) {
        return Redige.patch({action: "insertSection", afterId: div.id, section: {}}).then(function (section) {
            Redige.setSelection(section.id, 0);
            return section;
        });
    }

    function appendTypedDivisionAfter(currentDiv, afterId, selection) {
        const start = selection.startOffset ? 1 : 0;
        const newDiv = {
            type: currentDiv.type,
            children: currentDiv.children.slice(selection.startDivision.children.indexOf(selection.startSection) + start)
        };
        return Redige.patch({
            action: "deleteSection",
            sectionsIds: newDiv.children.map(function (e) {
                return e.id;
            })
        }).then(function () {
            return Redige.patch({action: "insertDivision", afterId: afterId, division: newDiv});
        });
    }

    function onKeyDown(event) {
        if (isCtrlEnter(event) && window.getSelection().isCollapsed) {
            return onCtrlEnterPress(event);
        }
    }

    CtrlEnterHandler.KEY_ENTER = 13;
    CtrlEnterHandler.KEY_CODE_CTR_ENTER = 10;

    module.exports = new CtrlEnterHandler();

})();