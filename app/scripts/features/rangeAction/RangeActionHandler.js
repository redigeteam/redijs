(function () {
    //For delete/Input on Range
    "use strict";

    let editor;

    function RangeActionHandler() {
    }

    RangeActionHandler.prototype.init = function (rootNode) {
        editor = rootNode;
        rootNode.addEventListener("keydown", function (event) {
            if (isPrintableKey(event)) {
                return onKeyDown(event);
            }
        });
    };

    RangeActionHandler.prototype.removeCurrentSelection = onKeyDown;

    function onKeyDown(event) {
        const selection = Redige.getDocumentSelection();
        if (selection.startSection !== selection.endSection) {
            const parentElement = selection.startSection.parent;
            removeSelection(selection);
            Redige.dispatchEventByName(parentElement, "structureChanged");

            if (Redige.getDocument().children.length === 0) {
                Redige.patch({action: "insertSection", section: {}}).then(function (newSection) {
                    Redige.setSelection(newSection.id, 0);
                });
            } else {
                setSelection(selection);
            }
            if (isDelete(event) || isSuppr(event)) {
                event.preventDefault();
                return true;
            }
        }
        return true;
    }

    function removeSelection(selection) {
        removeMiddleContent(selection);
        removeStartingDivContent(selection);
        removeEndingDivContent(selection);
    }

    function removeMiddleContent(selection) {
        const startSection = selection.startSection;
        const endSection = selection.endSection;
        const docChildren = getDocumentChildren(startSection);

        const start = getDocumentChildrenIndex(startSection, docChildren) + 1;
        const end = getDocumentChildrenIndex(endSection, docChildren);
        for (let index = start; index < end; index++) {
            deleteSection(docChildren[index]);
        }
    }

    function getDocumentChildrenIndex(section, docChildren) {
        if (section.parent instanceof Redige.Division) {
            return docChildren.indexOf(section.parent);
        } else {
            return docChildren.indexOf(section);
        }
    }

    function getDocumentChildren(section) {
        if (section.parent instanceof Redige.Division) {
            return section.parent.parent.children;
        } else {
            return section.parent.children;
        }
    }

    function removeStartingDivContent(selection) {
        const startSection = selection.startSection;
        const endSection = selection.endSection;
        const startDivision = selection.startDivision;
        const endDivision = selection.endDivision;
        const startDivisionChildren = startDivision.children;

        if (startDivision instanceof Redige.Division) {
            let end;
            if (endDivision === startDivision) {
                end = startDivisionChildren.indexOf(endSection);
            } else {
                end = startDivisionChildren.length;
            }
            for (let index = startDivisionChildren.indexOf(startSection) + 1; index < end; index++) {
                deleteSection(startDivisionChildren[index]);
            }
        }

        removeBeginingOfStartSection(selection);
    }

    function removeEndingDivContent(selection) {
        const endSection = selection.endSection;
        const endDivision = selection.endDivision;
        const endDivisionChildren = endDivision.children;

        if (endDivision instanceof Redige.Division) {
            let start;
            if (endDivision === selection.startDivision) {
                start = endDivisionChildren.indexOf(endSection) + 1;
            } else {
                start = 0;
            }
            for (let index = start; index < endDivisionChildren.indexOf(endSection); index++) {
                deleteSection(endDivisionChildren[index]);
            }
        }

        removeEndingOfEndSection(selection);
    }

    function removeBeginingOfStartSection(selection) {
        const startSection = selection.startSection;
        if (startSection instanceof Redige.Section && selection.startOffset > 0) {
            Redige.patch({
                action: "updateSection",
                section: {id: startSection.id, content: startSection.content.substring(0, selection.startOffset)}
            });
            cleanUpDecoratorOnStartSection(startSection, selection.startOffset);
            if (startSection.content.length === 0) {
                resetTypeAndLevel(startSection);
            }
        } else {
            deleteSection(startSection);
        }
    }

    function removeEndingOfEndSection(selection) {
        const endSection = selection.endSection;
        const endSectionOldLenght = endSection.content.length;
        if (endSection instanceof Redige.Section) {
            Redige.patch({
                action: "updateSection",
                section: {id: endSection.id, content: endSection.content.substring(selection.endOffset)}
            });
            cleanUpDecoratorOnEndSection(endSection, selection.endOffset);
        } else {
            deleteSection(endSection);
        }

        if (endSection instanceof Redige.Section && selection.endOffset === endSectionOldLenght) {
            deleteSection(endSection);
        }

    }

    function setSelection(selection) {
        if (Redige.getById(selection.startSection.id)) {
            Redige.setSelection(selection.startSection.id, selection.startOffset);
        } else if (Redige.getById(selection.endSection.id)) {
            Redige.setSelection(selection.endSection.id, 0);
        }
    }

    function resetTypeAndLevel(section) {
        const patch = {id: section.id};
        if (section.level !== '0') {
            patch.level = '0';
        }
        if (section.type !== 'hierarchical') {
            patch.type = 'hierarchical';
        }
        Redige.patch({
            action: "updateSection",
            section: patch
        });
    }

    function cleanUpDecoratorOnStartSection(section, offset) {
        section.decorators.forEach(function (decorator) {
            if (decorator.start < offset) {
                if (decorator.end > offset) {
                    Redige.patch({
                        action: "updateDecorator",
                        sectionId: section.id,
                        decorator: {end: offset, id: decorator.id}
                    });
                }
            } else {
                removeDecorator(section, decorator);
            }
        });
    }

    function cleanUpDecoratorOnEndSection(section, offset) {
        section.decorators.forEach(function (decorator) {
            if (decorator.end > offset || decorator.start < offset) {
                let patch = {id: decorator.id};
                if (decorator.start < offset) {
                    patch.start = 0;
                } else {
                    patch.start = decorator.start - offset;
                }
                patch.end = decorator.end - offset;
                Redige.patch({
                    action: "updateDecorator",
                    sectionId: section.id,
                    decorator: patch
                });
            } else {
                removeDecorator(section, decorator);
            }
        });
    }

    function removeDecorator(section, decorator) {
        return Redige.patch({
            action: "removeDecorator",
            sectionId: section.id,
            decoratorId: decorator.id
        });
    }

    function deleteSection(section) {
        Redige.patch({
            action: "deleteSection",
            sectionsIds: [section.id]
        });
    }

    function isPrintableKey(event) {
        if (event.ctrlKey || event.altKey || event.metaKey) {
            return false;
        }
        const keycode = event.keyCode;
        return keycode === RangeActionHandler.KEY_SUPPR || keycode === RangeActionHandler.KEY_DELETE ||
            (keycode > 47 && keycode < 58) || // number keys
            keycode === 32 || keycode === 13 || // spacebar & return key(s) (if you want to allow carriage returns)
            (keycode > 64 && keycode < 91) || // letter keys
            (keycode > 95 && keycode < 112) || // numpad keys
            (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
            (keycode > 218 && keycode < 223);   // [\]' (in order)
    }

    function isDelete(event) {
        return event.which === RangeActionHandler.KEY_DELETE;
    }

    function isSuppr(event) {
        return event.which === RangeActionHandler.KEY_SUPPR;
    }

    RangeActionHandler.KEY_SUPPR = 46;
    RangeActionHandler.KEY_DELETE = 8;
    module.exports = new RangeActionHandler();
})
();