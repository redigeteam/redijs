(function () {
    "use strict";
    var copyTextHandler = require('./CopyTextHandler');
    var copyRedigeHandler = require('./CopyRedigeHandler');
    var copyHTMLHandler = require('./CopyHTMLHandler');

    function CopyHandler() {
    }

    CopyHandler.prototype.init = function (editor) {
        editor.addEventListener("copy", event => {
            this.copyToClipboard(event);
        });
    };

    CopyHandler.prototype.copyToClipboard = function (event) {
        let redigeSelectionClone = copyRedigeHandler.getRedigeClone();
        event.clipboardData.setData('application/redige', JSON.stringify(redigeSelectionClone));
        event.clipboardData.setData('text/plain', copyTextHandler.getData(redigeSelectionClone));
        event.clipboardData.setData('text/html', copyHTMLHandler.getData(redigeSelectionClone));
        event.preventDefault();
    };

    module.exports = new CopyHandler();
})();
