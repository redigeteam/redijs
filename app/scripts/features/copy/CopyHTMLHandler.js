(function () {
    "use strict";

    const HTMLExporter = require('redige-html-exporter');

    function CopyHTMLHandler() {
    }

    CopyHTMLHandler.getData = function (docClone) {
        let result = "";
        if (docClone.copyType === "inline") {
            result = HTMLExporter.exportSection(docClone.content);
        } else if (docClone.copyType === 'divisions') {
            result += HTMLExporter.exportDocument(docClone.content);
        }
        return result;
    };

    module.exports = CopyHTMLHandler;
})();
