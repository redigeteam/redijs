(function () {
    "use strict";

    function CopyTextHandler() {
    }

    CopyTextHandler.getData = function (docClone) {
        let result = "";
        if (docClone.copyType === "inline") {
            result = docClone.content.content;
        } else if (docClone.copyType === 'divisions') {
            result = docClone.content.children.map(function (child) {
                if (child.children) {
                    return handleDivision(child);
                } else {
                    return child.content;
                }
            }).join('\n');
        }
        return result;
    };

    function handleDivision(div) {
        let result = '\n';
        result += div.children.map(function (section) {
            if (section.content) {
                return section.content;
            } else {
                return "";
            }
        }).join("\n");
        return result + '\n';
    }

    module.exports = CopyTextHandler;
})();
