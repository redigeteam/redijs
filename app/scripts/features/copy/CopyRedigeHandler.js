(function () {
    "use strict";

    function CopyRedigeHandler() {
    }

    CopyRedigeHandler.getData = function () {
        return;
    };

    CopyRedigeHandler.getRedigeClone = function () {
        let selection = Redige.getDocumentSelection();
        let startSection = selection.startSection;
        let endSection = selection.endSection;
        let result = {};
        //In line copy
        if (startSection === endSection) {
            result.content = cloneInlineSelection(startSection, selection.startOffset, selection.endOffset);
            result.copyType = "inline";
        } else {
            result.content = cloneMultiDivSelection(selection);
            result.copyType = "divisions";
        }
        overrideIds(result.content);
        return result;
    };

    function overrideIds(node) {
        node.id = Redige.getGUID();
        if (node.children) {
            node.children.forEach(function (elm) {
                overrideIds(elm);
            });
        }
        if (node.decorators) {
            node.decorators.forEach(function (elm) {
                overrideIds(elm);
            });
        }
    }

    function cloneInlineSelection(startSection, startOffset, endOffset) {
        let result = startSection.parse();
        if (startSection.content) {
            if (!startOffset) {
                startOffset = 0;
            }
            if (!endOffset && startSection.content) {
                endOffset = startSection.content.length;
            }
            result.content = startSection.content.substring(startOffset, endOffset);
            if (startSection.decorators) {
                result.decorators = startSection.decorators.filter(function (decorator) {
                    return !(decorator.end <= startOffset || decorator.start >= endOffset) && decorator.type.indexOf('code') === -1;
                }).map(function (decorator) {
                    let newDec = {id: Redige.getGUID(), type: decorator.type, data: decorator.data};
                    if (decorator.start <= startOffset) {
                        newDec.start = 0;
                    } else {
                        newDec.start = decorator.start - startOffset;
                    }
                    if (decorator.end > endOffset) {
                        newDec.end = result.content.length;
                    } else {
                        newDec.end = decorator.end - startOffset;
                    }
                    return newDec;
                });
            }
        }
        return result;
    }

    function cloneMultiDivSelection(selection) {
        let result = {children: []};
        result.children = result.children.concat(copyStartingDivContent(selection));
        result.children = result.children.concat(copyMiddleContent(selection));
        result.children = result.children.concat(copyEndingDivContent(selection));
        return result;
    }

    function copyStartingDivContent(selection) {
        const startSection = selection.startSection;
        const endSection = selection.endSection;
        const startDivision = selection.startDivision;
        const endDivision = selection.endDivision;
        const startDivisionChildren = startDivision.children;

        let result = [];
        if (startSection.content && selection.startOffset < startSection.content.length) {
            result.push(cloneInlineSelection(startSection, selection.startOffset));
        }
        if (startDivision instanceof Redige.Division) {
            let end;
            if (endDivision === startDivision) {
                end = startDivisionChildren.indexOf(endSection);
            } else {
                end = startDivisionChildren.length;
            }
            for (let index = startDivisionChildren.indexOf(startSection) + 1; index < end; index++) {
                if (startDivisionChildren[index] instanceof Redige.Section) {
                    result.push(startDivisionChildren[index].parse());
                }
            }
            if (endDivision !== startDivision) {
                let newDiv = startDivision.parse();
                newDiv.children = result;
                return [newDiv];
            } else {
                return result;
            }

        } else {
            return result;
        }
    }

    function copyEndingDivContent(selection) {
        const endSection = selection.endSection;
        const endDivision = selection.endDivision;
        const endDivisionChildren = endDivision.children;
        let result = [];
        if (endDivision instanceof Redige.Division) {
            let start;
            if (endDivision === selection.startDivision) {
                start = endDivisionChildren.indexOf(endSection) + 1;
            } else {
                start = 0;
            }
            for (let index = start; index < endDivisionChildren.indexOf(endSection); index++) {
                if (endDivisionChildren[index] instanceof Redige.Section) {
                    result.push(endDivisionChildren[index].parse());
                }
            }
            result.push(cloneInlineSelection(endSection, 0, selection.endOffset));

            if (endDivision !== selection.startDivision) {
                let newDiv = endDivision.parse();
                newDiv.children = result;
                return [newDiv];
            } else {
                return result;
            }

        } else {
            if (selection.endOffset !== 0) {
                result.push(cloneInlineSelection(endSection, 0, selection.endOffset));
            }
            return result;
        }
    }

    function copyMiddleContent(selection) {
        const startSection = selection.startSection;
        const endSection = selection.endSection;
        const docChildren = getDocumentChildren(startSection);
        const start = getDocumentChildrenIndex(startSection, docChildren) + 1;
        const end = getDocumentChildrenIndex(endSection, docChildren);
        let result = [];
        for (let index = start; index < end; index++) {
            result.push(docChildren[index].parse());
        }
        return result;
    }

    function getDocumentChildrenIndex(section, docChildren) {
        if (section.parent instanceof Redige.Division) {
            return docChildren.indexOf(section.parent);
        } else {
            return docChildren.indexOf(section);
        }
    }

    function getDocumentChildren(section) {
        if (section.parent instanceof Redige.Division) {
            return section.parent.parent.children;
        } else {
            return section.parent.children;
        }
    }

    module.exports = CopyRedigeHandler;
})();
