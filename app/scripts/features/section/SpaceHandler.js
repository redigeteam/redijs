(function () {
    "use strict";

    function SpaceHandler() {
    }

    SpaceHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("keydown", function (event) {
            if (!Redige.isInSection()) {
                return true;
            }
            return onKeyDown(event);
        });
    };

    function isSpace(event) {
        return event.which === SpaceHandler.KEY_SPACE;
    }

    function isDoubleSpaceCase(section, offset) {
        return offset - 1 > 0 && section.content.substring(offset - 1, offset) === ' ';
    }

    function isActiveDecorator(section, offset) {
        var found = section.decorators.find(function (decorator) {
            return decorator.end === offset;
        });
        return found;
    }

    function stopDecorators(element, offset) {
        element.decorators.forEach(function (decorator) {
            if (decorator.end === offset) {
                decorator.end--;
            }
        });
        element.refreshContent();
        Redige.setSelection(element.id, offset);
    }

    function onKeyDown(event) {
        var selection = Redige.getDocumentSelection();
        if (selection && selection.isCollapsed && isSpace(event)) {
            var sectionElement = selection.startSection;
            var sectionOffset = selection.startOffset;
            if (isDoubleSpaceCase(sectionElement, sectionOffset) && isActiveDecorator(sectionElement, sectionOffset)) {
                stopDecorators(sectionElement, sectionOffset);
                event.preventDefault();
            }
        }
        return true;
    }

    SpaceHandler.KEY_SPACE = 32;

    module.exports = new SpaceHandler();
})();