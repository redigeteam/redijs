(function () {
    "use strict";

    function EnterHandler() {
    }

    EnterHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("keypress", function (event) {
            if (!Redige.isInSection()) {
                return true;
            }
            return onKeyPress(event);
        });
    };

    function isEnter(event) {
        return event.which === EnterHandler.KEY_ENTER && !event.altKey && !event.ctrlKey && !event.metaKey;
    }

    function isAltEnter(event) {
        return event.which === EnterHandler.KEY_ENTER && event.altKey && !event.ctrlKey && !event.metaKey;
    }

    function isListBreakUseCase(selection) {
        return (selection.startSection.type === "olist" || selection.startSection.type === "ulist")
            && (selection.startSection.content.trim().length === 0);
    }

    function isTitleBreakUseCase(selection) {
        return (selection.startSection.level !== "0")
            && (!selection.startSection.content || selection.startSection.content.length === 0);
    }

    function onEnterPress(event) {
        var selection = Redige.getDocumentSelection();
        if (selection.isCollapsed && selection.startSection.content !== undefined) {
            event.preventDefault();
            var section = selection.startSection;
            var offset = selection.startOffset;
            var newSection;
            if (isTitleBreakUseCase(selection) || isListBreakUseCase(selection)) {
                newSection = selection.startSection;
                newSection.type = "hierarchical";
                newSection.level = "0";
            }
            else {
                //Create the new section copying the first
                newSection = split(section, offset);
                //Add the new created section as a following section of the selected one
                Redige.insertAfter(newSection, section);
            }
            //Move the caret to the beginning of the new section
            Redige.setSelection(newSection.id, 0);
            Redige.dispatchEventByName(section, "structureChanged");
        }
        return false;
    }

    function onAltEnterPress(event) {
        var selection = Redige.getDocumentSelection();
        if (selection.isCollapsed) {
            event.preventDefault();
            var row = getElementFromCell(selection.startSection, "TR");
            var table = getElementFromCell(selection.startSection, "RD-TABLE");
            var insertedRow = table.insertRow(row.rowIndex + 1);
            Redige.setSelection(insertedRow.cells[0].firstChild.id, 0);
        }
        return false;
    }

    function split(section, insertOffset) {
        var newSection = new Redige.Section();
        newSection.type = section.type;
        newSection.level = section.level;

        if (insertOffset === section.content.length || section.content.length === 0) {
            newSection.content = "";
            return newSection;
        }

        var content = section.content;
        //split the content
        section.content = content.substr(0, insertOffset);
        newSection.content = content.substr(insertOffset);
        //process decorator (split each one in two)
        var newDecoratorList = [];
        var removedDecorator = [];
        section.decorators.forEach(function (decorator) {
            if (decorator.start < insertOffset) {
                if (decorator.end > insertOffset) {
                    var copy = new Redige.Decorator(decorator);
                    copy.end = copy.end - insertOffset;
                    copy.start = 0;
                    removedDecorator.push(copy);
                    decorator.end = insertOffset;
                }
                newDecoratorList.push(decorator);
            }
            else {
                decorator.start = decorator.start - insertOffset;
                decorator.end = decorator.end - insertOffset;
                removedDecorator.push(decorator);
            }
        });
        section.decorators = newDecoratorList;
        section.refreshContent();
        newSection.decorators = removedDecorator;
        newSection.refreshContent();
        return newSection;
    }

    function getElementFromCell(cell, elementTag) {
        var result;
        while (!result && cell) {
            if (!Redige.isInTable(cell)) {
                return;
            }
            if (cell.nodeName === elementTag) {
                result = cell;
            } else {
                cell = cell.parentElement;
            }
        }
        return result;
    }

    function onKeyPress(event) {
        if (isEnter(event) && Redige.isInSection()) {
            return onEnterPress(event);
        } else if (isAltEnter(event) && Redige.isInTable()) {
            return onAltEnterPress(event);
        }
    }

    EnterHandler.KEY_ENTER = 13;
    EnterHandler.KEY_CODE_CTR_ENTER = 10;
    module.exports = new EnterHandler();
})();