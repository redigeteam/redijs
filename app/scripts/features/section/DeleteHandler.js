(function () {
    "use strict";

    function DeleteHandler() {
    }

    DeleteHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("keydown", function (event) {
            if (!Redige.isInSection() || Redige.isImageInput()) {
                return true;
            }
            return onKeyDown(event);
        });
    };

    function isDelete(event) {
        return event.which === DeleteHandler.KEY_DELETE;
    }

    /*
     * (this method applyed if(sectionOffset === 0 && sectionLength > 1))
     * 1 - if there is a previous section that could be merged
     *       <p><span>toto</span></p>
     *       <p><span>|content</span></p>
     *    => <p><span>totocontent</span></p>
     */
    function deleteIndexIs0InSection(section) {
        //merge with previous section if any
        if (section.previousSibling !== null && section.previousSibling.tagName === "RD-SECTION") {
            mergeSection(section);
        }
    }

    function deleteIndexIs0InSpan(section, sectionOffset) {
        //avoid unfair html tag added (by html contentEditable)
        /*
         If you remove span content then type some chars, the browser will add
         structured data matching with the removed span.

         Ex : if you remove a span that have a class having font-weigth='bold', removing the content, then typing
         will insert a <b> tag...
         */
        setTimeout(function () {
            section.refreshContent();
            Redige.setSelection(section.id, sectionOffset - 1);
        });
    }

    function mergeSection(section) {
        const previousSectionNode = section.previousSibling;
        const futurCarsetPosition = previousSectionNode.content.length;
        mergeDecorator(previousSectionNode, section);
        previousSectionNode.content += section.content;
        previousSectionNode.refreshContent();
        section.parentElement.removeChild(section);
        Redige.setSelection(previousSectionNode.id, futurCarsetPosition);
    }

    function mergeDecorator(previousSection, currentSection) {
        const previousLength = previousSection.content.length;
        currentSection.decorators.forEach(function (decorator) {
            decorator.start += previousLength;
            decorator.end += previousLength;
            previousSection.decorators.push(decorator);
        });
    }

    function onDelDown(event) {
        const selection = Redige.getDocumentSelection();
        if (selection && selection.isCollapsed) {
            const section = selection.startSection;
            const parentNode = section.parent;
            const sectionOffset = selection.startOffset;
            let preventDefault = false;

            //isIndex0InSection
            if (sectionOffset === 0) {
                deleteIndexIs0InSection(section);
                preventDefault = true;
            }
            else
            //isIndex0InSpan
            if (window.getSelection().anchorOffset - 1 === 0 && window.getSelection().anchorNode.textContent.length === 1) {
                deleteIndexIs0InSpan(section, sectionOffset);
            }
            Redige.dispatchEventByName(parentNode, "structureChanged");
            if (preventDefault) {
                event.preventDefault();
                return false;
            }
        }
        return true;
    }

    function onKeyDown(event) {
        if (isDelete(event)) {
            return onDelDown(event);
        }
        return true;
    }

    DeleteHandler.KEY_DELETE = 8;
    module.exports = new DeleteHandler();
})();