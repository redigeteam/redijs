(function () {
    "use strict";

    function InputHandler() {
    }

    InputHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("input", function (event) {
            if (!Redige.isInSection()) {
                return true;
            }
            return onInput(event);
        });
    };

    function onInput(event) {
        //prevent Default after suppr : could be out of a span.
        if (window.getSelection().anchorOffset === 0 && window.getSelection().anchorNode.nodeName !== "#text") {
            event.preventDefault();
            return false;
        }
        else {
            handleEvent();
        }
    }

    function handleEvent() {
        var selection = Redige.getDocumentSelection();
        var section = selection.startSection;
        var decorators = section.decorators;
        var opennedDecorator = [];
        var allDecorator = [];

        var currentPosition = 0;
        if (!section instanceof Redige.Section) {
            return;
        }
        Redige.toArray(section.childNodes).forEach(function (node) {
            if (node.nodeName === "SPAN") {
                var activeDecoratorList = getActiveDecorator(node, decorators);
                // Openning Décorators
                activeDecoratorList.forEach(function (decorator) {
                    if (opennedDecorator.indexOf(decorator) === -1) {
                        decorator.start = currentPosition;
                        opennedDecorator.push(decorator);
                        allDecorator.push(decorator);
                    }
                });
                // Closing Décorators
                var opennedDecoratorCopy = [].concat(opennedDecorator);
                opennedDecoratorCopy.forEach(function (decorator) {
                    if (activeDecoratorList.indexOf(decorator) === -1) {
                        decorator.end = currentPosition;
                        opennedDecorator.splice(opennedDecorator.indexOf(decorator), 1);
                    }
                });
            }
            currentPosition += node.textContent.length;
        });
        //closed at the end
        opennedDecorator.forEach(function (decorator) {
            decorator.end = currentPosition;
        });
        // disappeared Décorators
        var sectionDecorators = [].concat(decorators);
        sectionDecorators.forEach(function (decorator) {
            if (allDecorator.indexOf(decorator) === -1) {
                decorators.splice(decorators.indexOf(decorator), 1);
            }
        });
    }

    function getDecoratorById(decorators, decoratorId) {
        var result = null;
        decorators.some(function (decorator) {
            if (decorator.id === decoratorId) {
                result = decorator;
                return true;
            }
            else {
                return false;
            }
        });
        return result;
    }

    function getActiveDecorator(node, decorators) {
        var result = [];
        var decoratorIdList = node.getAttribute("decoratoridlist");
        if (decoratorIdList !== null && decoratorIdList.length > 0) {
            decoratorIdList.split(",").forEach(function (str) {
                if (str.length > 0) {
                    var decorator = getDecoratorById(decorators, str);
                    if (decorator) {
                        result.push(decorator);
                    }
                }
            });
        }
        return result;
    }

    module.exports = new InputHandler();
})();