(function () {
    "use strict";

    function TabHandler() {
    }

    TabHandler.prototype.init = function (rootElement) {
        rootElement.addEventListener("keydown", function (event) {
            if (!Redige.isInSection()) {
                return true;
            }
            return onKeyDown(event);
        });
    };

    function handleEvent(event) {
        event.preventDefault();
        var selection = Redige.getDocumentSelection();
        if (selection && (selection.isCollapsed || selection.startSection === selection.endSection) && selection.startSection.parentElement.type !== 'blockcode' && (!Redige.isInTable() || isListInTable(selection.startSection))) {
            changeLevel(!!event.shiftKey, selection.startSection);
        } else if (selection.startSection.parentElement.type === 'blockcode') {
            changeIndent(!!event.shiftKey, selection);
        }
    }

    TabHandler.prototype.changeLevel = changeLevel;

    function changeLevel(increment, section) {
        var level = parseInt(section.level);
        if (increment) {
            level = decrementLevel(level);
        }
        else {
            level = incrementLevel(level);
        }
        section.level = level.toString();
        Redige.dispatchEventByName(section, "structureChanged");
    }

    TabHandler.prototype.changeIndent = changeIndent;

    function changeIndent(isUnindent, selection) {
        if (isUnindent) {
            unindent(selection);
        } else {
            indent(selection);
        }
    }

    function indent(selection) {
        if (selection.startSection === selection.endSection) {
            insertIndent(selection.startSection, selection.startOffset);
            changeDecoratorIndex(selection.startSection, selection.startOffset, 1);
            selection.startSection.refreshContent();
            selection.startOffset++;
            Redige.setSelection(selection.startSection.id, selection.startOffset);

        } else {
            // TODO Insérer la tabulation dans les sections entre et startSection et endSection.
        }
    }

    function unindent(selection) {
        if (isSectionContainsIndent(selection.startSection, selection.startOffset)) {
            if (selection.startSection === selection.endSection) {
                removeIndent(selection.startSection, selection.startOffset);
                changeDecoratorIndex(selection.startSection, selection.startOffset, -1);
                selection.startSection.refreshContent();
                selection.startOffset--;
                Redige.setSelection(selection.startSection.id, selection.startOffset);
            } else {
                // TODO Insérer la tabulation dans les sections entre et startSection et endSection.
            }
        }
    }

    function isSectionContainsIndent(section, index) {
        return (section.content.charAt(index - 1) === '\u0009');
    }

    function removeIndent(section, index) {
        section.content = section.content.substring(0, index - 1) + '' + section.content.substring(index, section.content.length);
    }

    function insertIndent(section, index) {
        section.content = [section.content.slice(0, index), '\u0009', section.content.slice(index)].join('');
    }

    function changeDecoratorIndex(section, startOffset, position) {
        for (var i = 0; i < section.decorators.length; i++) {
            if (startOffset <= section.decorators[i].start) {
                section.decorators[i].start += position;
                section.decorators[i].end += position;
            } else if (section.decorators[i].start < startOffset && startOffset <= section.decorators[i].end) {
                section.decorators[i].end += position;
            }
        }
    }

    function decrementLevel(level) {
        if (level === 0) {
            level = 6;
        }
        else {
            level--;
        }
        return level;
    }

    function incrementLevel(level) {
        if (level === 6) {
            level = 0;
        }
        else {
            level++;
        }
        return level;
    }

    function isTab(event) {
        return event.which === TabHandler.KEY_TAB;
    }

    function isListInTable(section) {
        return Redige.isInTable() && (section.type.indexOf("list") > 0);
    }

    function onKeyDown(event) {
        if (isTab(event)) {
            handleEvent(event);
            return false;
        }
        return true;
    }

    TabHandler.KEY_TAB = 9;

    module.exports = new TabHandler();
})();