const _ = require("lodash");

module.exports = function (Redige) {
    Redige.insertInSection = function (inlineToAdd, selection) {
        let offset = selection.endOffset;

        if (inlineToAdd.level !== selection.endSection.level && (offset === 0 || offset === selection.endSection.content.length) && selection.endSection.content.length > 0) {
            if (offset === 0) {
                return Redige.patch({
                    action: "insertSection",
                    beforeId: selection.endSection.id,
                    section: inlineToAdd,
                }).then(function (addedSection) {
                    Redige.setSelection(addedSection.id, 0);
                });
            } else {
                return Redige.patch({
                    action: "insertSection",
                    after: selection.endSection.id,
                    section: inlineToAdd,
                }).then(function (addedSection) {
                    Redige.setSelection(addedSection.id, 0);
                });
            }
        }
        else if (selection.endSection.content.length === 0) {
            return Redige.patch({
                action: "updateSection", section: {
                    id: selection.endSection.id,
                    content: inlineToAdd.content
                }
            });
        } else {
            let initialContent = selection.endSection.content;
            let newContent = initialContent.substr(0, selection.startOffset) + inlineToAdd.content + initialContent.substr(offset);
            let decoratorOffset = selection.startOffset + inlineToAdd.content.length - offset;

            let decorators = selection.endSection.decorators;
            let newDecorators = decorators.map(function (decorator) {
                let newDecorator = _.clone(decorator);
                if (decorator.start > offset) {
                    newDecorator.start = decorator.start + decoratorOffset;
                }
                if (decorator.end > offset) {
                    newDecorator.end = decorator.end + decoratorOffset;
                }
                return newDecorator;
            });
            inlineToAdd.decorators.forEach(function (decorator) {
                let newDecorator = _.clone(decorator);
                newDecorator.start += offset;
                newDecorator.end += offset;
                newDecorators.push(newDecorator);
            });
            return Redige.patch({
                action: "updateSection",
                section: { id: selection.endSection.id, content: newContent, decorators: newDecorators }
            }).then(function (updatedSection) {
                Redige.setSelection(updatedSection.id, selection.startOffset + inlineToAdd.content.length);
                return updatedSection;
            });
        }
    }
};
