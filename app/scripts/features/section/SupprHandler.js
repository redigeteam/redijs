(function () {
    "use strict";

    function SupprHandler() {
    }

    SupprHandler.prototype.init = function (rootElement) {
        rootElement.addEventListener("keydown", function (event) {
            if (!Redige.isInSection()) {
                return true;
            }
            return onKeyDown(event);
        });
    };

    function isMergeable(section) {
        return (section !== null && section.tagName === "RD-SECTION");
    }

    function handleEvent(selection) {
        var offset = selection.startOffset;
        var section = selection.startSection;
        var parent = section.parent;
        var content = section.content;
        if (selection.isCollapsed) {
            // cas de la section vide (et qu'elle n'est pas la seule)
            if ((isMergeable(section.previousSibling) || isMergeable(section.nextSibling)) && offset <= 1 && content.length === 0) {
                var previousSibling = findSectionToSelect(section.previousSibling, "previous");
                var nextSibling = findSectionToSelect(section.nextSibling, "next");
                removeSection(section);
                if (nextSibling) {
                    Redige.setSelection(nextSibling.id, 0);
                } else if (previousSibling) {
                    Redige.setSelection(previousSibling.id, previousSibling.content.length);
                }
            }
            //cas de la suppression du "saut de ligne" s'il y a une section mergeable après
            else if ((offset === content.length) && isMergeable(section.nextSibling)) {
                mergeSection(section);
                section.refreshContent();
                Redige.setSelection(section.id, content.length);
            } else if ((offset === 0 && content.length === 1)) {
                //used to avoid an empty <p>. Should always contains a span (even empty)
                section.content = "";
                Redige.setSelection(section.id, 0);
            }
        }
        Redige.dispatchEventByName(parent, "structureChanged");
    }

    function removeSection(section) {
        section.parentElement.removeChild(section);
    }

    function mergeSection(currentSection) {
        // S'il y a une section après
        var nextSection = currentSection.nextSibling;
        var lastLength = currentSection.content.length;

        nextSection.decorators.forEach(function (decorator) {
            decorator.start += lastLength;
            decorator.end += lastLength;
            currentSection.decorators.push(decorator);
        });
        currentSection.innerHTML = currentSection.innerHTML + nextSection.innerHTML;
        removeSection(nextSection);
    }

    function onSupprDown(event) {
        var selection = Redige.getDocumentSelection();
        if (selection) {
            var shouldPrevent = preventSupprDefault(selection);
            handleEvent(selection);
            if (shouldPrevent) {
                event.preventDefault();
                return false;
            }
            else {
                return true;
            }
        }
    }

    function isSuppr(event) {
        return event.which === SupprHandler.KEY_SUPPR;
    }

    function preventSupprDefault(selection) {
        if (selection.isCollapsed) {
            var sectionOffset = selection.startOffset;
            var sectionLength = selection.startSection.content.length;
            //empty paragraph case
            if (sectionOffset <= 1 && sectionLength === 0) {
                return true;
            }
            //suppr after the last char
            if (sectionOffset === sectionLength) {
                return true;
            }
            //suppr the last char (to avoid to remove the last empty span)
            if (sectionOffset === 0 && sectionLength === 1) {
                return true;
            }
        }
        else {
            /*
             * limit case : when you Ctr+A the suppr an empty doc, you get a range selection of the 0x01, si you have to prevent default
             */
            return selection.startSection.content.length === 0 && selection.startSection === selection.endSection;
        }
    }

    function findSectionToSelect(sibling, direction) {
        var sectionToSelect = sibling;
        while (sectionToSelect && sectionToSelect.tagName !== "RD-SECTION") {
            if (direction === "next") {
                sectionToSelect = sectionToSelect.nextSibling;
            } else if (direction === "previous") {
                sectionToSelect = sectionToSelect.previousSibling;
            }
        }
        return sectionToSelect;
    }

    function onKeyDown(event) {
        if (isSuppr(event)) {
            return onSupprDown(event);
        }
        return true;
    }

    SupprHandler.KEY_SUPPR = 46;

    module.exports = new SupprHandler();
})();