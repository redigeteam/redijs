(function () {
    "use strict";
    const rangeActionHandler = require('../rangeAction/RangeActionHandler');
    const copyHandler = require('../copy/CopyHandler');

    function CutHandler() {
    }

    CutHandler.prototype.init = function (editor) {
        editor.addEventListener("cut", event => {
            copyHandler.copyToClipboard(event);
            const selection = Redige.getDocumentSelection();
            if (selection.startSection === selection.endSection) {
                const content = selection.startSection.content;
                const newContent = content.replace(content.substring(selection.startOffset, selection.endOffset), '');
                if (newContent.length > 0) {
                    Redige.patch({
                        action: 'updateSection',
                        section: { id: selection.startSection.id, content: newContent }
                    });
                } else {
                    Redige.patch({ action: 'deleteSection', sectionsIds: [selection.startSection.id] });
                }
            } else {
                rangeActionHandler.removeCurrentSelection(event);
            }
        });
    };

    module.exports = new CutHandler();
})();
