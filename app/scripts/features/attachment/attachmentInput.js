(function () {
    "use strict";

    function AttachmentInput() {
    }

    let currentSelection;

    AttachmentInput.createAttachmentInput = function (attachment) {

        currentSelection = Redige.getDocumentSelection();

        const attachmentInputDivision = tomplate(
            '<rd-attachment-input id="attachmentInput" class="attachmentInput" contenteditable="false" ondrop="{onDrop()}" ondragover="{onDragOver()}" ondragleave="{onDragLeave()}">' +
            '<div class="block-control">' +
            '<button class="block-control-remove" onclick="{onRemove()}"><i class="mdi mdi-minus"></i></button>' +
            '</div>' +
            '<span>Importer une pièce jointe</span>' +
            '<input type="file" class="attachmentFile" name="attachmentFile" id="attachmentFile" onchange="{onChange()}"/>' +
            '<label class="attachmentFile-label" for="attachmentFile">Sélectionner le fichier</label>' +
            '</rd-attachment-input>',
            {
                onDrop: function (event) {
                    dragAndDrop(event.dataTransfer, attachment);
                },
                onDragOver: function (event) {
                    attachmentInputDivision.classList.add('dragover');
                    event.preventDefault();
                },
                onDragLeave: function (event) {
                    if (isMouseInDivBound(event, attachmentInputDivision)) {
                        attachmentInputDivision.classList.remove('dragover');
                    }
                    event.preventDefault();
                },
                onRemove: function () {
                    Redige.setSelectionToPreviousSection(attachment);
                    Redige.patch({action: 'deleteSection', sectionsIds: [attachment.id]});
                },
                onChange: function () {
                    insertFile(inputFile, attachment);
                }
            });
        const inputFile = attachmentInputDivision.querySelector(".attachmentFile");

        setTimeout(function () {
            attachmentInputDivision.classList.add("attachmentInput-created");
            Redige.dispatchEventByName('selectionChange');
        }, 0);

        return attachmentInputDivision;
    };

    function dragAndDrop(data, attachment) {
        if (data.files.length > 0) {
            insertFromFile(data.files[0], attachment);
        } else {
            console.log("WTF ? nothing dragged");
        }
    }

    function insertFile(src, attachment) {
        if (src.files && src.files.length > 0) {
            insertFromFile(src.files[0], attachment);
        } else {
            console.error("WTF ? input not an attachment");
        }
    }

    function insertFromFile(file, attachment) {
        attachment.name = file.name;
        var reader = new FileReader();
        reader.onloadend = function () {
            Redige.patch({
                action: 'updateSection',
                section: {id: attachment.id, data: reader.result}
            }).then(function () {
                Redige._editor.focus();
                Redige.setSelectionToNextSection(attachment);
                Redige.dispatchEventByName("rdg-fileInserted-event", "", {'fileId': attachment.id, 'filePath': file.path});
            });
        };
        reader.readAsDataURL(file);
    }

    function isMouseInDivBound(event, element) {
        var rect = element.getBoundingClientRect();
        return event.x > rect.left + rect.width || event.x < rect.left || event.y > rect.top + rect.height || event.y < rect.top;
    }

    module.exports = AttachmentInput;
})();
