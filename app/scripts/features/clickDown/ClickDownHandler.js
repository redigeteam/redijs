(function () {
    "use strict";

    /*
     * This handler is used to set the focus in the last section of the document
     * when the user click under the last section, in the HTML/Body node
     */
    function ClickDownHandler() {
    }

    ClickDownHandler.prototype.init = function () {
        document.addEventListener("mousedown", function (event) {
            if (event.srcElement.nodeName === "HTML" || event.srcElement.nodeName === "BODY" || event.srcElement.nodeName === "REDIGE-EDITOR") {
                selectLastSection();
                event.preventDefault();
            }
        });
    };

    function selectLastSection() {
        const sections = Redige.getDocument().children;
        const lastSection = sections[sections.length - 1];
        if (lastSection.type !== 'hierarchical') {
            const newSection = new Redige.Section();
            newSection.init({ type: 'hierarchical', level: '0' });
            Redige.insertAfter(newSection, lastSection);
            Redige.setSelection(newSection.id, newSection.content.length);
        } else if (lastSection && lastSection.content) {
            Redige.setSelection(lastSection.id, lastSection.content.length);
        }
    }

    module.exports = new ClickDownHandler();
})();