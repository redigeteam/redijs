(function () {
    "use strict";

    const _ = require("lodash");

    function NumberingHandler() {
    }

    NumberingHandler.prototype.init = function (rootNode) {
        rootNode.addEventListener("structureChanged", function () {
            refreshNumbering(rootNode);
        });
        return this;
    };
    NumberingHandler.prototype.refreshNumbering = refreshNumbering;

    function refreshNumbering(rootNode) {
        var doc = Redige.getDocument();

        refreshHierarchicalNumbering(doc.children);
        refreshOListNumbering(doc.children);

        _.filter(rootNode.children, function (child) {
            return child instanceof Redige.Division;
        }).forEach(function (division) {
            refreshHierarchicalNumbering(division.children);
            refreshOListNumbering(division.children);
        });
    }

    function refreshHierarchicalNumbering(sections) {
        var hierachicalNumbering = [0, 0, 0, 0, 0, 0, 0];
        _.filter(sections, function (section) {
            return section instanceof Redige.Section && section.type === 'hierarchical';
        }).forEach(function (section) {
            var level = getLevelAsInt(section);
            if (level === 0) {
                section.numbering = null;
                section.localNumbering = null;
            } else {
                hierachicalNumbering[level]++;
                resetSubLevel(level, hierachicalNumbering);
                section.numbering = serializeHierarchicalNumbering(hierachicalNumbering, level);
                section.localNumbering = hierachicalNumbering[level].toString();
            }
        });
    }

    function refreshOListNumbering(childNodes) {
        var oNumbering = [0, 0, 0, 0, 0, 0, 0];
        childNodes.forEach(function (section) {
            if (section instanceof Redige.Section) {
                if (isTableSection(section)) {
                    refreshOListNumberingInTableCell(section);
                } else if (isUlist(section)) {
                    resetNumberingOnUlist(oNumbering, section);
                } else if (!isOlist(section)) {
                    oNumbering = [0, 0, 0, 0, 0, 0, 0];
                } else {
                    var level = getLevelAsInt(section);
                    oNumbering[level]++;
                    resetSubLevel(level, oNumbering);
                    section.numbering = serializeOlistNumbering(oNumbering, level);
                    section.localNumbering = oNumbering[level].toString();
                }
            } else {
                oNumbering = [0, 0, 0, 0, 0, 0, 0];
            }
        });
    }

    function refreshOListNumberingInTableCell(root) {
        var rows = root.rows;
        for (var i = 0; i < rows.length; i++) {
            refreshOListNumbering(rows[i]);
        }
    }

    function isOlist(section) {
        return section.type === "olist";
    }

    function isUlist(section) {
        return section.type === "ulist";
    }

    function isTableSection(section) {
        return section instanceof Redige.Table;
    }

    function resetNumberingOnUlist(oNumbering, ulistElement) {
        for (var index = getLevelAsInt(ulistElement); index < 7; index++) {
            oNumbering[index] = 0;
        }
    }

    function resetSubLevel(level, numbering) {
        for (var index = level + 1; index < 7; index++) {
            numbering[index] = 0;
        }
    }

    function getLevelAsInt(section) {
        return parseInt(section.level);
    }

    function serializeHierarchicalNumbering(tab, level) {
        var result = "";
        tab.forEach(function (element, index) {
            if (index > 0 && level >= index) {
                if (result.length > 0) {
                    result += "." + element;
                }
                else {
                    result += element;
                }
            }
        });
        return result;
    }

    function serializeOlistNumbering(tab, level) {
        var result = "";
        var firstValuedLevelPassed = false;
        tab.forEach(function (element, index) {
            if (element) {
                firstValuedLevelPassed = true;
            }
            if (firstValuedLevelPassed && level >= index) {
                if (result.length > 0) {
                    result += "." + element;
                }
                else {
                    result += element;
                }
            }
        });
        return result;
    }

    module.exports = new NumberingHandler();
})();