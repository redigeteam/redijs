(function () {
    "use strict";
    var linkInput = require('./LinkInput');

    function LinkHandler() {
    }

    LinkHandler.prototype.init = function () {
        Redige._editor.addEventListener("click", function (event) {
            if ((event.ctrlKey || (event.metaKey && Redige.isMacOs())) && event.srcElement.tagName === "SPAN" && event.srcElement.classList.contains("link")) {
                var decorator = getDecorator(event.srcElement.parentElement.decorators, event.srcElement.attributes.decoratoridlist.value);
                if (decorator) {
                    handleEvent(decorator);
                }
            }
        });
    };

    LinkHandler.prototype.handleEvent = handleEvent;

    function handleEvent(decorator) {
        linkInput.createLinkInput(decorator);
        Redige.dispatchEventByName('selectionChange');
    }

    function getDecorator(decorators, decoratorId) {
        var decoratorIdList = decoratorId.split(',');
        for (var i = 0; i < decorators.length; i++) {
            for (var j = 0; j < decoratorIdList.length; j++) {
                if (decorators[i].id === decoratorIdList[j] && decorators[i].type === "link") {
                    return decorators[i];
                }
            }
        }
        return null;
    }

    module.exports = new LinkHandler();
})();