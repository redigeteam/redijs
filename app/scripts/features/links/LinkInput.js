(function () {
    "use strict";

    function LinkInput() {
    }

    var popup, urlInput, _decorator, _section, _decoratorElement, _parentElement;

    LinkInput.createLinkInput = function (decorator) {
        _decoratorElement = Redige.query(Redige._editor, "[decoratoridlist*='" + decorator.id + "']");
        if (_decoratorElement) {
            _section = _decoratorElement.parentElement;
            _parentElement = _section.parentElement;
            _decorator = decorator;
            if (!popup) {
                createDiv();
            }
            document.body.appendChild(popup);
            positionLinkInputDiv(popup, _decoratorElement, _parentElement);
            urlInput.value = decorator.data || '';
            urlInput.focus();
        }
    };

    function createDiv() {
        popup = tomplate(
            '<div id="linkInput" class="linkInput" contenteditable="false">' +
            '<div class="linkInput-urlDiv">' +
            '<input type="url" oninput="{onUrlInput()}" onkeyup="{onUrlKeyUp()}" placeholder="URL du lien" class="linkInput-url"/>' +
            '<button type="button" onclick="{onGotoClick()}" class="linkInput-btnGoTo"><i class="mdi mdi-share mdi-24px"></i></button>' +
            '</div>' +
            '<button onclick="{onValid()}" type="button" class="linkInput-btnValid"><i class="mdi mdi-check mdi-24px"></i></button>' +
            '<button onclick="{onRemove()}" type="button" class="linkInput-btnRemove"><i class="mdi mdi-minus mdi-24px"></i></button>' +
            '</div>',
            {
                onValid: function () {
                    save();
                    close();
                },
                onRemove: function () {
                    removeDecorator();
                    close();
                },
                onUrlKeyUp: function (event) {
                    if (isEnter(event) && urlInput.value.length > 0) {
                        save();
                        close();
                    }
                },
                onUrlInput: function () {
                    var disabled = (urlInput.value.length > 0) ? '' : 'disabled';
                    Redige.queryAll(popup, "button").forEach(function (el) {
                        el.disabled = disabled;
                    });
                },
                onGotoClick: function () {
                    window.open(urlInput.value);
                }
            });
        urlInput = popup.querySelector(".linkInput-url");

        document.addEventListener('mousedown', function (event) {
            if (document.contains(popup)) {
                if (event.target !== popup && !popup.contains(event.target)) {
                    if (urlInput.value) {
                        save();
                    }
                    close();
                }
            }
        }, true);

        window.addEventListener('resize', function () {
            close();
            if (!_decorator.data) {
                removeDecorator();
            }
        });
    }

    function save() {
        Redige.patch({
            action: 'updateDecorator',
            data: urlInput.value,
            sectionId: _section.id,
            decorator: { id: _decorator.id, data: urlInput.value }
        });
        Redige._editor.focus();
        Redige.restoreSelection();
    }

    function close() {
        if (popup.parentElement) {
            popup.parentElement.removeChild(popup);
        }
    }

    function removeDecorator() {
        Redige.patch({ action: 'removeDecorator', sectionId: _section.id, decoratorId: _decorator.id });
    }

    function positionLinkInputDiv(div, decoratorElement, parentElement) {
        var leftPosition = decoratorElement.getBoundingClientRect().left;
        //limit the input left position to fit in the view port. (document.body.clientWidth do not include right scrollbar)
        if ((leftPosition + div.offsetWidth) > document.body.clientWidth) {
            div.style.left = document.body.clientWidth - div.offsetWidth - 20 + 'px';
        } else {
            div.style.left = leftPosition + 'px';
        }
        if (parentElement.nodeName === 'RD-DIVISION') {
            div.style.top = (parentElement.offsetTop + decoratorElement.offsetTop + decoratorElement.offsetHeight) + 'px';
        } else if (Redige.isInTable(parentElement)) {
            div.style.top = getTableOffsetTop(decoratorElement.parentElement) + decoratorElement.offsetHeight + 'px';
        } else {
            div.style.top = (decoratorElement.offsetTop + decoratorElement.offsetHeight) + 'px';
        }
    }

    function getTableOffsetTop(elt) {
        var offsetTop = 0;

        while (elt.nodeName !== "RD-DIVISION") {
            // Escape TR because offsetTop is the same than TD
            if (elt.nodeName !== "TR") {
                offsetTop += elt.offsetTop;
            }
            elt = elt.parentElement;
        }

        return offsetTop;
    }

    function isEnter(event) {
        return event.which === LinkInput.KEY_ENTER && event.ctrlKey === false;
    }

    LinkInput.KEY_ENTER = 13;

    module.exports = LinkInput;
})();