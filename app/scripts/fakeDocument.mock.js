var mockData = function () {
    "use strict";
    return new Redige.Document().init({
        id: 1,
        children: [

            {
                id: "S11",
                level: "1",
                type: "hierarchical",
                content: "Mon Titre 1"
            },
            {
                id: "S21",
                level: "2",
                type: "hierarchical",
                content: "Mon Titre 2"
            },
            {
                id: "S21s",
                level: "0",
                type: "hierarchical",
                content: ""
            },
            {
                id: "S31",
                level: "3",
                type: "hierarchical",
                content: "Mon Titre 3"
            },
            {
                id: "S41",
                level: "4",
                type: "attachment",
                name: "MyFile.pdf",
                data: 'data:text/plain;base64,cw=='
            },
            {
                id: "S51",
                level: "5",
                type: "hierarchical",
                content: "Mon Titre 5"
            },
            {
                id: "S61",
                level: "6",
                type: "hierarchical",
                content: "Mon Titre 6"
            },
            {
                id: "i2",
                type: "blockcode",
                children: [
                    {
                        id: "S1",
                        level: "0",
                        type: "hierarchical",
                        content: "aaa"
                    },
                    {
                        id: "S2",
                        level: "0",
                        type: "hierarchical",
                        content: "Bonjour voici un élémént décoré",
                        decorators: [{
                            id: "A",
                            start: 17,
                            end: 24,
                            type: "link",
                            data: "http://demo.redige.net"
                        }, {
                            id: "B",
                            start: 7,
                            end: 20,
                            type: "bold"
                        }]
                    }, {
                        id: "cod1",
                        level: "0",
                        type: "hierarchical",
                        content: "function(){",
                        decorators: []
                    }, {
                        id: "cod2",
                        level: "0",
                        type: "hierarchical",
                        content: "var toto = 4;",
                        decorators: []
                    }, {
                        id: "cod3",
                        level: "0",
                        type: "hierarchical",
                        content: "console.log('hey !!');",
                        decorators: []
                    }, {
                        id: "cod4",
                        level: "0",
                        type: "hierarchical",
                        content: "//nop",
                        decorators: []
                    }
                ]
            }, {
                id: "i101s8559559",
                type: "blockquote",
                children: [
                    {
                        id: "S11mquote",
                        level: "0",
                        type: "hierarchical",
                        content: "mon texte 1"
                    },
                    {
                        id: "S21mquote",
                        level: "0",
                        type: "hierarchical",
                        content: "You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
                        decorators: [{
                            id: "r287dbf46-7850-4cd5-8070-6e3fed48523dquote",
                            start: 210,
                            end: 214,
                            type: "link",
                            data: "http://www.google.com"
                        }, {
                            id: "Bquote",
                            start: 7,
                            end: 20,
                            type: "bold"
                        }]
                    }
                ]
            },
            {
                id: "S11m",
                level: "0",
                type: "hierarchical",
                content: "mon texte 1"
            },
            {
                id: "S11bis",
                level: "0",
                type: "hierarchical",
                content: ""
            },
            {
                id: "S21m",
                level: "0",
                type: "hierarchical",
                content: "You think water moves fast? You should see ice. It moves like it has a mind. Like it knows it killed the world once and got a taste for murder. After the avalanche, it took us a week to climb out. Now, I don't know exactly when we turned on each other, but I know that seven of us survived the slide... and only five made it out. Now we took an oath, that I'm breaking now. We said we'd say it was the snow that killed the other two, but it wasn't. Nature is lethal but it doesn't hold a candle to man.",
                decorators: [{
                    id: "r387dbf46-7850-4cd5-8070-6e3fed48523d",
                    start: 210,
                    end: 214,
                    type: "link",
                    data: "http://www.google.com"
                }, {
                    id: "B",
                    start: 7,
                    end: 20,
                    type: "bold"
                }]
            },
            {
                id: "S31m",
                level: "0",
                type: "hierarchical",
                content: "Mon TExt 2"
            },
            {
                id: "imgId2",
                type: "image",
                data: "http://soocurious.com/fr/wp-content/uploads/2014/07/personne-redige-rapport.jpg"
            },
            {
                id: "S21o",
                level: "0",
                type: "olist",
                content: "Mon TExt 1"
            }, {
                id: "S2u1",
                level: "0",
                type: "ulist",
                content: "Mon TExt 2"
            }
        ]

    });
    //return new Redige.Document().init({"id":"r8a088385-e86e-46c4-9911-04932d385034","divisions":[{"id":"rc5d94e14-c4d3-4222-9a8d-6472a2c3fbde","children":[{"id":"r87658aec-2afb-4174-8f4b-4274640a32a4","decorators":[],"level":"1","type":"hierarchical","content":"Marketing"},{"id":"r3921067c-cff7-4ba6-844c-26f1aa3499df","decorators":[],"level":"2","type":"hierarchical","content":"twitter"},{"id":"r7700f6e4-c0da-408b-9926-e8db8879c9e8","decorators":[{"id":"rd291581c-ca55-488d-b519-789629529ae8","type":"bold","start":36,"end":46}],"level":"0","type":"hierarchical","content":"Le compte twitter pour redige est : @RedigeNet"},{"id":"r574eefb2-fd34-4f27-8af6-e8e0a1b9f3de","decorators":[],"level":"2","type":"hierarchical","content":"campagnes"},{"id":"r59e5a63f-a4f2-457b-bc5b-88e1b998831e","decorators":[],"level":"0","type":"hierarchical","content":"Il pourrait être intéressant de cibler :"},{"id":"rececf526-166e-4c7e-ad67-6d248bbc09b7","decorators":[],"level":"0","type":"ulist","content":"les communautés de dev via Twitter "},{"id":"ra2a09f89-ade7-47bc-99a8-0c9d4bced866","decorators":[],"level":"0","type":"ulist","content":"les SSII via des Flyers sur les parkings"},{"id":"r0bd7ec1b-20a0-4803-89cd-7177dbbe85e4","decorators":[],"level":"0","type":"ulist","content":"les étudiants via Corentin (media ?, Btesteurs)"},{"id":"rbcc09f85-2a94-4203-a798-ee2fb4364042","decorators":[],"level":"0","type":"ulist","content":"les profs via Corentin (bouche à oreille, Btesteurs)"},{"id":"rfc076c81-d5e1-4a2d-bece-8de2a517aa24","decorators":[],"level":"0","type":"ulist","content":"les non-dev Znk (Btesteurs)"},{"id":"r112a51bc-a8e6-4a9e-9fd9-cb2fa72a28b7","decorators":[],"level":"1","type":"hierarchical","content":"Offres"},{"id":"r61759189-00cb-4e1f-be1a-ae3086ff38cc","decorators":[],"level":"2","type":"hierarchical","content":"Offre full web"},{"id":"rbdd30196-addd-44db-a72b-522740a1e012","decorators":[],"level":"0","type":"hierarchical","content":"C'est le premier niveau de prix (5€/mois)."},{"id":"r6f2b33a3-1ef7-4ca4-ac9d-a65ea2f38e7c","decorators":[],"level":"0","type":"hierarchical","content":"Il s'agit de proposer TOUTE l'expérience utilisateur d'édition et d'export dans le navigateur."},{"id":"r0030a26d-6cae-4d07-9aad-d71149bdc428","decorators":[],"level":"3","type":"hierarchical","content":"V1\u0001"},{"id":"rc374f78e-327c-44b9-9103-f480be49bce6","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"rf3d44d5a-f38d-4b2f-ac44-07d17b976bdf","decorators":[],"level":"0","type":"hierarchical","content":"Le principe est comme pour jsFiddle d'avoir un outil full cloud, sans notion de fileStorage."},{"id":"ra1ec7ef3-4397-4d6c-a827-86413d49b4af","decorators":[],"level":"0","type":"hierarchical","content":"Chaque fichier est une URL : url-base/login/fileName/version/<hash>"},{"id":"rc167a8c7-7429-44b5-9887-2492917d7a16","decorators":[],"level":"0","type":"hierarchical","content":"Les fichiers sont en base et public non découvrables."},{"id":"raae3be3a-b369-4402-933b-8460e5ed0657","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"rb7e49fbc-c40b-4e33-958e-c8f1d9b69e24","decorators":[],"level":"3","type":"hierarchical","content":"V2"},{"id":"r07af0f8a-d5a1-49d7-b495-d17254bff042","decorators":[],"level":"0","type":"hierarchical","content":"L'intégration à GDocs, dropbox, box... permet de conserver les fichiers dans un environnement cloud synchronisé multidevice (stockage seulement). Il est possible d'ouvrir un document redige dans Gdocs, dropbox etc.."},{"id":"r5032fe11-0a25-409b-8b7a-febf89662acd","decorators":[],"level":"3","type":"hierarchical","content":"Version futuriste"},{"id":"r05a453e3-f8bc-4b6c-bbdb-1843815442d2","decorators":[],"level":"0","type":"ulist","content":"intégration des tableaux"},{"id":"r583171dc-46d8-4e4d-b826-095b41b47c92","decorators":[],"level":"0","type":"ulist","content":"export revealJS"},{"id":"r33dc4e88-81de-4651-bb39-3b00a0b3af65","decorators":[],"level":"0","type":"ulist","content":"export docx"},{"id":"re49bd24f-9f70-4e31-8d74-c2f1249104d5","decorators":[],"level":"0","type":"ulist","content":"export markdown"},{"id":"r77a10171-a4c4-4cf7-8fca-26545aaeeec9","decorators":[],"level":"0","type":"ulist","content":"import docx"},{"id":"ra9912ea2-6d5f-4d58-816a-c66840075c0f","decorators":[],"level":"0","type":"ulist","content":"import markdown"},{"id":"r632c224b-6b59-4f86-b633-d12720b34e63","decorators":[],"level":"0","type":"ulist","content":"editeur graphique de thème"},{"id":"r26bff1e8-0c5d-427b-afe1-307e14216ecb","decorators":[],"level":"0","type":"ulist","content":"editeur de mise en page fine"},{"id":"r21e4ae0d-95ed-4ea3-a318-fcd44068af87","decorators":[],"level":"0","type":"ulist","content":"le checkstyle de l'auteur"},{"id":"r350263f9-8f8b-4eec-8429-8e9d0a3740b8","decorators":[],"level":"0","type":"ulist","content":"les règles de checkstyle des editeurs, CMS"},{"id":"r9ab78f2a-742c-4254-b409-1f98fc961328","decorators":[],"level":"0","type":"ulist","content":"mode collaboratif (séance de réunion avec une COPIE d'un doc partagé via BDD)"},{"id":"r5e191a52-42dd-4eb8-8101-37d8e55f57f2","decorators":[],"level":"0","type":"ulist","content":"version mobile"},{"id":"r3da42bad-c0e8-4cb5-9a25-ae96763f91a7","decorators":[],"level":"0","type":"ulist","content":"vue plan"},{"id":"r8e86c2e7-f70d-4591-b4c9-889d73306854","decorators":[],"level":"0","type":"ulist","content":"quick access (marge de layout)"},{"id":"rc2ea8bc2-a549-4c7c-be13-5e97ec6e493b","decorators":[],"level":"0","type":"ulist","content":"Todos"},{"id":"r615527b7-79a1-4bc5-a488-f1d55ba4808d","decorators":[],"level":"0","type":"ulist","content":"Comments"},{"id":"r3878d2b4-6816-48f4-8e53-412936b296ae","decorators":[],"level":"0","type":"ulist","content":"search"},{"id":"r67fce0f2-5be0-4173-acff-24f708f352e0","decorators":[],"level":"0","type":"ulist","content":"replace"},{"id":"r7bdad263-7c2b-4cfe-bc40-6d50b0ff9a9f","decorators":[],"level":"0","type":"ulist","content":"gestion de page de garde"},{"id":"r5cf928a0-1ace-40a0-86bd-c12fa75e8f53","decorators":[],"level":"0","type":"ulist","content":"gestion de header footer"},{"id":"rf3e13d80-13c1-494a-ab6b-b5d9b4d8dc76","decorators":[],"level":"0","type":"ulist","content":"print (bouton)"},{"id":"r872f7091-b48e-41fc-9c1c-4fc61dc0a78a","decorators":[],"level":"0","type":"ulist","content":"print (offre commercial d'impression en ligne)"},{"id":"rbe7cb1e0-b8ea-4f61-8edd-9f4ed1547573","decorators":[],"level":"0","type":"ulist","content":"Git compare tools"},{"id":"re3525ba8-22d6-433f-8ccf-a2877ed1584a","decorators":[],"level":"0","type":"ulist","content":"emoticon (image inlines)"},{"id":"r6e0b0495-4248-4440-a013-d8aaa5970c50","decorators":[],"level":"0","type":"ulist","content":"checkbox"},{"id":"ra1bfa534-5acb-449d-83c2-309e95b6adaf","decorators":[],"level":"0","type":"ulist","content":""},{"id":"r8a8fa6ec-f7ac-4e64-bc15-5dd3bb4acbfc","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"r4251d2a0-d499-4029-b182-f5b4c03393b4","decorators":[],"level":"2","type":"hierarchical","content":"Offre web+offline"},{"id":"r9acc3b40-b9be-453a-9e55-3c795f4e3b89","decorators":[],"level":"0","type":"hierarchical","content":"C'est le second niveau de prix (19€/mois)"},{"id":"re7c9201b-5a6c-49b5-9cbb-13c0cc3c7a4d","decorators":[],"level":"0","type":"hierarchical","content":"Il s'agit de pouvoir utiliser toutes les fonctionnalités de redige en offline. Les fichiers étant sur le disque dur éventuellement synchronisé par ailleurs via gdocs etc.."},{"id":"r95ceefe1-e933-4958-823c-83515f8a1efd","decorators":[],"level":"0","type":"hierarchical","content":"C'est une application desktop à l'apparence monolythique capable de faire l'édition et export."},{"id":"rdfffbcd7-a943-4c43-9b3a-ab9e4ca5ba37","decorators":[],"level":"0","type":"hierarchical","content":"Les fonctions de partage / diffusion / collab sont accessibles si online"},{"id":"rfe2674ba-b173-4c47-9e6a-1415a8099d0b","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"racab4cf7-6fe5-45d1-b18f-81501aaa9e1b","decorators":[],"level":"3","type":"hierarchical","content":"V1"},{"id":"rc9a8e0e4-d8b7-4154-bc03-737d6fa82d9f","decorators":[],"level":"0","type":"hierarchical","content":"Avoir un faux navigateur permettant l'écriture sur disque, faire apparaitre dedans le site de l'offre full web"},{"id":"r05dfb8a1-2ac8-4b3a-aecd-3f28063d1033","decorators":[],"level":"0","type":"hierarchical","content":"Aucune fonctionnalité offline pour l'instant"},{"id":"r3f94a52e-2739-45b1-9033-d3763819a970","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"ra7fea35c-a1df-4c8c-81f4-6a498154a1e0","decorators":[],"level":"0","type":"hierarchical","content":"La notion de section est tout simplement un paragraphe avec un type. Il peut s'agir :"},{"id":"rbd034bef-e146-4d22-8be3-60698293164e","decorators":[],"level":"0","type":"ulist","content":"d'un paragraphe normal"},{"id":"rd71c2076-3ec9-4869-8292-c47e0ee7d7fc","decorators":[],"level":"0","type":"ulist","content":"d'un titre"},{"id":"r70b1a85e-021a-40a9-be0f-dae38f6b0636","decorators":[],"level":"0","type":"ulist","content":"d'un élément d'une liste numérotée"},{"id":"ra27ff55b-3788-4f38-ab77-130ed9540cf0","decorators":[],"level":"0","type":"ulist","content":"d'un élément d'une liste à puces"},{"id":"rf94c0c16-a646-4ef0-a57b-3bbe1d814662","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"r4aefb2f7-0b10-4355-8e87-d6bc248d1eb5","decorators":[],"level":"0","type":"hierarchical","content":"Les children se caractérisent aussi par un niveau (Exemple : le niveau des children de type Titre définit s'il s'agit d'un titre ou d'un sous titre, d'un sous-sous titre...)"},{"id":"r72448df8-de9a-43db-9608-8fbc78b9ee24","decorators":[],"level":"2","type":"hierarchical","content":"Offre entreprise"},{"id":"r95ad6afe-2002-4a03-825d-a7dce72535e5","decorators":[],"level":"0","type":"hierarchical","content":"Une formule de licence pour l'offre cloud privé ou autohébergé"},{"id":"r3a7be3ed-80d4-402c-9961-b0d4c9fb5c6d","decorators":[],"level":"1","type":"hierarchical","content":"Technique"},{"id":"r986a9be0-a272-4f2b-95fd-feb74da4286b","decorators":[],"level":"2","type":"hierarchical","content":"Environnement de dev"},{"id":"r8cc39bb1-2abf-4008-8098-99ebc0876a9f","decorators":[],"level":"3","type":"hierarchical","content":"Outillage dev"},{"id":"r8cfb0189-c53d-4960-88b5-3bf5ccd61837","decorators":[],"level":"0","type":"ulist","content":"IntelliJ"},{"id":"ra5b2613f-c41f-40b3-88ae-c35f5991feea","decorators":[],"level":"0","type":"ulist","content":"Git"},{"id":"r94feed8a-c78d-4465-8267-1e62faff1241","decorators":[],"level":"0","type":"ulist","content":"Java"},{"id":"r70ed9a8b-7c3f-4c2c-993a-87c9e351436c","decorators":[],"level":"0","type":"ulist","content":"nodeJS"},{"id":"r4c45fc5e-c925-4ee0-b9cc-14c9de442961","decorators":[],"level":"0","type":"ulist","content":"maven"},{"id":"r47cdeeb7-64d6-47b7-a4be-e1e6bbe3d0f7","decorators":[],"level":"0","type":"ulist","content":"saas (+ruby)"},{"id":"rdc44ed41-5281-46d5-91f5-93452c9c1c49","decorators":[],"level":"2","type":"hierarchical","content":"Dépots Git"},{"id":"r17d4b991-f911-4436-829a-1e61a7c13b16","decorators":[],"level":"3","type":"hierarchical","content":"Redige"},{"id":"r3809a2cc-8606-4a43-9941-72e99ba701eb","decorators":[],"level":"0","type":"hierarchical","content":"URL : https://bitbucket.org/mmouterde/redige "},{"id":"r6b429fda-d2e8-4cee-b8b4-21e022da2c65","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"r8ea6ce05-33c2-441f-b683-165675c71f46","decorators":[],"level":"0","type":"hierarchical","content":"Ce projet est correspond à l'offre offline. C'est une application Java intégrant une webview."},{"id":"rae5a623d-836b-4c07-a98d-ad6e56af6316","decorators":[],"level":"0","type":"hierarchical","content":"Plusieurs branches : "},{"id":"r2a607bc9-5d57-4c5b-9fc5-bffacb21587e","decorators":[],"level":"0","type":"ulist","content":"emptyShell c'est la branche de la V1"},{"id":"r3b0c6c6d-03b2-48d4-9030-06275879b395","decorators":[],"level":"0","type":"ulist","content":"preJS est la branche qui marque la fin de la migration Java>JS. Elle intègre beaucoup de fonctionnalité mais l'essentiel est développé en Java et over-architecturé"},{"id":"r5dc24921-52a1-45fd-b849-e6c623a592c0","decorators":[],"level":"1","type":"ulist","content":"export PDF"},{"id":"rb1cb2e2f-11ed-49a6-b61f-dec916469d47","decorators":[],"level":"1","type":"ulist","content":"spelling"},{"id":"r2ff0b97a-084e-47a5-b6a1-e82eeb741a20","decorators":[],"level":"1","type":"ulist","content":"layering"},{"id":"re0edaa53-4fb1-4459-a52f-466751caa356","decorators":[],"level":"1","type":"ulist","content":"export markdown"},{"id":"r015f21e3-2ba7-4e0e-9811-caa214aac209","decorators":[],"level":"1","type":"ulist","content":"export html"},{"id":"raf20d97f-5b0d-4c83-a19d-deaf0420a444","decorators":[],"level":"1","type":"ulist","content":"import markdown"},{"id":"rf3cf12a1-96cd-4504-a5f2-66d6787055e5","decorators":[],"level":"1","type":"ulist","content":"export RevealJS"},{"id":"rad2ce1af-c717-43c4-a7d8-101f29e43cf2","decorators":[],"level":"1","type":"ulist","content":"vue plan"},{"id":"rbae8c8a4-7334-4112-a8b9-578eb47becca","decorators":[],"level":"1","type":"ulist","content":"vue layout"},{"id":"rad820453-17a2-4cda-aab3-49753cb077d0","decorators":[],"level":"3","type":"hierarchical","content":"redige-saas"},{"id":"r0694466d-6739-4794-8224-7c1f80f5135c","decorators":[{"id":"r3bc57a99-8d60-49f3-a1f6-2055bbe81149","start":6,"end":39,"type":"link"}],"level":"0","type":"hierarchical","content":"URL : https://bitbucket.org/mmouterde/redige-saas"},{"id":"r73f17f16-0ee4-402c-940e-cf0198f64126","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"r33e6bd84-d96d-4e4c-8e76-4b735dbabb22","decorators":[],"level":"0","type":"hierarchical","content":"Ce projet regroupe tous les élements de la plateforme web (offre web)"},{"id":"r25062fe6-2a55-438d-8db0-785270e0abee","decorators":[],"level":"0","type":"hierarchical","content":"NOTE : Il faut penser à découplé pour pouvoir instancier plusieurs saas sans etre couplé avec la souscription..."},{"id":"r694e47b0-c406-48a8-a789-5c55765bd9c9","decorators":[],"level":"0","type":"hierarchical","content":"Deux branches pour ce repo :"},{"id":"r5bcb7554-8713-468c-a9d2-9ac9b93b38e5","decorators":[],"level":"0","type":"ulist","content":"light version courante"},{"id":"rac5b6244-1743-4bcf-926c-4498e196091e","decorators":[],"level":"0","type":"ulist","content":"master <obsolete> : version incluant un editeur CSS angularJS"},{"id":"r59c1c732-ca3f-4fda-8013-4bee831c1cbb","decorators":[],"level":"0","type":"hierarchical","content":"NOTE : il faudra reporter light sur master et l'oublier"},{"id":"r53218236-8ea2-4d83-a2ae-d7bd2b4f7071","decorators":[],"level":"0","type":"hierarchical","content":"C'est une application MEAN"},{"id":"rc16afe6e-2a73-488e-add2-d53217f8e8ba","decorators":[],"level":"3","type":"hierarchical","content":"redige-java-api"},{"id":"r172f167a-2062-4a18-85bb-454bdeec244d","decorators":[],"level":"0","type":"hierarchical","content":"URL : https://bitbucket.org/mmouterde/redige-java-api"},{"id":"r00862109-567f-4abd-9cd6-66afbdb20498","decorators":[],"level":"0","type":"hierarchical","content":"Il s'agit des API de l'offre WEB gérant le spelling et l'export pdf"},{"id":"r1bcaf63c-1407-4958-806a-f53834c5c1d0","decorators":[],"level":"0","type":"hierarchical","content":"C'est une application Vertx (API REST)"},{"id":"r5762cfd6-934d-4423-8786-99fe750b5dd7","decorators":[],"level":"3","type":"hierarchical","content":"redijs"},{"id":"rf58dc94b-2ff4-4df0-8491-fc9b1b3ad68d","decorators":[],"level":"0","type":"hierarchical","content":"URL : https://bitbucket.org/mmouterde/redige-java-api "},{"id":"r0c05e1c1-5826-418f-b1aa-ba424afc05e7","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"re216be22-0dc7-457b-89f2-db1abfffebce","decorators":[],"level":"0","type":"hierarchical","content":"C'est le projet contenant l'IHM de l'éditeur (hors gestion de fichier). Son but est devenir potentiellement open source"},{"id":"rf246b984-62b1-463a-8d81-37e16a263411","decorators":[],"level":"0","type":"hierarchical","content":"et remplacer CKEditor ou Cie pour dominer le monde"},{"id":"r444612c2-0c06-40be-b1d6-82cd8e47da2f","decorators":[],"level":"0","type":"hierarchical","content":"c'est une application JS natif"},{"id":"rfc1d6580-a40c-417f-bacd-8454a96520db","decorators":[],"level":"0","type":"hierarchical","content":"Deux branches : "},{"id":"rabf78211-456f-4bf9-b719-fdf82d14f7d1","decorators":[],"level":"0","type":"ulist","content":"master <obsolete> une version angularJS"},{"id":"r058da65e-cbdb-48f8-8a3a-28ddff322e1f","decorators":[],"level":"0","type":"ulist","content":"light (version courante) une version JS native"},{"id":"rc23d1a9f-5ab3-45b1-a953-92538218ad15","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"r481bb1db-1968-4366-a1de-43af74f3ef6f","decorators":[],"level":"3","type":"hierarchical","content":"redige-website"},{"id":"r61a091fc-5353-44c2-b045-0bff12a93ece","decorators":[],"level":"0","type":"hierarchical","content":"URL : https://bitbucket.org/mmouterde/redige-website"},{"id":"re889cf89-5745-4a30-9ba9-2a3318ded963","decorators":[],"level":"0","type":"hierarchical","content":""},{"id":"r8be4a1f0-0611-4033-8e54-8754da5e2cf2","decorators":[],"level":"0","type":"hierarchical","content":"C'est le site web (landing page pour l'instant) mais il a pour objectif de porter tout le site web, docs et gestion des comptes."},{"id":"r068def9a-fbf0-4580-b3b8-a33d614e5f71","decorators":[],"level":"0","type":"hierarchical","content":"C'est une application _EAN"}],"type":"default"},{"id":"rbe127065-9eb6-43ca-abff-77bebdf2f3fa","children":[{"id":"r5e44f56d-17e2-4ee3-8729-ac65e52ea618","decorators":[],"level":"0","type":"hierarchical","content":""}],"type":"default"}],"file":"undefined"});
};

setTimeout(function () {
    "use strict";
    Redige.patch({ action: 'load', data: mockData() });
}, 250);