var previousOnload = window.onload;
window.onload = function () {
    "use strict";
    var editor = document.getElementById("editor");
    Redige.init(editor);
    if (previousOnload) {
        previousOnload();
    }
};